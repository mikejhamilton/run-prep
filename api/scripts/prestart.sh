#! /usr/bin/env sh

echo "Running inside /scripts/prestart.sh"

# PRESTART
# ----------
# This script executes after the entrypoint exits successully but before the application begins. Here, you should check
# if external resources are available and correctly configured, like a database or message queue. 

# DB Availability Check
echo "Checking database availability"
poetry run check_db_available

# DB Schema Check
# Schema migrations are handled outside of the project using dbmate (https://bitbucket.ambrygen.com/projects/AR/repos/dbmate-migrations/browse)
# This is an automated check that the dbmate schema is correct for the current commit
# echo "Checking database schema"
# poetry run check_db_schema

# poetry run add_worker_user
# poetry run add_unavailable_user