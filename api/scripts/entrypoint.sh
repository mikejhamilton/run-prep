#!/usr/bin/env sh
set -e

# ENTRYPOINT
# ----------
# This script should be responsible for ensuring the startup configuration of a container is sufficient to run on its own
# In other words, if no errors occur in this file, you should be left with a running container. The application inside that
# container may encounter errors for application-level reasons, but the container should in general continue running

# Check that a virtual environment exists in the expected location
if [ -d "/home/app/cache/virtualenvs" ]; then
    echo "Good, you've got a virtualenv!"
    if [ "$(find /home/app/cache/virtualenvs -mindepth 1 -maxdepth 1 -type d -printf 1 | wc -m)" -eq 1 \
      -a "$(find /home/app/cache/virtualenvs -maxdepth 1 ! -type d -printf 1 | wc -m)" -eq 0 ]; then
        echo "Even better, just one virtualenv!"
        echo "$(find /home/app/cache/virtualenvs -mindepth 1 -maxdepth 1 -type d)"
        export UWSGI_VIRTUALENV="$(find /home/app/cache/virtualenvs -mindepth 1 -maxdepth 1 -type d)"
    fi
    exec "$@"
else
    echo "Oh no! No virtualenv was detected, something went wrong"
fi

# Clear out any existing pycache files
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf