"""Helper module for reading Docker secrets https://docs.docker.com/engine/swarm/secrets/.
It is possible to do this handling in Dynaconf, but for simplicity, we load all secrets here
in a single module.
"""

import os
import uuid

TESTDB = 'test.db'
TESTDB_PATH = f"/home/app/{TESTDB}{uuid.uuid4()}"
TEST_DATABASE_URI = 'sqlite:///' + TESTDB_PATH

with open(os.path.join("/run", "secrets", "db_uri"), "r") as f:
    SECRET_DB_URI = f.read()



class Standard:

    OIDC_INTROSPECTION_AUTH_METHOD = "bearer"
    SECRET_KEY = os.environ.get("FLASK_SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = str(SECRET_DB_URI).strip()

class UnitTest:

    SECRET_KEY = os.environ.get("FLASK_SECRET_KEY", "default_test_key")
    SQLALCHEMY_DATABASE_URI = TEST_DATABASE_URI
    CELERY_BROKER_URL = "USE_A_MOCK"
