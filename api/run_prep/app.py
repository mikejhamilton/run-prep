import os

from dynaconf import FlaskDynaconf
from flask import Flask
from flask_restful import Api

from run_prep.configs.secrets import Standard, UnitTest
from run_prep.core.containers.service import services
from run_prep.core.extensions.db import db
from run_prep.core.extensions.oidc import oidc
from run_prep.core.loaders.model import load_models
from run_prep.core.loaders.resource import load_resources

# from run_prep.core.loaders.service import load_services

# if os.environ.get("FLASK_ENV", "production") == "production":
#     print("Patching all for DataDog")
#     from ddtrace import patch_all
#     patch_all()


def create_app():

    app = Flask(__name__)
    app.config['SQLALCHEMY_POOL_PRE_PING'] = True
    # app.services = services
    # load_services(app)

    # Load dynamic configuration
    if os.environ.get("RUNNING_PYTEST_UNITTESTS", False) == "True":
        app.config.from_object(UnitTest)
    else:
        app.config.from_object(Standard)

    # Load basic extensions

    FlaskDynaconf(app)
    db.init_app(app)
    oidc.init_app(app)
    app.api = Api(app)

    # app.tasks = tasks
    # load_tasks(app)

    load_models()
    load_resources(app)

    return app
