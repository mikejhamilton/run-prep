import re

from flask_restful import Resource
from run_prep.constants.permissions import ALL_GROUPS
from run_prep.core.containers.service import services

__all__ = (
    'HealthCheck',
)

class HealthCheck(Resource):
    url = "/api/healthcheck"
    endpoint = "api.healthcheck"

    # @services.auth.accept_token(require_token=False, groups_allowed=ALL_GROUPS)
    def get(self):

        resp = services.payload.get_payload_response(
            api_data="ok",
            status_code=200
        )

        return resp
