from run_prep.core.services.base import BaseService
import pytz

__all__ = ("DateService",)


class DateService(BaseService):
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "date"

    def pst_to_utc(self, date):
        """
        Converts a datetime assumed to be PST and converts it to UTC
        """

        utc_timezone = pytz.timezone("Etc/Greenwich")
        pst_timezone = pytz.timezone("America/Los_Angeles")

        pst_date = pst_timezone.localize(date)     # Assume date to be PST
        utc_date = pst_date.astimezone(utc_timezone)

        return utc_date
