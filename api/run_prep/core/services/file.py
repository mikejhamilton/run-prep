import os
from datetime import datetime

from run_prep.constants.exceptions import IncorrectFileExtension, InvalidFileName
from run_prep.core.containers.service import services
from run_prep.core.services.base import BaseService
from werkzeug.utils import secure_filename

__all__ = ("FileService",)


class FileService(BaseService):
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "file"

    def save_file(self, path, f, timestamp=False, time_format="%x_%X"):
        """
            Saves file to a path with a secure filename.
            Creates path directory if it does not exist.
            Allows optional timestamp to be appended to file name.
        """
        os.makedirs(path, exist_ok=True)
        if timestamp:
            name, ext = os.path.splitext(f.filename)
            file_name = secure_filename(f"{name}_{datetime.now().strftime(time_format)}{ext}")
        else:
            file_name = secure_filename(f.filename)
        if not file_name:
            # secure_filename can return an empty file name
            raise InvalidFileName(f"Invalid filename: {f.filename}")
        full_file_path = os.path.join(path, file_name)
        f.stream.seek(0) # seek to the beginning of file
        f.save(full_file_path)
        services.log.info(f"Created file: {full_file_path}")
        return full_file_path

    def validate_extension(self, f, ext):
        _, file_ext = os.path.splitext(f.filename)
        if file_ext not in ext:
            raise IncorrectFileExtension(f"File: {f.filename} has wrong extension, needs: {ext}")
