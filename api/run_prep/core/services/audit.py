from run_prep.core.services.base import BaseService


__all__ = ("AuditService",)

class AuditService(BaseService):
        
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "audit"

