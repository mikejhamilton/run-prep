"""An abstract base class for all application services. Since many services will run
in the background and should be explicitly initialized when the application starts,
the service autoloader will attempt to call Service.start(app) on any newly loaded service
passing the current application as the first positional argument.

This method should be implemented in any subclasses of BaseService and should contain
any setup/startup code to ensure the service runs as expected after initialization
"""

from abc import ABC, abstractmethod

class BaseService(ABC):

    @classmethod
    @abstractmethod
    def get_name(cls):
        pass

    @abstractmethod
    def start(self, app):
        pass