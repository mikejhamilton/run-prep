from run_prep.core.extensions.db import db, db_events
from run_prep.core.models.event import Event
from run_prep.core.services.base import BaseService
from run_prep.constants.event_class_blacklist import EVENT_CLASS_BLACKLIST
from run_prep.core.containers.service import services
from datetime import datetime
from run_prep.core.models.user import User
from uuid import uuid4
from flask import g
import re

__all__ = ("EventService",)

# Certain forms use "text" type for numeric inputs, so they are received as strings. When we store them
# As JSON strings in the db, Flask automatically converts numeric values. We do some processing on numeric
# strings here so that the event shows both versions as numeric.
def process_history(history_slice):
    entry = history_slice[0]
    if hasattr(entry, 'get_serializer'):
        return entry.get_serializer().dump(entry)
    elif isinstance(entry, str) and str.isnumeric(entry):
        f = float(entry)
        i = int(entry)
        return i if f == i else f
    elif isinstance(entry, datetime):
        return entry.strftime("%m/%d/%Y, %H:%M:%S")
    else:
        return entry

class EventService(BaseService):
    def start(self, app):
        @app.before_request
        def set_uuid():
            g.request_uuid = uuid4()

        @app.after_request
        def commit_event(resp):
            if 'event' in g:
                # Session transaction must be closed at beginning of hook to get updated view of database
                db_events.close()
                db_events.add(g.event)
                db_events.commit()

            return resp

    @classmethod
    def get_name(cls):
        return "events"

    @classmethod
    def create_events_after_flush(cls, session, flush_context):
        try:
            username = services.auth.get_current_username()
        except AttributeError:
            # Need to short circuit here in case a current user is not set. Events won't be created which should be
            # fine in general for tasks like creating a system user for the worker service.
            services.log.debug(f"Skipping event creation for {session}. This should only occur on system operations.")
            return

        if 'event' not in g:
            event = Event()
            event.uuid = g.setdefault("request_uuid", uuid4())
            event.username = username
            event.summary = []
            initial_values = {}
            final_values = {}
        else:
            event = g.event
            initial_values = event.initial_values
            final_values = event.final_values

        # Newly added instances
        for instance in session.new:
            if not isinstance(instance, EVENT_CLASS_BLACKLIST):
                model_name = instance.__class__.__name__
                ext_id = instance.ext_id
                # Initialize structure for final values first time model is encountered
                if model_name not in final_values.keys():
                    final_values[model_name] = {ext_id: {}}
                final_values[model_name][ext_id] = instance.get_serializer().dump(instance)
                message = f'Added {model_name} [{ext_id}]'
                event.summary = event.summary + [message] if event.summary is not None else [message]

        # Modified instances
        for instance in session.dirty:
            if session.is_modified(instance) and not isinstance(instance, EVENT_CLASS_BLACKLIST):
                model_name = instance.__class__.__name__
                ext_id = instance.ext_id
                state = db.inspect(instance)
                instance_has_changes = False

                for prop in state.attrs:
                    history = prop.load_history()
                    if history.has_changes():
                        instance_has_changes = True

                        if history.deleted:
                            # Initialize structure for initial values first time model is encountered.
                            if model_name not in initial_values.keys():
                                initial_values[model_name] = {ext_id: {}}
                            if ext_id not in initial_values[model_name].keys():
                                initial_values[model_name][ext_id] = {prop.key: {}}
                            initial_values[model_name][ext_id][prop.key] = process_history(history.deleted)

                        if history.added:
                            # Initialize structure for final values first time model is encountered.
                            if model_name not in final_values.keys():
                                final_values[model_name] = {ext_id: {}}
                            if ext_id not in final_values[model_name].keys():
                                final_values[model_name][ext_id] = {prop.key: {}}
                            final_values[model_name][ext_id][prop.key] = process_history(history.added)
                if instance_has_changes:
                    # Only append to summary if we didn't already create or modify this entity during this event
                    r = re.compile(f'^(Modified|Added) {model_name} \[{ext_id}\]$')
                    if not list(filter(r.match, event.summary)):
                        message = f'Modified {model_name} [{ext_id}]'
                        event.summary = event.summary + [message] if event.summary is not None else [message]

        # If either values field was populated with changes, save them to the event.
        if initial_values or final_values:
            event.initial_values = initial_values
            event.final_values = final_values
            g.event = event

db.event.listen(db.session, "after_flush", EventService.create_events_after_flush)