import logging
import sys
import traceback
from run_prep.core.services.base import BaseService
from pythonjsonlogger import jsonlogger
from datetime import datetime
from ddtrace.helpers import get_correlation_ids
from celery.signals import after_setup_logger

__all__ = ("LoggingService",)

class LoggingService(BaseService):

    def start(self, app):
        if app.config["ENV"] in ["development"]:
            pass
        else:
            self.handler.setFormatter(JsonFormatter())

    @classmethod
    def get_name(cls):
        return "log"
    
    def __init__(self):
        # Set up streamhandler to output json format
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)
        self.handler = logging.StreamHandler()
        if not self.logger.handlers:
            self.logger.addHandler(self.handler)

    def debug(self, msg):
        self.logger.debug(msg)

    def info(self, msg):
        self.logger.info(msg)

    def warning(self, msg):
        self.logger.warning(msg)

    def error(self, msg):
        self.logger.error(msg)

    def get_exception_logging(self):
        
        def _(exctype, value, tb):
            """
            Log exception by using the root logger.

            Parameters
            ----------
            exctype : type
            value : NameError
            tb : traceback
            """
            write_val = {
                'exception_type': str(exctype),
                'message': str(traceback.format_tb(tb, 10)),
                'exception': str(value)
            }
            self.logger.exception(str(write_val))
        
        return _

class JsonFormatter(jsonlogger.JsonFormatter):
    """Logging formatter for creating json-formatted messages for global logging
    """

    def add_fields(self, log_record, record, message_dict):
        """Each log record
        """
        super(JsonFormatter, self).add_fields(log_record, record, message_dict)
        if not log_record.get("timestamp"):
            # this doesn't use record.created, so it is slightly off
            now = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            trace_id, span_id = get_correlation_ids()
            log_record["dd.trace_id"] = trace_id or 0
            log_record["dd.span_id"] = span_id or 0
            log_record["timestamp"] = now
        if log_record.get("level"):
            log_record["level"] = log_record["level"].upper()
        else:
            log_record["level"] = record.levelname
