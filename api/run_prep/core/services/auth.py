from functools import wraps
from run_prep.core.services.base import BaseService
from run_prep.core.extensions.oidc import oidc
from run_prep.core.extensions.db import db
from run_prep.core.containers.service import services
from run_prep.core.models.user import User
from flask import request, g
from datetime import datetime
import json
import os

__all__ = ("AuthService",)


class AuthService(BaseService):

    def start(self, app):
        self.app = app
        self.token_groups_field = "amp-groups"
        self.username_field = "username"

    @classmethod
    def get_name(cls):
        return "auth"

    def get_current_user_groups(self):
        return g.oidc_token_info.get(self.token_groups_field, [])

    def get_current_username(self):
        try:
            if self.app.config["IS_CELERY_WORKER"] == 1:
                return "worker"
            else:
                return g.oidc_token_info[self.username_field]
        except Exception as e:
            services.log.warning(f"Username unavailable for request: {e}.")
            return "unavailable"

    def get_current_user(self) -> User:
        return User.query.filter(User.username == self.get_current_username()).one_or_none()


    def current_user_has_group_access(self, group):
        """Returns true only if user has the specified group access
        """
        return group in self.get_current_user_groups()

    def current_user_is_new_user(self):
        user = User.query.filter(User.username == self.get_current_username()).one_or_none()
        if user:
            return False
        return True

    def register_user(self):
        u = User(
            username=self.get_current_username(),
            last_login=datetime.now(),
        )
        db.session.add(u)
        db.session.commit()

    def accept_token(self, require_token=False, scopes_required=None, groups_allowed=None):
        """
        Use this to decorate view functions that should accept OAuth2 tokens,
        this will most likely apply to API functions.
        Tokens are accepted as part of the query URL (access_token value) or
        a POST form value (access_token).
        Note that this only works if a token introspection url is configured,
        as that URL will be queried for the validity and scopes of a token.
        :param require_token: Whether a token is required for the current
            function. If this is True, we will abort the request if there
            was no token provided.
        :type require_token: bool
        :param scopes_required: List of scopes that are required to be
            granted by the token before being allowed to call the protected
            function.
        :type scopes_required: list
        :param groups_allowed:
        :type groups_allowed: list
        .. versionadded:: 1.0
        """

        def wrapper(view_func):
            @wraps(view_func)
            def decorated(*args, **kwargs):
                if os.environ.get("RUNNING_PYTEST_UNITTESTS", False) == "True":
                    return view_func(*args, **kwargs)
                token = None
                auth_override = False
                # Check for non production backend auth override in headers
                if self.app.config["ENV"] not in ["production"]:
                    if 'AuthOverride' in request.headers and request.headers['AuthOverride'] == "True":
                        auth_override = True
                # Check for token in headers
                if 'Authorization' in request.headers and request.headers['Authorization'].startswith('Bearer '):
                    token = request.headers['Authorization'].split(None, 1)[1].strip()

                if self.app.config["ENV"] in ["development"]:
                    if token:
                        try:
                            validity = True
                            g.oidc_token_info = json.loads(token)
                        except json.decoder.JSONDecodeError:
                            validity = oidc.validate_token(token, scopes_required)
                    else:
                        validity = False
                        g.oidc_token_info = dict()
                else:
                    validity = oidc.validate_token(token, scopes_required)

                if validity is True and require_token:
                    if self.current_user_is_new_user():
                        services.log.info(f"Creating entity for new user: {self.get_current_username()}")
                        self.register_user()
                if auth_override:
                    return view_func(*args, **kwargs)
                if (validity is True) or (not require_token):
                    if groups_allowed and require_token:
                        for group in groups_allowed:
                            if self.current_user_has_group_access(group):
                                return view_func(*args, **kwargs)
                        response_body = {'error': 'unauthorized',
                                         'error_description': 'You do not have sufficient permissions to access this resource.'}
                        return response_body, 401, {'WWW-Authenticate': 'Bearer'}
                    else:
                        return view_func(*args, **kwargs)
                else:
                    response_body = {'error': 'invalid_token',
                                     'error_description': validity}
                    return response_body, 401, {'WWW-Authenticate': 'Bearer'}

            return decorated

        return wrapper
