from run_prep.core.services.base import BaseService
import functools

__all__ = ("AttributeService",)


class AttributeService(BaseService):
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "attribute"

    @classmethod
    def rgetattr(cls, obj, attr, *args):
        """Iteratively gets attributes from an object, splitting the attributes on periods."""
        def _getattr(obj, attr):
            return getattr(obj, attr, *args)
        return functools.reduce(_getattr, [obj] + attr.split('.'))
