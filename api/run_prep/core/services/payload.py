from run_prep.core.services.base import BaseService
from dataclasses import dataclass, field
from marshmallow import Schema, fields
from typing import Union

__all__ = ("PayloadService",)


class PayloadService(BaseService):
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "payload"

    def get_payload_response(
        self,
        api_data,
        status_code,
        errors=None,
        legacy_serializer=None,
        success=True,
        reason="",
        raw_data=None,
        headers=None,
    ):
        schema = PayloadResponseSchema()
        pr = PayloadResponse(
            api_data=api_data,
            status_code=status_code,
            success=success,
            reason=reason,
            raw_data=raw_data,
        )
        if errors:
            pr.errors = errors
        if legacy_serializer:
            print(f"Using legacy serializer {legacy_serializer}")

        return schema.dump(pr), status_code, headers


class DictOrListField(fields.Field):
    def _deserialize(self, value, attr, data, **kwargs):
        if isinstance(value, dict) or isinstance(value, list):
            return value
        else:
            raise ValidationError('Field should be dict or list')


class PayloadResponseSchema(Schema):
    api_data = DictOrListField()
    status_code = fields.Int()
    success = fields.Boolean(default=True)
    errors = fields.List(cls_or_instance=fields.Str, default=[])
    raw_data = fields.Raw(default=None)
    reason = fields.Str(default="")


@dataclass
class PayloadResponse:
    api_data: dict
    status_code: int
    success: bool = True
    errors: list = field(default_factory=list)
    raw_data: Union[str, None] = None
    reason: str = ""
