import hashlib

from run_prep.core.services.base import BaseService

__all__ = ("HashingService",)


class HashingService(BaseService):
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "hash"

    def hash_file(self, f):
        h = hashlib.md5()
        f.seek(0)
        while True:
            chunk = f.read(h.block_size)
            if not chunk:
                break
            h.update(chunk)
        f.seek(0)
        return h.hexdigest()
