from inspect import isclass
from sys import modules

from flask_restful import Resource
from run_prep.core.loaders.loader import dynamic_loader


def get_resources():
    """Dynamic view finder."""
    return dynamic_loader('resources', is_resource) + dynamic_loader('core/resources', is_resource)


def is_resource(item):
    """Determine if `item` is a `Resource` subclass
    (because we don't want to register `Resource` itself).
    """
    return item is not Resource and isclass(item) and issubclass(item, Resource)

def load_resources(app):
    for r in get_resources():
        app.api.add_resource(r, r.url, endpoint=r.endpoint) 
