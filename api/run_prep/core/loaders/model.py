from inspect import isclass
from sys import modules

from flask_sqlalchemy import Model
from run_prep.core.containers.service import services
from run_prep.core.loaders.loader import dynamic_loader


def get_models():
    """Dynamic model finder."""
    return dynamic_loader('models', is_model) + dynamic_loader('core/models', is_model)


def is_model(item):
    """Determines if `item` is a `db.Model`."""
    return isclass(item) and issubclass(item, Model) and not item.__ignore__()


def load_models():
    """Load application models for management script & app availability."""
    for model in get_models():
        services.log.info(model)
        setattr(modules[__name__], model.__name__, model)
