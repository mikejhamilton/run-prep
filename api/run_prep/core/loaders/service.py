from inspect import isclass
from sys import modules

from run_prep.core.loaders.loader import dynamic_loader
from run_prep.core.services.base import BaseService


def get_services():
    """Dynamic view finder."""
    return dynamic_loader('services', is_service) + dynamic_loader('core/services', is_service)


def is_service(item):
    """Determine if `item` is a `Resource` subclass
    (because we don't want to register `Resource` itself).
    """
    return item is not BaseService and isclass(item) and issubclass(item, BaseService)


def load_services(app):
    for s in get_services():
        app.services.register_service(s, app)
