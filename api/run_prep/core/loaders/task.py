from inspect import isclass
from sys import modules

from run_prep.core.loaders.loader import dynamic_loader
from run_prep.core.tasks.base import BaseTask


def get_tasks():
    """Dynamic task finder."""
    return dynamic_loader('tasks', is_task) + dynamic_loader('core/tasks', is_task)


def is_task(item):
    """Determines if `item` is a subclass of the BaseTaskModel."""
    return isclass(item) and issubclass(item, BaseTask)


def load_tasks(app):
    """Load application tasks for management script & app availability."""
    for t in get_tasks():
        app.tasks.register_task(t, app)
