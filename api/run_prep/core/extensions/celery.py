from celery import Celery
from run_prep.core.tasks.base import get_context_task 

def make_celery(app):
    celery = Celery(
        app.import_name,
        broker=app.config['CELERY_BROKER_URL'],
    )
    celery.conf.update(app.config)
    celery.Task = get_context_task(app)
    return celery