from flask_sqlalchemy import SQLAlchemy
from run_prep.configs.secrets import Standard
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

db = SQLAlchemy(session_options={"autoflush": False})

# Independent session that can be used while inspecting db's event hooks
engine = create_engine(Standard.SQLALCHEMY_DATABASE_URI, pool_pre_ping=True)
Session = sessionmaker(bind=engine)
db_events = Session()
