import requests
from flask import current_app
from flask_oidc import OpenIDConnect


class OIDC(OpenIDConnect):

    def _get_token_info(self, token):
        # We hardcode to use client_secret_post, because that's what the Google
        # oauth2client library defaults to
        request = {'token': token}
        headers = {'Content-type': 'application/x-www-form-urlencoded'}

        hint = current_app.config['OIDC_TOKEN_TYPE_HINT']
        if hint != 'none':
            request['token_type_hint'] = hint

        request['client_id'] = self.client_secrets['client_id']

        response = requests.request("POST", self.client_secrets['token_introspection_uri'], headers=headers, data=request)

        # TODO: Cache this reply
        return response.json()
    
    def load_secrets(self, app):
        # Load client_secrets.json to pre-initialize some configuration
        return dict(app.config['OIDC_CLIENT_SECRETS'])

oidc = OIDC()
