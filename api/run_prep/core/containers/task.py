from run_prep.core.tasks.base import BaseTask
from inspect import isclass

class TaskContainer:
    
    def register_task(self, task, app):
        if task is not BaseTask and isclass(task) and issubclass(task, BaseTask):
            if hasattr(self, task.get_name()):
                raise ValueError(f"A task with the name {task.get_name()} is already registered.")
            t = task()
            setattr(self, task.get_name(), t)
            t.register(app.celery)
        else:
            raise ValueError(f"Attempted to register an invalid task: {service}")

tasks = TaskContainer()