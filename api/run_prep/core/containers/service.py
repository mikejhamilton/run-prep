from inspect import isclass

from run_prep.core.services.base import BaseService


class ServiceContainer():
    
    def register_service(self, service, app):
        if service is not BaseService and isclass(service) and issubclass(service, BaseService):
            if hasattr(self, service.get_name()):
                raise ValueError(f"A service with the name {service.get_name()} is already registered.")
            setattr(self, service.get_name(), service())
            getattr(self, service.get_name()).start(app)
        else:
            raise ValueError(f"Attempted to register an invalid service: {service}")

services = ServiceContainer()
