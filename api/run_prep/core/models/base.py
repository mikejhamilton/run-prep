from datetime import datetime
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property
from run_prep.core.extensions.db import db


__all__ = ('Model', 'TimeStampModel')


class Model(db.Model):

    __abstract__ = True

    @classmethod
    def __ignore__(cls):
        """Custom class attr that lets us control which models get ignored.

        We are using this because knowing whether or not we're actually dealing
        with an abstract base class is only possible late in the class's init
        lifecycle.

        This is used by the dynamic model loader to know if it should ignore.
        """
        return cls.__name__ in ('Model', 'TimeStampModel')  # can add more abstract base classes here

    @hybrid_property
    def ext_id(self):
        return self.id

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    id = db.Column(db.Integer, primary_key=True)


class TimeStampModel(Model):

    __abstract__ = True

    @declared_attr
    def created_at(cls):
        return db.Column(
            db.DateTime,
            nullable=False,
            default=datetime.utcnow
        )

    @declared_attr
    def updated_at(cls):
        return db.Column(
            db.DateTime,
            default=datetime.utcnow,
            onupdate=datetime.utcnow
        )
