from run_prep.core.extensions.db import db
from run_prep.core.models.base import Model
from datetime import datetime
from datetime import timedelta
from dateutil import parser
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

__all__ = ('Event', 'EventSchema')

class Event(Model):
    __tablename__ = "events"

    uuid = db.Column(db.String(36), nullable=False)
    initial_values = db.Column(db.JSON, nullable=False)
    final_values = db.Column(db.JSON, nullable=True)
    summary = db.Column(db.JSON, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    username = db.Column(db.String(50), db.ForeignKey('users.username'))
    user = db.relationship('User')

    @classmethod
    def get_serializer(cls):
        return EventSchema()

    @classmethod
    def find_by_date_range_paginated(cls, page_num, page_size, low_date, high_date):

        return cls.query \
            .filter(low_date <= Event.created_at, high_date >= Event.created_at) \
            .order_by(Event.created_at.desc()) \
            .paginate(page_num, per_page=int(page_size))

class EventSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Event
        include_fk = True
        load_instance = True
