from run_prep.core.extensions.db import db
from run_prep.core.models.base import Model
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from run_prep.core.models.base import TimeStampModel

__all__ = ('User', 'UserInfoSchema')


class User(TimeStampModel):
    __tablename__ = 'users'

    # User Authentication fields
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), nullable=False, unique=True)
    last_login = db.Column(db.String(50), nullable=False)
    second_last_login = db.Column(db.String(50), nullable=True)

    @classmethod
    def get_serializer(cls):
        return UserInfoSchema()

class UserInfoSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = User
        load_instance = True
