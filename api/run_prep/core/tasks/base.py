import celery
from abc import ABC, abstractmethod
from run_prep.core.extensions.db import db
from flask import g
from uuid import uuid4

class BaseTask(ABC):
    """Base class for all long-running tasks in the application

    Task Definition
    >>> # api/amp/tasks/test.py
    >>>
    >>> __all__ = ("TestTask",)
    >>>
    >>> class TestTask(BaseTask):
    >>> 
    >>>     @classmethod
    >>>     def get_name(cls):
    >>>         return "test"
    >>>     
    >>>     @staticmethod
    >>>     def run(arg1, arg2, kwarg1=None):
    >>>         # Long Running Business Logic Here

    Task Invocation
    >>> from run_prep.core.containers.task import tasks
    >>> tasks.test.schedule(arg1, arg2, kwarg1="foo")
    """

    @classmethod
    @abstractmethod
    def get_name(cls):
        pass

    @staticmethod
    @abstractmethod
    def run(*args, **kwargs):
        pass

    def register(self, celery_app):

        @celery_app.task(
            name=self.get_name(),
            ignore_result=True
        )
        def _(*args, **kwargs):
            return self.run(*args, **kwargs)

        self.__task = _
        return _
    
    def schedule(self, *args, **kwargs):
        self.__task.apply_async(args=args, kwargs=kwargs) 
        
def get_context_task(app):

    class ContextTask(celery.Task):

        def __call__(self, *args, **kwargs):
            with app.app_context():
                # Avoid sharing connection accross threads
                # See https://docs.sqlalchemy.org/en/13/faq/connections.html
                g.request_uuid = uuid4()
                db.engine.dispose()
                return self.run(*args, **kwargs)

    return ContextTask