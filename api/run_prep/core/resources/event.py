from datetime import timedelta

from dateutil import parser
from flask import request
from flask_restful import Resource
from run_prep.constants.permissions import ALL_GROUPS
from run_prep.core.containers.service import services
from run_prep.core.models.event import Event

__all__ = (
    'FetchEventsByDateRangeResource',
)

class FetchEventsByDateRangeResource(Resource):
    url = "/api/v2/event/fetch-by-date-range"
    endpoint = "api.v2.event.fetch_by_date_range"

    @services.auth.accept_token(require_token=False, groups_allowed=ALL_GROUPS)
    def get(self):
        """
        Fetches paginated event rows by a low and high date range and pagination options.
        The low and high date are expected to be strings in format %Y-%d-%m

        The dates are expected to be PST and must be converted to UTC before comparing against the database.
        The high date is converted to midnight to ensure all events on that day are captured.
        """
        page = int(request.args.get("page", 1))
        size = int(request.args.get("sizePerPage", 30))
        low_date = request.args.get("lowDate")
        high_date = request.args.get("highDate")

        if not all([low_date, high_date]):
            fail_message = f"Missing item(s) from query parameters. (lowDate: {low_date}, highDate: {high_date})"
            return services.payload.get_payload_response(
                api_data=None,
                success=False,
                errors=[fail_message],
                status_code=400
            )

        high_date_obj = (parser.parse(high_date) + timedelta(days=1)).replace(hour=0, minute=0, second=0)
        low_date_obj = parser.parse(low_date)

        low_utc_date = services.date.pst_to_utc(low_date_obj)
        high_utc_date = services.date.pst_to_utc(high_date_obj)

        results = Event.find_by_date_range_paginated(
            page_num=page, page_size=size, low_date=low_utc_date, high_date=high_utc_date
        )

        events = [Event.get_serializer().dump(event) for event in results.items]

        return services.payload.get_payload_response(
            api_data={"items": events, "total": results.total},
            status_code=200
        )
