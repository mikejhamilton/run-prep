from flask_restful import Resource
from run_prep.core.extensions.db import db

__all__ = ('HealthCheckResource',)

class HealthCheckResource(Resource):
    url = "/api/healthcheck"
    endpoint = "api.healthcheck"

    def get(self):
        db_status = "ok"
        try:
            db.engine.execute("SELECT 1")
        except Exception as e:
            db_status = db_status = "degraded"
        statuses = [
            db_status
        ]
        if any([s != "ok" for s in statuses]):
            return {
                "status": "degraded",
                "db": db_status
            }
        else:
            return {
                "status": "ok",
                "db": db_status,
            }
