from flask_restful import Resource
from run_prep.core.extensions.db import db
from run_prep.core.models.user import User, UserInfoSchema
from run_prep.core.containers.service import services
from datetime import datetime

__all__ = ('LoginResource',)

serializer = UserInfoSchema()

class LoginResource(Resource):
    url = "/api/login"
    endpoint = "api.login"

    @services.auth.accept_token(require_token=True)
    def post(self):
        username = services.auth.get_current_username()
        user = User.query.filter(User.username==username).first_or_404()
        user.second_last_login = user.last_login
        user.last_login = datetime.now()
        db.session.add(user)
        db.session.commit()
        return {"success": "true"}
    