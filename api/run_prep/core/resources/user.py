from flask_restful import Resource
from flask import jsonify, current_app, g
from run_prep.core.extensions.db import db
from run_prep.core.models.user import User, UserInfoSchema
from run_prep.core.containers.service import services
from run_prep.constants.permissions import ALL_GROUPS

__all__ = ('UserResource', 'UserInfoResource',)

serializer = UserInfoSchema()

class UserResource(Resource):
    url = "/api/user"
    endpoint = "api.user"

    @services.auth.accept_token(require_token=True, groups_allowed=ALL_GROUPS)
    def get(self):
        users = User.query.all()
        return [serializer.dump(user) for user in users]

class UserInfoResource(Resource):
    url = "/api/user/info"
    endpoint = "api.user.info"

    @services.auth.accept_token(require_token=True, groups_allowed=ALL_GROUPS)
    def get(self):
        username = services.auth.get_current_username()
        user = User.query.filter(User.username==username).first_or_404()
        groups = services.auth.get_current_user_groups()
        data = serializer.dump(user)
        data["groups"] = groups
        return data
    