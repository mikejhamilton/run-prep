from run_prep.core.services.base import BaseService
from run_prep.core.containers.service import services
from run_prep.models.design import Design
from run_prep.core.extensions.db import db
from run_prep.models.sanger_primer import SangerPrimer
from run_prep.core.containers.service import services
from run_prep.constants.exceptions import ForbiddenModifyError

__all__ = ("SangerPrimerFactoryService",)


class SangerPrimerFactoryService(BaseService):
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "sanger_primer_factory"

    @classmethod
    def create_or_update_primer(cls, coords, sequence, name, direction, step, is_alubp=False):
        """
        Creates a new primer if one does not exist, otherwise modify current primer.
        Also creates a new design if the current one changes or does not exist.

        """
        coords, sequence, name, direction, step = services.parser.remove_whitespace(coords, sequence, name, direction, step)

        primer = SangerPrimer.find_by_name(name).one_or_none()
        primer_coords = services.parser.parse_genomic_coords(coords)
        if name in ["s-tag", "as-tag"]:
            if primer:
                return primer
            else:
                raise ForbiddenModifyError("Cannot modify or create s-tag/as-tag")
        primer_design = Design.find_or_create(
            direction, sequence, primer_coords["chromosome"], primer_coords["start"], primer_coords["end"]
        )
        db.session.flush()
        if primer:
            primer.design_id = primer_design.id
            primer.is_alubp = is_alubp
        else:
            primer = SangerPrimer(design_id=primer_design.id, sanger_primer_name=name, step=step, is_alubp=is_alubp)

        db.session.add(primer)
        return primer
