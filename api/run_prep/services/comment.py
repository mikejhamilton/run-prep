from run_prep.core.services.base import BaseService
from run_prep.models.amplicon_version import AmpliconVersion
from run_prep.models.comment import Comment

__all__ = ("CommentService",)


class CommentService(BaseService):

    def start(self, app):
        self.app = app

    @classmethod
    def get_name(cls):
        return "comments"

    @staticmethod
    def get_comments_by_amplicon_version_name(amplicon_version_name):
        """
        Returns comments based on a given amplicon_version name.
        :param amplicon_version_name:
        :return:
        """
        return Comment.query.join(AmpliconVersion, Comment.amplicon_versions).filter_by(
            amplicon_version_name=amplicon_version_name)

    @staticmethod
    def get_comments_by_amplicon_version_id(amplicon_version_id):
        """
        Returns comments based on a given amplicon_version id.
        :param amplicon_version_id:
        :return:
        """
        return Comment.query.join(AmpliconVersion, Comment.amplicon_versions).filter_by(id=amplicon_version_id)
