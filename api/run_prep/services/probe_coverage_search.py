import os
from run_prep.core.services.base import BaseService
from run_prep.models.targeted_array_probe import TargetedArrayProbe
from run_prep.models.ngs_target_group import NgsTargetGroup
from run_prep.models.ngs_probe import NgsProbe
from run_prep.models.mlpa_probe import MlpaProbe
from run_prep.models.target import Target
from run_prep.models.array_chip import ArrayChip
from run_prep.models.mlpa_kit import MlpaKit
from run_prep.models.ngs_probeset import NgsProbeset
from run_prep.models.upload_job import UploadJob
from run_prep.models.upload import Upload
from run_prep.models.library import Library
from run_prep.core.containers.service import services
from run_prep.core.extensions.db import db
from sqlalchemy import func, and_, or_

__all__ = ("ProbeCoverageSearchService",)


class ProbeCoverageSearchService(BaseService):
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "probe_coverage_search"

    @classmethod
    def targeted_micro_array_coverage_by_region(cls, chromosome, start, end):
        """
        Coverage Description:
        ---------------------
        Targeted Array Probes are the simplest case because their designs directly
        correspond to their targets. Therefore, a single probe has a single target
        and we can simply join all the probes to their targets, query for targets
        intersecting the region of interest, and call it a day.
        """
        probe_query = (
            db.session.query(func.count(TargetedArrayProbe.id), ArrayChip.name, Library.name, UploadJob.job_status)
            .join(Target, TargetedArrayProbe.target)
            .join(ArrayChip, TargetedArrayProbe.chip)
            .join(Library, ArrayChip.libraries)
            .join(UploadJob, ArrayChip.upload_job)
            .filter(Target.chromosome == chromosome)
            .filter(
                # Partial probe coverage at start of call
                (
                    (Target.start_position <= (int(start) - int(os.environ.get("ARRAY_PROBE_PADDING"))))
                    & (Target.end_position >= (int(start) - int(os.environ.get("ARRAY_PROBE_PADDING"))))
                )
                # Call surrounds Probe
                | (
                    (Target.start_position >= (int(start) - int(os.environ.get("ARRAY_PROBE_PADDING"))))
                    & (Target.end_position <= (int(end) + int(os.environ.get("ARRAY_PROBE_PADDING"))))
                )
                # Probe surrounds call
                | (
                    (Target.start_position <= (int(start) - int(os.environ.get("ARRAY_PROBE_PADDING"))))
                    & (Target.end_position >= (int(end) + int(os.environ.get("ARRAY_PROBE_PADDING"))))
                )
                # Partial probe coverage at end of call
                | (
                    (Target.start_position <= (int(end) + int(os.environ.get("ARRAY_PROBE_PADDING"))))
                    & (Target.end_position >= (int(end) + int(os.environ.get("ARRAY_PROBE_PADDING"))))
                )
            )
            .group_by(ArrayChip.name)
            .group_by(Library.name)
            .group_by(UploadJob.job_status)
        )

        return probe_query

    @classmethod
    def ngs_coverage_by_region(cls, chromosome, start, end):
        """
        Coverage Description:
        ---------------------
        NGS Probes are joined to targets in a Many-to-One relationship where many
        probes can correspond to a single target. Therefore, we must first join
        NGS Probes to Target Groups in order to query for coverage.
        """
        probe_query = (
            db.session.query(
                func.count(NgsProbe.id),
                NgsProbe.ngs_target_group_id,
                NgsProbeset.name,
                Library.name,
                UploadJob.job_status,
            )
            .join(NgsTargetGroup, NgsProbe.ngs_target_group)
            .join(NgsProbeset, NgsProbe.ngs_probeset)
            .join(Target, NgsTargetGroup.target)
            .join(Library, NgsProbeset.libraries)
            .join(UploadJob, NgsProbeset.upload_job)
            .filter(Target.chromosome == chromosome)
            .filter(Target.start_position >= int(start))
            .filter(Target.end_position <= int(end))
            .group_by(NgsProbe.ngs_target_group_id)
            .group_by(NgsProbeset.name)
            .group_by(Library.name)
            .group_by(UploadJob.job_status)
        )
        return probe_query

    @classmethod
    def mlpa_coverage_by_region(cls, chromosome, start, end):
        """
        Coverage Description:
        ---------------------
        MLPA Probes cover a region of interest that is the union of all Probe designs. Coverage is
        determined if the target call region surrounds the entire probe.
        """
        probe_query = (
            db.session.query(func.count(MlpaProbe.id), MlpaKit.name, Library.name, UploadJob.job_status)
            .join(MlpaKit, MlpaProbe.mlpa_kit)
            .join(Target, MlpaProbe.target)
            .join(Library, MlpaKit.libraries)
            .join(Upload, MlpaKit.upload)
            .join(UploadJob, Upload.job)
            .filter(Target.chromosome == chromosome)
            .filter(
                # Partial probe coverage at start of call
                (
                    (Target.start_position <= (int(start) - int(os.environ.get("MLPA_PROBE_PADDING"))))
                    & (Target.end_position >= (int(start) - int(os.environ.get("MLPA_PROBE_PADDING"))))
                )
                # Call surrounds Probe
                | (
                    (Target.start_position >= (int(start) - int(os.environ.get("MLPA_PROBE_PADDING"))))
                    & (Target.end_position <= (int(end) + int(os.environ.get("MLPA_PROBE_PADDING"))))
                )
                # Probe surrounds call
                | (
                    (Target.start_position <= (int(start) - int(os.environ.get("MLPA_PROBE_PADDING"))))
                    & (Target.end_position >= (int(end) + int(os.environ.get("MLPA_PROBE_PADDING"))))
                )
                # Partial probe coverage at end of call
                | (
                    (Target.start_position <= (int(end) + int(os.environ.get("MLPA_PROBE_PADDING"))))
                    & (Target.end_position >= (int(end) + int(os.environ.get("MLPA_PROBE_PADDING"))))
                )
            )
            .group_by(MlpaKit.name)
            .group_by(Library.name)
            .group_by(UploadJob.job_status)
        )

        return probe_query

    @classmethod
    def snp_coverage_by_region(cls, chromosome, start, end):
        """
        SNP Coverage still needs to be implemented
        """
        return None
