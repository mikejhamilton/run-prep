from run_prep.core.services.base import BaseService
from run_prep.models.library import Library
from run_prep.models.mlpa_kit import MlpaKit
from run_prep.models.ngs_probeset import NgsProbeset
from run_prep.models.upload import Upload
from run_prep.models.upload_job import UploadJob
from run_prep.core.extensions.db import db
from run_prep.models.array_chip import ArrayChip

__all__ = ("LibraryConnectionService",)


class LibraryConnectionService(BaseService):
    def start(self, app):
        self.app = app

    @classmethod
    def get_name(cls):
        return "library_connection"

    @staticmethod
    def get_libraries_from_job(job):
        """
            Returns list of libraries from a given job.
        """
        libraries = []
        if job.technology == "Targeted MicroArray":
            libraries = job.array_chips.libraries
        elif job.technology == "MLPA":
            for upload in job.uploads:
                if upload.upload_type == "MLPA Design":
                    libraries = upload.mlpa_kits.libraries
                    break
        elif job.technology == "NGS":
            libraries = job.ngs_probesets.libraries
        return libraries

    @staticmethod
    def get_upload_jobs_with_libraries():
        """
        Returns query to get all upload jobs joined with their respective probe group and libraries.  

        Probe upload jobs will result in creating either an Array Chip, Mlpa Kit or NGS Probeset.  
        The libraries are stored on the probe group (chip, sets, kits). UploadJobs must be joined  
        with their probe group and unioned together to get all jobs.
        """
        chips = (
            db.session.query(UploadJob)
            .join(ArrayChip, UploadJob.id == ArrayChip.upload_job_id, isouter=True)
            .join(Library, ArrayChip.libraries, isouter=True)
        )
        sets = (
            db.session.query(UploadJob)
            .join(NgsProbeset, UploadJob.id == NgsProbeset.upload_job_id, isouter=True)
            .join(Library, NgsProbeset.libraries, isouter=True)
        )
        kits = (
            db.session.query(UploadJob)
            .join(Upload, UploadJob.id == Upload.job_id, isouter=True)
            .join(MlpaKit, Upload.id == MlpaKit.upload_id, isouter=True)
            .join(Library, MlpaKit.libraries, isouter=True)
        )
        query = chips.union(sets).union(kits)
        return query
