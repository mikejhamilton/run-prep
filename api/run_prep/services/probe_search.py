from run_prep.core.services.base import BaseService
from run_prep.models.targeted_array_probe import TargetedArrayProbe
from run_prep.models.array_chip import ArrayChip
from run_prep.models.mlpa_probe import MlpaProbe
from run_prep.models.mlpa_probe import MlpaKit
from run_prep.models.ngs_probe import NgsProbe
from run_prep.models.ngs_probeset import NgsProbeset
from run_prep.models.upload_job import UploadJob
from run_prep.models.upload import Upload
from run_prep.core.containers.service import services
from run_prep.core.extensions.db import db
from sqlalchemy import func

__all__ = ("ProbeSearchService",)


class ProbeSearchService(BaseService):

    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "probe_search"
    
    @classmethod
    def find_mlpa_by_job_paginated(cls, job_id, page, size):
        return db.session.query(MlpaProbe) \
            .join(MlpaKit, MlpaProbe.mlpa_kit) \
            .join(Upload, MlpaKit.upload) \
            .join(UploadJob, Upload.job) \
            .filter(Upload.job_id == job_id) \
            .order_by(MlpaProbe.id.desc()) \
            .paginate(page, per_page=int(size))

    @classmethod
    def find_targeted_array_by_job_paginated(cls, job_id, page, size):
        return db.session.query(TargetedArrayProbe) \
            .join(ArrayChip, TargetedArrayProbe.chip) \
            .join(UploadJob, ArrayChip.upload_job) \
            .filter(ArrayChip.upload_job_id == job_id) \
            .order_by(ArrayChip.id.desc()) \
            .paginate(page, per_page=int(size))

    @classmethod
    def find_ngs_by_job_paginated(cls, job_id, page, size):
        return db.session.query(NgsProbe) \
            .join(NgsProbeset, NgsProbe.ngs_probeset) \
            .join(UploadJob, NgsProbeset.upload_job) \
            .filter(NgsProbeset.upload_job_id == job_id) \
            .order_by(NgsProbeset.id.desc()) \
            .paginate(page, per_page=int(size))
