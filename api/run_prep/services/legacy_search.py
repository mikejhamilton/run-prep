from run_prep.core.services.base import BaseService
from run_prep.core.extensions.db import db
from run_prep.models.amplicon import Amplicon
from run_prep.models.amplicon_version import AmpliconVersion
from run_prep.models.sanger_primer_pair import SangerPrimerPair
from run_prep.models.sanger_primer import SangerPrimer
from run_prep.models.design import Design
from werkzeug.exceptions import BadRequest
from sqlalchemy.orm import aliased

__all__ = ("LegacySearchService",)


class LegacySearchService(BaseService):
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "legacy_search"

    def fetch_all_unique_genes(self):
        return db.session.query(Amplicon.gene_symbol).distinct()

    def __primer_is_pcr(self, primer):
        primer_result = primer.one_or_none()
        if primer_result is None:
            return False
        elif primer_result.step == "PCR":
            return True
        return False

    def fetch_pcr_primer_pair_by_individual_names(self, forward_primer_name, reverse_primer_name):

        forward_primer = db.session.query(SangerPrimer).filter(SangerPrimer.sanger_primer_name == forward_primer_name)

        reverse_primer = db.session.query(SangerPrimer).filter(SangerPrimer.sanger_primer_name == reverse_primer_name)

        if not self.__primer_is_pcr(forward_primer) or not self.__primer_is_pcr(reverse_primer):
            raise BadRequest(
                f"Search for pcr primer pairs was given non-pcr primers: {forward_primer_name}, {reverse_primer_name}"
            )

        sangerPrimerF = aliased(SangerPrimer)
        sangerPrimerR = aliased(SangerPrimer)

        return (
            db.session.query(AmpliconVersion)
            .join(SangerPrimerPair, AmpliconVersion.primer_pairs)
            .join(sangerPrimerF, SangerPrimerPair.forward_sanger_primer)
            .join(sangerPrimerR, SangerPrimerPair.reverse_sanger_primer)
            .filter(
                (sangerPrimerF.sanger_primer_name == forward_primer_name)
                & (sangerPrimerR.sanger_primer_name == reverse_primer_name)
            )
        )

    def fetch_pcr_primer_pair_by_associated_primers(self, associated_primer):
        forward_match_avs = (
            db.session.query(AmpliconVersion)
            .join(SangerPrimerPair, AmpliconVersion.primer_pairs)
            .join(SangerPrimer, SangerPrimerPair.forward_sanger_primer)
            .filter(SangerPrimer.sanger_primer_name == associated_primer)
        )

        reverse_match_avs = (
            db.session.query(AmpliconVersion)
            .join(SangerPrimerPair, AmpliconVersion.primer_pairs)
            .join(SangerPrimer, SangerPrimerPair.reverse_sanger_primer)
            .filter(SangerPrimer.sanger_primer_name == associated_primer)
        )

        pair_match_avs = db.session.query(AmpliconVersion).filter(
            AmpliconVersion.pcr_primer_pair_name == associated_primer
        )

        return forward_match_avs.union(reverse_match_avs).union(pair_match_avs)

    def fetch_primer_design_by_amplicon_version(self, amplicon_version_name):
        forward_primers = (
            db.session.query(SangerPrimer, Design)
            .join(SangerPrimerPair, AmpliconVersion.primer_pairs)
            .join(SangerPrimer, SangerPrimerPair.forward_sanger_primer)
            .join(Design, SangerPrimer.design)
            .filter(AmpliconVersion.amplicon_version_name.like(amplicon_version_name + "%"))
        )

        reverse_primers = (
            db.session.query(SangerPrimer, Design)
            .join(SangerPrimerPair, AmpliconVersion.primer_pairs)
            .join(SangerPrimer, SangerPrimerPair.reverse_sanger_primer)
            .join(Design, SangerPrimer.design)
            .filter(AmpliconVersion.amplicon_version_name.like(amplicon_version_name + "%"))
        )

        return forward_primers.union(reverse_primers)

    def fetch_amplicon_version_by_primer_pair_name(self, primer_pair_name):
        forward_avs = (
            db.session.query(AmpliconVersion)
            .join(SangerPrimerPair, AmpliconVersion.primer_pairs)
            .join(SangerPrimer, SangerPrimerPair.forward_sanger_primer)
            .filter(AmpliconVersion.pcr_primer_pair_name.like(primer_pair_name + "%"))
        )

        reverse_avs = (
            db.session.query(AmpliconVersion)
            .join(SangerPrimerPair, AmpliconVersion.primer_pairs)
            .join(SangerPrimer, SangerPrimerPair.reverse_sanger_primer)
            .filter(AmpliconVersion.pcr_primer_pair_name.like(primer_pair_name + "%"))
        )

        return forward_avs.union(reverse_avs)

    def fetch_primer_design_by_name(self, names):
        forward_designs = (
            db.session.query(AmpliconVersion)
            .join(SangerPrimerPair, AmpliconVersion.primer_pairs)
            .join(SangerPrimer, SangerPrimerPair.forward_sanger_primer)
            .join(Design, SangerPrimer.design)
            .filter(SangerPrimer.sanger_primer_name.in_(names))
            .group_by(AmpliconVersion)
        )

        reverse_designs = (
            db.session.query(AmpliconVersion)
            .join(SangerPrimerPair, AmpliconVersion.primer_pairs)
            .join(SangerPrimer, SangerPrimerPair.reverse_sanger_primer)
            .join(Design, SangerPrimer.design)
            .filter(SangerPrimer.sanger_primer_name.in_(names))
            .group_by(AmpliconVersion)
        )

        return forward_designs.union(reverse_designs)

    def fetch_amplicon_version_by_name(self, name):
        return db.session.query(AmpliconVersion).filter(AmpliconVersion.amplicon_version_name == name)

    def fetch_amplicon_version_by_gene(self, gene):
        return (
            db.session.query(AmpliconVersion)
            .join(Amplicon, AmpliconVersion.amplicon)
            .filter(Amplicon.gene_symbol == gene)
        )
