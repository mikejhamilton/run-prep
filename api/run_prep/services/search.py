from run_prep.core.containers.service import services
from run_prep.core.extensions.db import db
from run_prep.core.services.base import BaseService
from run_prep.models.amplicon import Amplicon
from run_prep.models.amplicon_version import AmpliconVersion
from run_prep.models.comment import Comment
from run_prep.models.design import Design
from run_prep.models.rtpcr_amplicon_version import RtpcrAmpliconVersion
from run_prep.models.sanger_primer import SangerPrimer
from run_prep.models.sanger_primer_pair import SangerPrimerPair
from run_prep.models.upload_job import UploadJob
from sqlalchemy import and_, asc, desc, or_
from sqlalchemy.orm import aliased
from sqlalchemy.sql.expression import ClauseList

__all__ = ("SearchService",)

SangerPrimerF1 = aliased(SangerPrimer)
SangerPrimerR1 = aliased(SangerPrimer)
SangerPrimerF2 = aliased(SangerPrimer)
SangerPrimerR2 = aliased(SangerPrimer)
SangerPrimerPair1 = aliased(SangerPrimerPair)
SangerPrimerPair2 = aliased(SangerPrimerPair)
Design1 = aliased(Design)
Design2 = aliased(Design)
Design3 = aliased(Design)
Design4 = aliased(Design)

class SearchService(BaseService):
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "search"

    def get_primer_by_direction_and_step(cls, primer_pairs, direction, step):
        """
        Given a list of primer pairs, return the primer for the given direction and step.
        Typically used to find a certain primer from a given amplicon version.
        Example:
            get_primer_by_direction_and_step(Amplicon.primer_pairs, 'forward', 'PCR')
        """
        for primer_pair in primer_pairs:
            if (
                direction == "forward"
                and primer_pair.forward_sanger_primer.step == step
            ):
                return primer_pair.forward_sanger_primer
            elif (
                direction == "reverse"
                and primer_pair.reverse_sanger_primer.step == step
            ):
                return primer_pair.reverse_sanger_primer
        else:
            return None

    def get_amplicon_versions_by_filters(
        self, filter_dict, order_field=None, is_desc=False
    ):
        """
        Returns amplicon versions based on filters passed in through a filter dictionary.
        Example dict: {
            Gene: {
                Contains: [BLM]
            },
            Amplicon: {
                Contains: [BLM 2, BLM 4]
                Excludes: [BLM 5]
            }
        }
        """
        filter_columns = {
            "Gene": Amplicon.gene_symbol,
            "Amplicon": AmpliconVersion.amplicon_version_name,
            "PCR Primer Pair Name": AmpliconVersion.pcr_primer_pair_name,
            "Priority": AmpliconVersion.priority,
            "Validation Status": AmpliconVersion.validation_status,
        }
        filters = self.generate_filters(filter_columns, filter_dict)

        if order_field is None:
            if "General" in filter_dict.keys():
                return (
                    _get_amplicon_version_join_query(full_joins=True).filter(*filters)
                )
            else:
                return (
                    _get_amplicon_version_join_query(full_joins=False).filter(*filters)
                )


        direction = desc if is_desc else asc

        sort_columns = {
            "amplicon.gene_symbol": direction(Amplicon.gene_symbol),
            "isoform_version": direction(AmpliconVersion.isoform_version),
            "pcr_primer_pair_name": direction(AmpliconVersion.pcr_primer_pair_name),
            "c_range": direction(AmpliconVersion.c_dot_start),
            "priority": direction(AmpliconVersion.priority),
            "validation_status": direction(AmpliconVersion.validation_status),
        }

        if "General" in filter_dict.keys():
            services.log.info(f"Ordering by {order_field, sort_columns[order_field]}")
            return (
                _get_amplicon_version_join_query(full_joins=True).filter(*filters).order_by(sort_columns[order_field])
            )
        else:
            return (
                _get_amplicon_version_join_query(full_joins=False).filter(*filters).order_by(sort_columns[order_field])
            )

    def get_rtpcr_amplicon_versions_by_filters(
        self, filter_dict, order_field=None, is_desc=False
    ):
        """
        Returns rt-pcr amplicon versions based on filters passed in through a filter dictionary.
        Example dict: {
            Gene: {
                Contains: [BLM]
            },
            Amplicon: {
                Contains: [BLM 2, BLM 4]
                Excludes: [BLM 5]
            }
        }
        """
        filter_columns = {
            "Gene": Amplicon.gene_symbol,
            "Amplicon": RtpcrAmpliconVersion.amplicon_version_name,
            "PCR Primer Pair Name": RtpcrAmpliconVersion.pcr_primer_pair_name,
            "Priority": RtpcrAmpliconVersion.priority,
            "Validation Status": RtpcrAmpliconVersion.validation_status,
        }
        filters = self.generate_filters(filter_columns, filter_dict)

        if order_field is None:
            return RtpcrAmpliconVersion.query.join(Amplicon).filter(*filters)

        direction = desc if is_desc else asc

        sort_columns = {
            "amplicon.gene_symbol": direction(Amplicon.gene_symbol),
            "isoform_version": direction(RtpcrAmpliconVersion.isoform_version),
            "pcr_primer_pair_name": direction(
                RtpcrAmpliconVersion.pcr_primer_pair_name
            ),
            "c_range": direction(RtpcrAmpliconVersion.c_dot_start),
            "priority": direction(RtpcrAmpliconVersion.priority),
            "validation_status": direction(RtpcrAmpliconVersion.validation_status),
        }

        return (
            RtpcrAmpliconVersion.query.join(Amplicon)
            .filter(*filters)
            .order_by(sort_columns[order_field])
        )

    def filter_nearest_rtpcr_amplicon_by_midpoint(self, start, end, ampliconList):
        """
        Returns rt-pcr amplicon version that is nearest to the midpoint between start and end.
        """
        midpoint = ((end - start) / 2) + start

        result = None
        distance = -1

        for amplicon in ampliconList:
            currentMidpoint = (
                (amplicon.targets[0].end_position - amplicon.targets[0].start_position)
                / 2
            ) + amplicon.targets[0].start_position
            currentDistance = abs(midpoint - currentMidpoint)
            if (
                distance == -1
                or distance > currentDistance
                or (
                    distance == currentDistance
                    and result.amplicon_sample_id > amplicon.amplicon_sample_id
                )
            ):
                result = amplicon
                distance = currentDistance

        return result

    def get_entity_by_bootstrap_filters(
        self, entity, filters_dict, sortField, sortOrder
    ):
        """
        Returns a query filtered by a specific entity. Filters a provided by bootstrap props
        """
        filters = []
        for data_field, conditions in filters_dict.items():
            for condition, values in conditions.items():
                for value in values:
                    if condition == "=":
                        filters.append(getattr(entity, data_field) == value)
                    elif condition == "LIKE":
                        filters.append(getattr(entity, data_field).ilike(f"%{value}%"))
        filtered_queries = entity.query.filter(*filters)
        if sortField and sortOrder:
            column_sort_func = services.attribute.rgetattr(
                entity, sortField + "." + sortOrder
            )
            filtered_queries = filtered_queries.order_by(column_sort_func())
        return filtered_queries

    @staticmethod
    def generate_filters(filter_columns: [], filter_dict: {}) -> []:
        filters = []
        for f, conditions in filter_dict.items():
            for condition, values in conditions.items():
                for value in values:
                    if condition == "Contains":
                        if f == "General":
                            filters.append(
                                or_(
                                    AmpliconVersion.amplicon_version_name.ilike(f"%{value}%"),
                                    AmpliconVersion.pcr_primer_pair_name.ilike(f"%{value}%"),
                                    AmpliconVersion.isoform_version.ilike(f"%{value}%"),
                                    SangerPrimerF1.sanger_primer_name.ilike(f"%{value}%"),
                                    SangerPrimerR1.sanger_primer_name.ilike(f"%{value}%"),
                                    SangerPrimerF2.sanger_primer_name.ilike(f"%{value}%"),
                                    SangerPrimerR2.sanger_primer_name.ilike(f"%{value}%"),
                                    Design1.sequence.ilike(f"%{value}%"),
                                    Design2.sequence.ilike(f"%{value}%"),
                                    Design3.sequence.ilike(f"%{value}%"),
                                    Design4.sequence.ilike(f"%{value}%"),
                                    Comment.message.ilike(f"%{value}%"),
                                )
                            )
                        else:
                            filters.append(filter_columns[f].ilike(f"%{value}%"))
                    elif condition == "Exactly":
                        if f == "General":
                            filters.append(
                                or_(
                                    AmpliconVersion.amplicon_version_name==value,
                                    AmpliconVersion.pcr_primer_pair_name==value,
                                    AmpliconVersion.isoform_version==value,
                                    SangerPrimerF1.sanger_primer_name==value,
                                    SangerPrimerR1.sanger_primer_name==value,
                                    SangerPrimerF2.sanger_primer_name==value,
                                    SangerPrimerR2.sanger_primer_name==value,
                                    Design1.sequence==value,
                                    Design2.sequence==value,
                                    Design3.sequence==value,
                                    Design4.sequence==value,
                                    Comment.message==value,
                                )
                            )
                        else:
                            filters.append(filter_columns[f] == value)
                    elif condition == "Excludes":
                        if f == "General":
                            filters.append(
                                and_(
                                    AmpliconVersion.amplicon_version_name.notilike(f"%{value}%"),
                                    AmpliconVersion.pcr_primer_pair_name.notilike(f"%{value}%"),
                                    AmpliconVersion.isoform_version.notilike(f"%{value}%"),
                                    SangerPrimerF1.sanger_primer_name.notilike(f"%{value}%"),
                                    SangerPrimerR1.sanger_primer_name.notilike(f"%{value}%"),
                                    SangerPrimerF2.sanger_primer_name.notilike(f"%{value}%"),
                                    SangerPrimerR2.sanger_primer_name.notilike(f"%{value}%"),
                                    Design1.sequence.notilike(f"%{value}%"),
                                    Design2.sequence.notilike(f"%{value}%"),
                                    Design3.sequence.notilike(f"%{value}%"),
                                    Design4.sequence.notilike(f"%{value}%"),
                                    Comment.message.notilike(f"%{value}%"),
                                )
                            )
                        else:
                            filters.append(filter_columns[f].notlike(f"%{value}%"))
        return filters


def  _get_amplicon_version_join_query(full_joins=False):
    """Helper function for getting amplicon verison query."""
    if not full_joins:
        return AmpliconVersion.query.join(Amplicon)
    else:
        return (
                    AmpliconVersion.query
                        .join(Amplicon, AmpliconVersion.amplicon)
                        .join(Comment, AmpliconVersion.comments, isouter=True)
                        .join(SangerPrimerPair1, AmpliconVersion.primer_pairs, isouter=True)
                        .join(SangerPrimerPair2, AmpliconVersion.primer_pairs, isouter=True)
                        .join(SangerPrimerF1, SangerPrimerPair1.forward_sanger_primer, isouter=True)
                        .join(SangerPrimerR1, SangerPrimerPair1.reverse_sanger_primer, isouter=True)
                        .join(SangerPrimerF2, SangerPrimerPair2.forward_sanger_primer, isouter=True)
                        .join(SangerPrimerR2, SangerPrimerPair2.reverse_sanger_primer, isouter=True)
                        .join(Design1, SangerPrimerF1.design, isouter=True)
                        .join(Design2, SangerPrimerR1.design, isouter=True)
                        .join(Design3, SangerPrimerF2.design, isouter=True)
                        .join(Design4, SangerPrimerR2.design, isouter=True)
                        .distinct()
        )
