from run_prep.core.services.base import BaseService
from run_prep.core.containers.service import services
import requests
from flask import current_app
import contextlib

__all__ = ("ExternalAPIService",)

class ExternalAPIService(BaseService):

    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "external_apis"

    def get_gene_from_biotools_gene2isoforms(self, gene):
        return requests.get(f"{current_app.config['biotools_url']}/api/launch/gene2isoforms.json?genes={gene}")

    def get_gene_from_biotools_gene_info(self, gene):
        return requests.get(f"{current_app.config['biotools_url']}/api/genepage/gene_info/fetch_by_name.json?gene={gene}")

    def submit_job_to_bioinformatics(self, files, job_id, technology, full_overlap=False):

        params = {"job_id": job_id, "full_overlap": full_overlap}
        services.log.info(f"Submitting files to BI for job: {job_id}")
        bi_response = requests.post(f"{current_app.config['bioinformatics_url']}{technology}_upload", params=params, files=files)
        services.log.info(f"Response from BI: {bi_response}")
