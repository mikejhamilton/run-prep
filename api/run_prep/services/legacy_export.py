from run_prep.core.services.base import BaseService
from run_prep.models.sanger_primer_pair import SangerPrimerPair
from run_prep.core.containers.service import services

__all__ = ("LegacyExportService",)


class LegacyExportService(BaseService):
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "legacy_export"

    def generate_amplicons(self, amplicon_versions):
        """
        Genertor function to return a header and rows of amplicons versions.
        """
        yield "\t".join(
            [
                "Gene",
                "Amplicon",
                "PCR Methods",
                "PCR Temp",
                "Forward PCR Name",
                "Forward PCR Sequence",
                "Reverse PCR Name",
                "Reverse PCR Sequence",
                "Forward BDT Name",
                "Forward BDT Sequence",
                "Reverse BDT Name",
                "Reverse BDT Sequence",
                "Sequencing Direction",
                "Status",
                "Betaine",
                "DMSO",
                "Priority",
            ]
        ) + "\n"
        for row in amplicon_versions:
            primer_dict = dict()
            for primer_data in [
                ("forward_pcr", "forward", "PCR"),
                ("reverse_pcr", "reverse", "PCR"),
                ("forward_bdt", "forward", "BDT"),
                ("reverse_bdt", "reverse", "BDT"),
            ]:
                primer = services.search.get_primer_by_direction_and_step(
                    row.primer_pairs, primer_data[1], primer_data[2]
                )
                primer_dict[primer_data[0]] = primer

            row_data = [
                row.amplicon.gene_symbol,
                row.amplicon.exon + (f"v{row.version_number}" if row.version_number > 1 else ""),
                row.pcr_method or "",
                row.thermo_program or "",
                getattr(primer_dict["forward_pcr"], "sanger_primer_name", ""),
                services.attribute.rgetattr(primer_dict["forward_pcr"], "design.sequence", ""),
                getattr(primer_dict["reverse_pcr"], "sanger_primer_name", ""),
                services.attribute.rgetattr(primer_dict["reverse_pcr"], "design.sequence", ""),
                getattr(primer_dict["forward_bdt"], "sanger_primer_name", ""),
                services.attribute.rgetattr(primer_dict["forward_bdt"], "design.sequence", ""),
                getattr(primer_dict["reverse_bdt"], "sanger_primer_name", ""),
                services.attribute.rgetattr(primer_dict["reverse_bdt"], "design.sequence", ""),
                row.sequencing_direction,
                row.validation_status,
                row.betaine,
                row.dmso,
                row.priority,
            ]
            yield "\t".join([str(data) for data in row_data]) + "\n"

    def generate_rtpcr_amplicons(self, rtpcr_amplicon_versions):
        """
        Genertor function to return a header and rows of amplicons versions.
        """
        yield "\t".join(
            [
                "Gene",
                "Amplicon",
                "Amplicon Sample ID",
                "Primer Pair Name",
                "Forward Name",
                "Forward Sequence",
                "Forward Coordinates",
                "Forward Coordinates (Junction)",
                "Reverse Name",
                "Reverse Sequence",
                "Reverse Coordinates",
                "Reverse Coordinates (Junction)",
                "Status",
                "Betaine",
                "Priority",
            ]
        ) + "\n"
        for row in rtpcr_amplicon_versions:
            primer_dict = dict()
            for rp in row.rtpcr_primers:
                for direction in ['forward', 'reverse']:
                    design = rp.designs[0]
                    if design.direction == direction:
                        primer_dict[direction] = {
                            'name': rp.name,
                            'seq': design.sequence,
                            'coord': f'{design.chromosome!s}:{design.start_position}-{design.end_position}',
                            'coord_j': ''
                        }
                        # Add junction coord if second design present
                        if len(rp.designs) > 1:
                            design_j = rp.designs[1]
                            primer_dict[direction]['coord_j'] = f'{design_j.chromosome!s}:{design_j.start_position}-{design_j.end_position}'
            row_data = [
                row.amplicon.gene_symbol,
                row.amplicon.exon + (f"v{row.version_number}" if row.version_number > 1 else ""),
                row.amplicon_sample_id,
                row.pcr_primer_pair_name,
                primer_dict['forward']['name'],
                primer_dict['forward']['seq'],
                primer_dict['forward']['coord'],
                primer_dict['forward']['coord_j'],
                primer_dict['reverse']['name'],
                primer_dict['reverse']['seq'],
                primer_dict['reverse']['coord'],
                primer_dict['reverse']['coord_j'],
                row.validation_status,
                row.betaine,
                row.priority,
                ]
            yield "\t".join([str(data) for data in row_data]) + "\n"
