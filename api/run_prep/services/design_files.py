import os

from run_prep.constants.exceptions import DesignFileUploadError, UnknownJobIDError, MissingParameterError, UnknownStatusIDError
from run_prep.constants.job_statuses import UPLOADED, INITIALIZING, UPLOADED_SUPPLEMENTARY, BI_FAILED
from run_prep.core.containers.service import services
from run_prep.core.extensions.db import db
from run_prep.core.services.base import BaseService
from run_prep.models.array_chip import ArrayChip
from run_prep.models.mlpa_kit import MlpaKit 
from run_prep.models.ngs_probeset import NgsProbeset
from run_prep.models.upload import Upload
from run_prep.models.upload_job import UploadJob
from run_prep.models.library import Library

__all__ = ("DesignFileService",)


class DesignFileService(BaseService):

    def start(self, app):
        self.app = app

    @classmethod
    def get_name(cls):
        return "design_files"

    def start_ngs_upload(self, request, form_dict):
        """
        Store NGS form data and start background job to parse the ngs files.
        Assumes the following 4 files are uploaded in the form data
            -target_file
            -coordinatee_file
            -sequence_file
        Creates Upload entities for each file and a job entity for the entire upload
        Returns a the Job ID Created.
        """
        file_types = {
            "NGS Target": {"accept": [".bed"], "id": "target_file"},
            "NGS Isoform": {"accept": [".txt"], "id": "isoform_file"},
            "NGS Probe Coordinate": {"accept": [".bed"], "id": "coordinate_file"},
            "NGS Probe Sequence": {"accept": [".xls", ".xlsx"], "id": "sequence_file"},
        }
        missing_files = list(filter(lambda x: file_types[x]["id"] not in request.files, file_types))
        if missing_files:
            raise DesignFileUploadError(f"Missing file {missing_files} from NGS Upload.")
        upload_path = os.path.join(self.app.config["FILE_UPLOAD_PATH"], "probes")
        job = UploadJob(
            technology=form_dict["technology"],
            number_entries_created=0,
            number_of_files=0,
            number_errors_encountered=0,
            job_status=INITIALIZING,
        )
        db.session.add(job)
        db.session.flush()
        services.log.info(f"Created upload job entitiy: {job.id}")
        for file_type, file_dict in file_types.items():
            services.file.validate_extension(request.files[file_dict["id"]], file_dict["accept"])
            Upload.create_upload(request.files[file_dict["id"]], upload_path, job.id, file_type)
            job.number_of_files += 1
        probeset = NgsProbeset(
            name="not-set",
            upload_job_id=job.id,
        )
        libraries = form_dict["library"].split(',')
        for name in libraries:
            lib = Library.find_or_create(name)
            probeset.libraries.append(lib)
        db.session.add(probeset)
        job.job_status = UPLOADED
        db.session.add(job)
        db.session.commit()
        return job

    def start_mlpa_upload(self, request, form_dict):
        """
        Store mlpa form data and start background job to parse the mlpa files.
        Assumes there is at least one, but normally multiple sequence files being uploaded.
        Creates Upload entities for each sequence file and a job entity for the entire upload
        """
        if not request.files:
            raise DesignFileUploadError("No files provided to MLPA Probe upload.")
        if "isoform_file" not in request.files:
            raise DesignFileUploadError("Mising MLPA isoform file.")
        if "isoform_file" in request.files and len(request.files) == 1:
            raise DesignFileUploadError("Missing MLPA probe sequence file.")

        upload_path = os.path.join(self.app.config["FILE_UPLOAD_PATH"], "probes")
        job = UploadJob(
            technology=form_dict["technology"],
            number_entries_created=0,
            number_of_files=0,
            number_errors_encountered=0,
            job_status=INITIALIZING,
        )
        db.session.add(job)
        db.session.flush()
        services.log.info(f"Created upload job entity: {job.id}")

        for key, f in request.files.items():
            if key == "isoform_file":
                services.file.validate_extension(f, [".txt"])
                upload = Upload.create_upload(f, upload_path, job.id, "MLPA Isoform")
                continue
            else:
                services.file.validate_extension(f, [".xls", ".xlsx"])
                upload = Upload.create_upload(f, upload_path, job.id, "MLPA Design")

            job.number_of_files += 1
            kit = MlpaKit(
                upload_id=upload.id,
                name=upload.original_filename.split(".")[0],
            )
            libraries = form_dict["library"].split(',')
            for name in libraries:
                lib = Library.find_or_create(name)
                kit.libraries.append(lib)
        db.session.add(kit)
        job.job_status = UPLOADED
        db.session.add(job)
        db.session.commit()
        return job

    def start_targeted_microarray_upload(self, request, form_dict):
        """
        Store microarray form data and start background job to parse the microarray files.
        Assumes the following file is uploaded in the form data
            -design_file
        Creates Upload entities for each file and a job entity for the entire upload
        """
        file_types = {
            "Targeted MicroArray Probe Sequence": {"accept": [".xml"], "id": "design_file"},
        }
        self.check_for_missing_files(request, form_dict, file_types)
        upload_path = os.path.join(self.app.config["FILE_UPLOAD_PATH"], "probes")
        job = UploadJob(
            technology=form_dict["technology"],
            number_entries_created=0,
            number_of_files=0,
            number_errors_encountered=0,
            job_status=INITIALIZING,
        )
        db.session.add(job)
        db.session.flush()
        services.log.info(f"Created upload job enitity: {job.id}")
        for file_type, file_dict in file_types.items():
            services.file.validate_extension(request.files[file_dict["id"]], file_dict["accept"])
            Upload.create_upload(request.files[file_dict["id"]], upload_path, job.id, file_type)
            job.number_of_files += 1
        array_chip = ArrayChip(
            name="not-set",
            upload_job_id=job.id,
        )
        libraries = form_dict["library"].split(',')
        for name in libraries:
            lib = Library.find_or_create(name)
            array_chip.libraries.append(lib)
        db.session.add(array_chip)
        job.job_status = UPLOADED
        db.session.add(job)
        db.session.commit()
        return job

    def start_ngs_supplementary_upload(self, request, form_dict):
        """
        Creates Upload entities for each file and a job entity for the entire upload
        """

        file_types = {
            "NGS Supplementary": {"accept": [".bed"], "id": "file"},
        }
        job_id = request.form.get("job_id", None)
        status = request.form.get("status", None)
        if job_id is None:
            raise UnknownJobIDError(f"Could not find a job id for request {request}")
        if status is None:
            raise UnknownStatusIDError(f"Could not find status for request {request}")

        upload_path = os.path.join(self.app.config["FILE_UPLOAD_PATH"], "probes")
        job = UploadJob.query.filter(UploadJob.id == job_id).one()

        # If error from BI endpoint, set job status to failed.
        if "success" not in status.lower():
            job.job_status = BI_FAILED
            db.session.add(job)
            db.session.commit()
            services.log.error(f"Failed BI Upload. Status: {status}")
            exc = request.form.get("exception", None)
            services.log.error(f"Exception from BI for job {job.id}: {exc}")
            services.log.info(f"Updated upload job enitity: {job.id}")
            return job

        # Upload BI Supplementary file and update job status
        for file_type, file_dict in file_types.items():
            services.file.validate_extension(request.files[file_dict["id"]], file_dict["accept"])
            Upload.create_upload(request.files[file_dict["id"]], upload_path, job.id, file_type)
            job.number_of_files += 1
        job.job_status = UPLOADED_SUPPLEMENTARY
        db.session.add(job)
        db.session.commit()
        services.log.info(f"Updated upload job enitity: {job.id}")
        return job

    def start_mlpa_supplementary_upload(self, request, form_dict):
        """
        Creates Upload entities for each file and a job entity for the entire upload
        """

        file_types = {
            "MLPA Supplementary": {"accept": [".bed"], "id": "file"},
        }
        job_id = request.form.get("job_id", None)
        status = request.form.get("status", None)
        if job_id is None:
            raise UnknownJobIDError(f"Could not find a job id for request {request}")
        if status is None:
            raise UnknownStatusIDError(f"Could not find status for request {request}")

        upload_path = os.path.join(self.app.config["FILE_UPLOAD_PATH"], "probes")
        job = UploadJob.query.filter(UploadJob.id == job_id).one()

        # If error from BI endpoint, set job status to failed.
        if "success" not in status.lower():
            job.job_status = BI_FAILED
            db.session.add(job)
            db.session.commit()
            services.log.error(f"Failed BI Upload. Status: {status}")
            exc = request.form.get("exception", None)
            services.log.error(f"Exception from BI for job {job.id}: {exc}")
            services.log.info(f"Updated upload job enitity: {job.id}")
            return job

        # Upload BI Supplementary file and update job status    
        for file_type, file_dict in file_types.items():
            services.file.validate_extension(request.files[file_dict["id"]], file_dict["accept"])
            Upload.create_upload(request.files[file_dict["id"]], upload_path, job.id, file_type)
            job.number_of_files += 1
        job.job_status = UPLOADED_SUPPLEMENTARY
        db.session.add(job)
        db.session.commit()
        services.log.info(f"Updated upload job enitity: {job.id}")
        return job

    def check_for_missing_files(self, request, form_dict, file_types):
        missing_files = list(filter(lambda x: file_types[x]["id"] not in request.files, file_types))
        if missing_files:
            raise DesignFileUploadError(f"Missing file {missing_files} from Microarray Upload.")