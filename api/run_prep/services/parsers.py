from run_prep.core.services.base import BaseService
from run_prep.core.containers.service import services
from run_prep.constants.exceptions import UnknownQueryParamFilter
import re

__all__ = ("ParserService",)


class ParserService(BaseService):
    def start(self, app):
        pass

    @classmethod
    def get_name(cls):
        return "parser"

    def parse_genomic_coords(self, coords, strict=False):
        """Parses genomic coordinates and returns a dictionary of broken down coordinates.
        Example:
        'chr4:3,232-3,827' -> {'chromosome': chr4, 'start': 3232, 'end': 3827}
        """
        coord_regex = r"^(?P<chromosome>chr(\d+|[XY])):(?P<start>[\d\,]+)-(?P<end>[\d\,]+)$"
        match = re.match(coord_regex, coords)
        if not match:
            if strict:
                raise ValueError(f"Unable to parse genomic coordinates: {coords}")
            else:
                return {
                    "chromosome": "NA",
                    "start": -1,
                    "end": -1,
                }
        return {
            "chromosome": match.group("chromosome"),
            "start": match.group("start").replace(",", ""),
            "end": match.group("end").replace(",", ""),
        }

    def parse_LHS_bracket_query(self, args):
        """
        Parses request query parameters with filters and conditions into a dictionary.
        Design inspiration:
        https://www.moesif.com/blog/technical/api-design/REST-API-Design-Filtering-Sorting-and-Pagination/#lhs-brackets

        Example: filter[condition]=value
                 Gene[Contains]=BLM

        :param werkzeug.ImmutableMultiDict args: Dictionary of arguments from request
        :return: Dictionary of query strings broken down
                 Example: {
                     Filter1: {
                         Condition1: [value1, value2]
                         Condition2: [value1, value2]
                     }
                 }
        :rtype: dict
        """
        # Convert request query args to python dictionary
        # This step is helpful due to duplicate arguments stored in an Immutable multi dict.
        filter_args = {}
        for k in args.keys():
            filter_args[k] = args.getlist(k)
        # Drop paginate and sort arguments from filter query dictionary if they exist
        filter_args.pop('page', None)
        filter_args.pop('sizePerPage', None)
        filter_args.pop('sortField', None)
        filter_args.pop('sortOrder', None)

        # Drop order arguments from query dictionary if they exist
        filter_args.pop('orderBy', None)
        filter_args.pop('desc', None)

        filter_condition_regex = r'^(?P<filter>[\w ]+)\[(?P<condition>[\w= ]+)\]'
        dict = {}
        for k, v in filter_args.items():
            match = re.match(filter_condition_regex, k)
            if not match:
                raise UnknownQueryParamFilter(f"Unknown filter structure: {filter_args}")
            filter = match.group('filter')
            condition = match.group('condition')
            dict.setdefault(filter, {}).setdefault(condition, [])
            dict[filter][condition] = dict[filter][condition] + v
        return dict

    def remove_whitespace(self, *argv):
        """
        Parses request query parameters removes any leading/trailing whitespaces and returns them in a list.
       
        Example: {'    ATM 220 s  ','ATM 210 as3     '} -> ['ATM 220s','ATM 210 as3']

        :param werkzeug.ImmutableMultiDict args: Dictionary of arguments from request
        :return: List of query strings with leading/trailing whitespaces removed
        :rtype: list
        """
        # Convert request query args to python dictionary
        # This step is helpful due to duplicate arguments stored in an Immutable multi dict.
        result = []
        for arg in argv:
            result.append(arg.strip())

        return result