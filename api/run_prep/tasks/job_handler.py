from run_prep.core.tasks.base import BaseTask
from run_prep.models.upload_job import UploadJobSchema
from run_prep.core.extensions.db import db
from run_prep.core.containers.task import tasks
from run_prep.core.containers.service import services
from run_prep.constants.job_statuses import FAILED_NO_HANDLER, UPLOADED, UPLOADED_SUPPLEMENTARY 

__all__ = ("JobHandler",)

job_serializer = UploadJobSchema()

class JobHandler(BaseTask):

    @classmethod
    def get_name(cls):
        return "job_handler"

    @staticmethod
    def run(upload_job):
        job = job_serializer.load(upload_job, session=db.session)

        services.log.info(f"Attempting to schedule job handler for {upload_job['technology']} with status {upload_job['job_status']}")
        if upload_job["technology"] == "MLPA":
            if upload_job["job_status"] == UPLOADED:
                tasks.mlpa_ingest.schedule(upload_job)
                return
            elif upload_job["job_status"] == UPLOADED_SUPPLEMENTARY:
                tasks.mlpa_ingest_supplementary.schedule(upload_job)
                return
        elif upload_job["technology"] == "NGS":
            if upload_job["job_status"] == UPLOADED:
                tasks.ngs_ingest.schedule(upload_job)
                return
            elif upload_job["job_status"] == UPLOADED_SUPPLEMENTARY:
                tasks.ngs_ingest_supplementary.schedule(upload_job)
                return
        elif upload_job["technology"] == "Targeted MicroArray":
            tasks.targeted_array_ingest.schedule(upload_job)
            return
        elif upload_job["technology"] == "SNP MicroArray":
            pass

        services.log.warning(f"Found no handler for task: {upload_job}")
        job.job_status = FAILED_NO_HANDLER
        db.session.add(job)
        db.session.commit()