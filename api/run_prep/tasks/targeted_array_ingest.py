from run_prep.core.tasks.base import BaseTask
from run_prep.core.extensions.db import db
from run_prep.models.upload_job import UploadJob, UploadJobSchema
from run_prep.models.upload import Upload, UploadSchema
from run_prep.models.targeted_array_probe import TargetedArrayProbe
from run_prep.models.array_chip import ArrayChip
from run_prep.models.design import Design
from run_prep.models.target import Target
from run_prep.constants.array_types import SNP_ARRAY, TARGETED
from run_prep.constants.job_statuses import PENDING_REVIEW, PARSING_FAILED
from run_prep.core.containers.service import services
import xml.etree.ElementTree as ET
from celery.utils.log import get_task_logger
import traceback

__all__ = ("TargetedArrayIngestTask",)

job_serializer = UploadJobSchema()
log = get_task_logger(__name__)

class TargetedArrayIngestTask(BaseTask):

    @classmethod
    def get_name(cls):
        return "targeted_array_ingest"

    @staticmethod
    def run(upload_job):
        job = job_serializer.load(upload_job, session=db.session)
        services.log.info(f"Starting supplementary work on {upload_job}")
        try:
            upload = Upload.query.filter(Upload.job_id == job.id).one() 
            array_chip = ArrayChip.query.filter(ArrayChip.upload_job_id == job.id).one()
            array_chip.name = upload.original_filename.replace(".xml", "")

            db.session.add(array_chip)
            db.session.flush()

            tree = ET.parse(upload.local_location)
            root = tree.getroot()

            probes = []
            errors = 0

            for elem in root.findall("./pattern/reporter"):
                if "name" in elem.attrib.keys() and "active_sequence" in elem.attrib.keys() and "systematic_name" in elem.attrib.keys():
                    try:

                        d = elem.attrib
                        parsed_coords = services.parser.parse_genomic_coords(d["systematic_name"])
                        design = Design(
                            direction="forward",
                            sequence=d["active_sequence"],
                            chromosome=parsed_coords["chromosome"],
                            start_position=parsed_coords["start"],
                            end_position=parsed_coords["end"]
                        )
                        target = Target.find_or_create(
                            chromosome=parsed_coords["chromosome"],
                            start=parsed_coords["start"],
                            end=parsed_coords["end"]
                        )
                        db.session.add(design)
                        db.session.add(target)
                        db.session.flush()
                        probes.append(TargetedArrayProbe(
                            name=d["name"],
                            design_id=design.id,
                            array_chip_id=array_chip.id,
                            target_id = target.id,
                        ))

                    except Exception as e:
                        log.info(f"Encountered error parsing XML element {elem} in array_ingest job: {e}")
                        errors += 1
                
            db.session.add_all(probes)
            db.session.flush()
        
            job.number_entries_created = len(probes)
            job.number_errors_encountered = errors
            job.job_status = PENDING_REVIEW
            db.session.add(job)
            db.session.commit()
            services.log.info(f"Finished supplementary work on {upload_job}")
        except Exception:
            job.job_status = PARSING_FAILED
            db.session.add(job)
            db.session.commit()
            services.log.error(f"Faild to parse files for job: {upload_job}")
            services.log.error(traceback.format_exc())
