from run_prep.core.tasks.base import BaseTask
from run_prep.core.extensions.db import db
from run_prep.models.upload_job import UploadJob, UploadJobSchema
from run_prep.models.upload import Upload, UploadSchema
from run_prep.constants.job_statuses import SUBMITTED, SUBMIT_FAILED
from run_prep.core.containers.service import services
from celery.utils.log import worker_logger as log
import contextlib
import traceback

__all__ = ("NGSIngestTask",)

job_serializer = UploadJobSchema()

class NGSIngestTask(BaseTask):

    @classmethod
    def get_name(cls):
        return "ngs_ingest"

    @staticmethod
    def run(upload_job):
        job = job_serializer.load(upload_job, session=db.session)

        services.log.info(f"Starting work on {upload_job}")

        uploads = Upload.query.filter(Upload.job_id == job.id).all()

        files, close_file_method = _format_uploads_for_ngs(uploads)

        try:
            services.external_apis.submit_job_to_bioinformatics(files, job.id, "ngs")
        except Exception as e:
            services.log.error(f"Failed to submit job: {str(e)}")
            services.log.error(traceback.format_exc())
            job.job_status = SUBMIT_FAILED
        else:
            services.log.info(f"Submit Job to BI.")
            job.job_status = SUBMITTED
        finally:
            close_file_method()

        db.session.add(job)
        db.session.commit()

def _format_uploads_for_ngs(uploads):
    """Formats mlpa file uploads to the expected format for BioInformatics"""
    files = dict()
    with contextlib.ExitStack() as stack:
        for upload in uploads:
            file_obj = stack.enter_context(open(upload.local_location, "rb"))
            if upload.upload_type == "NGS Isoform":
                files["genefile"] = file_obj
            elif upload.upload_type == "NGS Probe Coordinate":
                files["probefile"] = file_obj
        close_files = stack.pop_all().close
        return files, close_files
