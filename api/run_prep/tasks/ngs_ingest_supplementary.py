import difflib
from collections import defaultdict

import pandas as pd
from run_prep.constants.exceptions import TargetProbeMatchError
from run_prep.constants.job_statuses import PENDING_REVIEW, SUBMIT_FAILED, SUBMITTED
from run_prep.constants.symbols import SYMBOLS_TO_DIRECTIONS
from run_prep.core.containers.service import services
from run_prep.core.extensions.db import db
from run_prep.core.tasks.base import BaseTask
from run_prep.models.design import Design
from run_prep.models.ngs_probe import NgsProbe
from run_prep.models.ngs_probeset import NgsProbeset
from run_prep.models.ngs_target_group import NgsTargetGroup
from run_prep.models.target import Target
from run_prep.models.upload import Upload, UploadSchema
from run_prep.models.upload_job import UploadJob, UploadJobSchema
from celery.utils.log import worker_logger as log

__all__ = ("NGSIngestSupplementaryTask",)

job_serializer = UploadJobSchema()

class NGSIngestSupplementaryTask(BaseTask):

    @classmethod
    def get_name(cls):
        return "ngs_ingest_supplementary"

    @staticmethod
    def run(upload_job):
        job = job_serializer.load(upload_job, session=db.session)

        services.log.info(f"Starting supplementary work on {upload_job}")

        supplementary = Upload.query.filter(Upload.job_id == job.id) \
                    .filter(Upload.upload_type == "NGS Supplementary") \
                    .first()
        coordinate = Upload.query.filter(Upload.job_id == job.id) \
                    .filter(Upload.upload_type == "NGS Probe Coordinate") \
                    .one()
        target = Upload.query.filter(Upload.job_id == job.id) \
                    .filter(Upload.upload_type == "NGS Target") \
                    .one()
        sequence = Upload.query.filter(Upload.job_id == job.id) \
                    .filter(Upload.upload_type == "NGS Probe Sequence") \
                    .one()
        
        probeset = NgsProbeset.query.filter(NgsProbeset.upload_job_id == job.id).one()

        supplementary = pd.read_table(supplementary.local_location)
        coordinate = pd.read_table(coordinate.local_location, header=None, names=["chromosome", "start", "end", "name"])
        target = pd.read_table(target.local_location, header=None, names=["chromosome", "start", "end", "name"])
        sequence = pd.read_excel(sequence.local_location)

        probe = supplementary.merge(coordinate, left_on="ngs_probe_id", right_on="name")
        probe = probe.merge(sequence, left_on="name", right_on="SequenceName")

        target_groups = dict()
        for idx, row in target.iterrows():
            target_obj = Target.find_or_create(
                chromosome=row["chromosome"],
                start=row["start"],
                end=row["end"]
            )
            db.session.flush()
            target_group = NgsTargetGroup(
                name=row["name"],
                target_id=target_obj.id
            )
            db.session.add(target_group)
            db.session.flush()
            target_groups[row["name"]] = target_group

        target_probe_matches = get_all_matching_targets(probe, target)

        probes = []
        for idx, row in probe.iterrows():
            match = get_single_matching_target(row["name"], target_probe_matches)
            if match is None:
                target_group_id = None
            else:
                target_group_id = target_groups[match].id
            design = Design.find_or_create(
                direction=SYMBOLS_TO_DIRECTIONS[row["strand"]],
                seq=get_clean_sequence(row["Sequence"]),
                chromosome=row["ngs_chromosome"],
                start=row["ngs_probe_start"],
                end=row["ngs_probe_end"],
            )
            db.session.add(design)
            db.session.flush()
            probe_obj = NgsProbe(
                name=row["name"],
                design_id=design.id,
                ngs_probeset_id=probeset.id,
                ngs_target_group_id=target_group_id,
                c_dot_start=get_clean_c_dot(row["c_var_start"]),
                c_dot_end=get_clean_c_dot(row["c_var_end"])
            )
            probes.append(probe_obj)
            job.number_entries_created += 1
        
        db.session.add_all(probes)
        job.job_status = PENDING_REVIEW
        db.session.add(job)
        db.session.commit()
        services.log.info(f"Finished supplementary work on {upload_job}")

def get_all_matching_targets(probe_df, target_df):
    matches = dict()
    for idx, row in target_df.iterrows():
        try:
            isoform, cds = get_isoform_and_cds_from_target(row["name"])
        except TargetProbeMatchError as e:
            services.log.warning(f"Failed to match target to probe: {str(e)}")
            matches[row["name"]] = False
        else:
            matches[row["name"]] = (
                probe_df["name"].str.contains(isoform) & probe_df["name"].str.contains(cds + "(\.|_)", regex=True)
            )
    df = pd.DataFrame.from_dict(matches)
    df.index = probe_df["name"]
    df = df.stack().reset_index()
    df.columns = ["probe_name", "target_name", "is_match"]
    services.log.info(
        f"{len(df[df.is_match])} probes out of a total {len(probe_df)} ({(len(df[df.is_match])/len(probe_df)) * 100}) matched. "
    )
    return df[df.is_match]

def get_single_matching_target(probe_name, target_probe_matches):
    matches = target_probe_matches[target_probe_matches.probe_name == probe_name]
    if len(matches) != 1:
        services.log.warning(f"Unable to find target match for {probe_name}, got {matches.values}")
        return None
    return matches.iloc[0].target_name

def get_isoform_and_cds_from_target(target):
    """
    Returns the isoform and cds number from a target name.
    Assumes target name is split by underscores and cds number
    follows the isoform value "..._NM_000000_CDS1_..."
    """
    target_array = target.split("_")
    isoform_location = None
    for idx, item in enumerate(target_array):
        if item == "NM":
            isoform_location = idx + 1
            break
    if isoform_location is None:
        raise TargetProbeMatchError(f"Failed to parse isoform in target {target}")
    isoform = target_array[isoform_location]
    exon = target_array[isoform_location + 1]
    cds = exon.replace("Exon", "CDS")
    return isoform, cds

def get_clean_sequence(sequence):
    return sequence.replace("\\5Biosg\\", "")

def get_clean_c_dot(c_val):
    if c_val == ".":
        return None
    elif pd.isna(c_val):
        return None
    else:
        try:
            return int(c_val.replace("c.", ""))
        except:
            return None
