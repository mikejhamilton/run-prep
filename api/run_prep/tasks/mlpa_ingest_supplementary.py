from run_prep.core.tasks.base import BaseTask
from run_prep.core.extensions.db import db
from run_prep.models.upload_job import UploadJob, UploadJobSchema
from run_prep.models.upload import Upload, UploadSchema
from run_prep.models.design import Design
from run_prep.models.target import Target
from run_prep.models.mlpa_probe import MlpaProbe
from run_prep.models.mlpa_kit import MlpaKit
from run_prep.constants.symbols import SYMBOLS_TO_DIRECTIONS
from run_prep.constants.job_statuses import PENDING_REVIEW, PARSING_FAILED
from run_prep.core.containers.service import services
from celery.utils.log import worker_logger as log
import traceback
import pandas as pd

__all__ = ("MLPAIngestSupplementaryTask",)

job_serializer = UploadJobSchema()

class MLPAIngestSupplementaryTask(BaseTask):

    @classmethod
    def get_name(cls):
        return "mlpa_ingest_supplementary"

    @staticmethod
    def run(upload_job):
        job = job_serializer.load(upload_job, session=db.session)
        services.log.info(f"Starting supplementary work on {upload_job}")

        try:
            supplementary_files = Upload.query.filter(Upload.job_id == job.id) \
                        .filter(Upload.upload_type == "MLPA Supplementary") \
                        .all()
            design_files = Upload.query.filter(Upload.job_id == job.id) \
                        .filter(Upload.upload_type == "MLPA Design") \
                        .all()

            opened_supplementary_files = []
            opened_design_files = []

            for supplementary in supplementary_files:
                df = pd.read_table(supplementary.local_location)
                df["supplementary_upload_id"] = supplementary.id
                opened_supplementary_files.append(df)
            for design in design_files:
                df = pd.read_excel(design.local_location, header=12)
                df["design_upload_id"] = design.id
                opened_design_files.append(df)

            supplementary_df = pd.concat(opened_supplementary_files)
            opened_df = pd.concat(opened_design_files)

            opened_df = opened_df.merge(supplementary_df, left_on="Probe number", right_on="mlpa_probe_id")

            for idx, row in opened_df.iterrows():
                if row["cds"] == "." or row["symbol"] == ".":
                    services.log.error(f"Cannot align to exon for probe {row['Probe number']}, ignoring.")
                    job.number_errors_encountered += 1
                    continue

                kit = MlpaKit.query \
                    .filter(MlpaKit.upload_id == row["design_upload_id"]) \
                    .one()
                lpo_len = len(row["LPO"])
                rpo_len = len(row["RPO"])
                if not pd.isna(row["SP"]):
                    sp_len = len(row["SP"])
                else:
                    sp_len = 0
                length = int(row["mlpa_probe_end"]) - int(row["mlpa_probe_start"]) + 1
                diff = abs(sum([lpo_len, rpo_len, sp_len]) - length)
                if diff != 0:
                    lpo_design = None
                    rpo_design = None
                    sp_design = None
                else:
                    lpo_design = Design(
                        direction=SYMBOLS_TO_DIRECTIONS[row["strand"]],
                        sequence=row["LPO"],
                        chromosome=row["mlpa_chromosome"],
                        start_position=row["mlpa_probe_start"],
                        end_position=int(row["mlpa_probe_start"]) + lpo_len
                    )
                    db.session.add(lpo_design)
                    if sp_len != 0:
                        sp_design = Design(
                            direction=SYMBOLS_TO_DIRECTIONS[row["strand"]],
                            sequence=row["SP"],
                            chromosome=row["mlpa_chromosome"],
                            start_position=int(row["mlpa_probe_start"]) + lpo_len + 1,
                            end_position=int(row["mlpa_probe_end"]) + sp_len
                        )
                        db.session.add(sp_design)

                    else:
                        sp_design = None
                    rpo_design = Design(
                        direction=SYMBOLS_TO_DIRECTIONS[row["strand"]],
                        sequence=row["RPO"],
                        chromosome=row["mlpa_chromosome"],
                        start_position=int(row["mlpa_probe_start"]) + lpo_len + sp_len + 1,
                        end_position=int(row["mlpa_probe_end"])
                    )
                    db.session.add(rpo_design)
                target = Target.find_or_create(
                    chromosome=row["mlpa_chromosome"],
                    start=row["mlpa_probe_start"],
                    end=row["mlpa_probe_end"]
                )
                db.session.add(target)
                db.session.flush()
                probe = MlpaProbe(
                    lpo_design_id=lpo_design.id if lpo_design else None,
                    rpo_design_id=rpo_design.id if rpo_design else None,
                    sp_design_id=sp_design.id if sp_design else None,
                    mlpa_kit_id=kit.id,
                    primary_ligation_site=(int(row["mlpa_probe_start"]) + lpo_len) if (diff != 0) else None,
                    secondary_ligation_site=None if sp_len == 0 else int(row["mlpa_probe_start"]) + sp_len,
                    name=row["mlpa_probe_id"],
                    exon=row["cds"],
                    gene_symbol=row["symbol"],
                    mapview=row["Mapview"],
                    target_id=target.id
                )
                db.session.add(probe)
                db.session.flush()
                job.number_entries_created += 1

            job.job_status = PENDING_REVIEW
            db.session.add(job)
            db.session.commit()
            services.log.info(f"Finished supplementary work on {upload_job}")
        except Exception:
            job.job_status = PARSING_FAILED
            db.session.add(job)
            db.session.commit()
            services.log.error(f"Faild to parse files for job: {upload_job}")
            services.log.error(traceback.format_exc())
