import sqlite3
import sys
import os

def main():
    if not len(sys.argv) == 2:
        raise ValueError(f"Need to pass 1 param to CLI with path for existing sqlite DB file.")

    path = sys.argv[-1]

    if not os.path.isfile(path):
        raise ValueError(f"File {path} does not exist.")

    with open(f"dump.sql", "w") as f:
        with sqlite3.connect(path) as con:
            for line in con.iterdump():
                f.write(f"{line}\n")