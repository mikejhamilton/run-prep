import os
import csv

from run_prep.app import create_app
from run_prep.core.extensions.db import db
from run_prep.models.amplicon import Amplicon
from run_prep.models.amplicon_version import AmpliconVersion
from run_prep.core.containers.service import services

import pandas as pd


def main(app=None):
    """
    Populates isoform values for Amplicons versions that do not have one.
    Null isoform amplicons are queried from the database.
    """
    if not app:
        app = create_app()

    with app.app_context():
        upload_path = os.path.join(app.config["FILE_UPLOAD_PATH"], "legacy_data")
        cleaned_data = pd.read_csv(os.path.join(upload_path, "cleaned.csv"))
        null_isoform_amps = AmpliconVersion.query.filter(AmpliconVersion.isoform_version.notlike("NM_%")).all()
        null_isoform_errors = []
        resolved_isoforms = 0

        # Update isoforms
        for amplicon_version in null_isoform_amps:
            iso = get_similar_primary_amplicon(amplicon_version)
            if not iso:
                iso, null_isoform_errors = get_bi_isoform_value(amplicon_version, cleaned_data, null_isoform_errors)
            if iso:
                try:
                    amplicon_version.isoform_version = iso
                    db.session.commit()
                except Exception as e:
                    error_msg = f"Failed to update amplicon database row: {str(e)}"
                    null_isoform_errors.append((amplicon_version.id, amplicon_version.amplicon_version_name, error_msg))
                else:
                    resolved_isoforms += 1

    services.log.info(f"Resolved {(resolved_isoforms / len(null_isoform_amps)) * 100}% of {len(null_isoform_amps)} null isoform cases.")

    save_errors_to_file(null_isoform_errors, upload_path, "populate_isoform_errors.csv")


def get_bi_isoform_value(amplicon_version, bi_dataframe, errors):
    """
        Searches for amplicon version in BI Dataframe and returns the BI isoform value.
    """

    try:
        if len(amplicon_version.amplicon_version_name.split(" ")) > 2:
            error_msg = "Unknown Amplicon name format"
            errors.append((amplicon_version.id, amplicon_version.amplicon_version_name, error_msg))
            return None, errors

        symbol = amplicon_version.amplicon_version_name.split(" ")[0]
        exon_version = amplicon_version.amplicon_version_name.split(" ")[-1]
        avs_row = bi_dataframe[(bi_dataframe["symbol"] == symbol) & (bi_dataframe["amplicon_name"] == exon_version)]

        if len(avs_row) > 1:
            error_msg = "More than one BI row found"
            errors.append((amplicon_version.id, amplicon_version.amplicon_version_name, error_msg))
            return None, errors
        elif len(avs_row) == 0:
            error_msg = "No BI row found"
            errors.append((amplicon_version.id, amplicon_version.amplicon_version_name, error_msg))
            return None, errors

        bi_isoform = extract_isoform_string(avs_row["Predicted Isoform"].values[0])

        if not bi_isoform:
            error_msg = "No BI isoform present"
            errors.append((amplicon_version.id, amplicon_version.amplicon_version_name, error_msg))
            return None, errors

        return bi_isoform, errors
    except Exception as e:
        error_msg = f"Unknown Error {str(e)}"
        errors.append((amplicon_version.id, amplicon_version.amplicon_version_name, error_msg))
        return None, errors


def get_similar_primary_amplicon(amplicon_version):
    """
    Queries similiar primary amplicon versions that have the same gene and exon
    and tries to return an isoform version. Returns None if no isoform version can be found.
    """

    gene = amplicon_version.amplicon.gene_symbol
    exon = amplicon_version.amplicon.exon
    amplicon = Amplicon.query.filter_by(gene_symbol=gene).filter_by(exon=exon).one()
    similar_avs = (
        AmpliconVersion.query.filter_by(amplicon_id=amplicon.id)
        .filter_by(priority="Primary")
        .order_by(AmpliconVersion.version_number.desc())
    )
    for avs in similar_avs:
        if "NM_" in avs.isoform_version:
            return avs.isoform_version
    else:
        return None


def save_errors_to_file(errors, file_path, file_name):
    services.log.info(f"Writing to file: {os.path.join(file_path, file_name)}")
    with open(os.path.join(file_path, file_name), "w") as f:
        writer = csv.writer(f, delimiter=",")
        for error_row in errors:
            writer.writerow(error_row)


def extract_isoform_string(isoform):
    try:
        return isoform.split(".")[0]
    except:
        return None
