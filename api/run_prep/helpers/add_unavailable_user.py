from run_prep.core.extensions.db import db
from run_prep.app import create_app
from run_prep.core.models.user import User
from run_prep.core.containers.service import services
from datetime import datetime

def main():
    app = create_app()
    with app.app_context():
        if not User.query.filter(User.username == "unavailable").one_or_none():
            services.log.info(f"No unavailable user found, creating...")
            unavailable = User(
                username="unavailable",
                last_login=datetime.now()
            )
            db.session.add(unavailable)
            db.session.commit()
        else:
            services.log.info(f"Unavailable user found, skipping creation.")