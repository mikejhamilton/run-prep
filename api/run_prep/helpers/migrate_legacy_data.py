import csv
import os
import re
from collections import defaultdict

import pandas as pd
import sqlalchemy
from run_prep.app import create_app
from run_prep.core.containers.service import services
from run_prep.core.extensions.db import db
from run_prep.models.amplicon import Amplicon
from run_prep.models.comment import Comment
from run_prep.models.amplicon_version import AmpliconVersion
from run_prep.models.design import Design
from run_prep.models.sanger_primer import SangerPrimer
from run_prep.models.sanger_primer_pair import SangerPrimerPair
from run_prep.models.target import Target
from run_prep.helpers import add_worker_user, update_amplicon_versions

class NotTwoPrimersError(Exception):
    pass

class InvalidCoordinatesError(Exception):
    pass

class InvalidVersionError(Exception):
    pass

class AlternateValueNotAvailableError(Exception):
    pass

class UnknownThermocyclerProgramError(Exception):
    pass

class UnknownUserError(Exception):
    pass

class MultipleAluError(Exception):
    pass


THERMOCYCLER_PROGRAMS = (
    "61",
    "57.2",
    "63.5",
    "68.5",
    "57.2_N",
    "61_N",
    "63.5_N",
    "67_N",
    "GRAD",
    "HS_TAKLR",
    "TD",
    "SD",
    "ALU_61",
    "ALU1",
    "ALU2",
    "MSH2_INV",
)

def main():

    errors = defaultdict(list)

    ctx = None
    app = create_app()
    with app.app_context():
        add_worker_user.main(app)
        username = services.auth.get_current_username()
        if username == "unavailable":
            raise UnknownUserError("Failed to get worker user.")
        upload_path = os.path.join(app.config["FILE_UPLOAD_PATH"], "legacy_data")
        prod_data = pd.read_csv(os.path.join(upload_path, "migration.csv"))
        # Get updated PCR Methods DF
        pcr_data = pd.read_csv(os.path.join(upload_path, "pcr_methods.csv"))
        pcr_data = pcr_data[["PCR METHOD", "TEMP", "PCR Primer Pair Name"]]
        dup_df = pcr_data[pcr_data["PCR Primer Pair Name"].duplicated(keep=False)]
        pcr_data = pcr_data.drop_duplicates(subset=["PCR Primer Pair Name"], keep=False)
        # Get Cleaned Data from BI
        cleaned_data = pd.read_csv(os.path.join(upload_path, "cleaned.csv"))
        cleaned_data["predicted_isoform_version"] = cleaned_data["Predicted Isoform"].apply(extract_isoform_version)
        # Merge into master dataframe
        prod_data = prod_data.merge(cleaned_data, how="left", on="ID", suffixes=("", "_cleaned"))
        prod_data = prod_data.merge(pcr_data, how="left", left_on=["pcr_primer_pair_name"], right_on=["PCR Primer Pair Name"])
        prod_data = prod_data.where(prod_data.notnull(), None)

        stagd = Design.find_or_create(
            "forward",
            "TCTGCCTTTTTCTTCCATCGGG",
            "NA",
            -1,
            -1,
        )
        astagd = Design.find_or_create(
            "reverse",
            "TCCCCAACCCCCTAAAGCGA",
            "NA",
            -1,
            -1,
        )
        db.session.flush()
        stag = SangerPrimer(
            design_id=stagd.id,
            sanger_primer_name="s-tag",
            step="BDT",
            is_alubp=False
        )
        astag = SangerPrimer(
            design_id=astagd.id,
            sanger_primer_name="as-tag",
            step="BDT",
            is_alubp=False
        )
        db.session.add(stag)
        db.session.add(astag)
        db.session.flush()
        tags = SangerPrimerPair(
            forward_sanger_primer_id=stag.id,
            reverse_sanger_primer_id=astag.id
        )
        db.session.add(tags)
        db.session.commit()

        error_rows = []

        for idx, row in enumerate(prod_data.to_dict(orient="records")):

            try:
                primers = [
                    ("forward", row["forward_sequence"], get_row_value(row, "forward_pcr_primer_coordinates", alt="Calculated Forward Primer Location", strict=False), row["forward_pcr_primer_name"], "PCR"),
                    ("reverse", row["reverse_sequence"], get_row_value(row, "reverse_pcr_primer_coordinates", alt="Calculated Reverse Primer Location", strict=False), row["reverse_pcr_primer_name"], "PCR"),
                    ("forward", row["forward_seq_primer_sequence"], row["forward_sequencing_primer_coordinates"], row["forward_sequencing_name"], "BDT"),
                    ("reverse", row["reverse_seq_primer_sequence"], row["reverse_sequencing_primer_coordinates"], row["reverse_sequencing_primer_name"], "BDT"),
                ]

                alu_flag = is_alu(row["amplicon_name"])
                exon = re.sub(r"v\d+", "", row["amplicon_name"])
                primer_pairs_check = defaultdict(int)

                # Check to see if the appropriate amount of primers are present
                for direction, sequence, coords, name, step in primers:
                    if not isinstance(coords, str):
                        if name in ["s-tag", "as-tag"] or step == "BDT":
                            # s/as-tag and BDT primers are allowed to have null coordinates
                            primer_pairs_check[step] += 1
                        elif step == "PCR" and "alu" in name.lower():
                            # Alus are allowed to have null coordinates
                            primer_pairs_check[step] += 1
                        else:
                            raise InvalidCoordinatesError(f"Invalid coordinates for {name}, row {idx} on step {step}")
                    elif isinstance(coords, str):
                        parsed_coords = services.parser.parse_genomic_coords(coords)
                        primer_pairs_check[step] += 1
                for step, num_primers in primer_pairs_check.items():
                    if num_primers != 2:
                        raise NotTwoPrimersError(f"Didn't get exactly 2 primers for row {idx} on step {step}, got {primers}")

                if exon != row["amplicon_name"]:
                    services.log.info(f"Replacing exon name for row {idx}: was {row['amplicon_name']}, is now {exon}")

                try:
                    int(row["version"])
                except ValueError:
                    raise InvalidVersionError(f"Invalid version number for row {idx}: {row['version']}")

                if int(row["version"]) == 1:
                    version_suffix = ""
                else:
                    version_suffix = f"v{row['version']}"

                # Try to get PCR Method and Temp from pcr_method.csv data
                thermo_program = row["TEMP"] if not pd.isna(row["TEMP"]) else None
                pcr_method = row["PCR METHOD"] if not pd.isna(row["TEMP"]) else None

                # Get temp from prod data
                if not thermo_program and row["pcrtemp"]:
                    thermo_program = row["pcrtemp"]
                try:
                    thermo_program = str(thermo_program)
                    thermo_program = thermo_program.upper()
                except:
                    pass

                if thermo_program not in THERMOCYCLER_PROGRAMS:
                    raise UnknownThermocyclerProgramError(f"Unknown Thermocycler program for row {idx}: {row['pcrtemp']}")
                if not pcr_method:
                    pcr_method = "STANDARD"

                primer_pairs = defaultdict(list)
                alu_count = 0
                for direction, sequence, coords, name, step in primers:
                    if step == "BDT" and pcr_method in ["Gel", "Long Range PCR"]:
                        primer_pairs[step].append(None)
                        continue

                    if not isinstance(coords, str):
                        if name == "s-tag":
                            primer_pairs[step].append(stag)
                        elif name == "as-tag":
                            primer_pairs[step].append(astag)
                        elif step == "BDT":
                            bdt_d = Design.find_or_create(
                                direction,
                                sequence,
                                "NA",
                                -1,
                                -1,
                            )
                            db.session.flush()
                            bdt_primer = SangerPrimer(
                                design_id=bdt_d.id,
                                sanger_primer_name=name,
                                step="BDT",
                                is_alubp=False
                            )
                            db.session.add(bdt_primer)
                            db.session.flush()
                            primer_pairs[step].append(bdt_primer)
                        elif "alu" in name.lower() and step == "PCR":
                            # There cannot be more than one alu with null coordinates present for pcr primers.
                            if alu_count == 1:
                                raise MultipleAluError(f"Got more than one AluBP for row {idx} on step {step}, got {primers}")
                            alu_design = Design.find_or_create(
                                direction,
                                sequence,
                                "NA",
                                -1,
                                -1,
                            )
                            db.session.flush()
                            alu_primer = SangerPrimer(
                                design_id=alu_design.id,
                                sanger_primer_name=name,
                                step="PCR",
                                is_alubp=True
                            )
                            db.session.add(alu_primer)
                            db.session.flush()
                            primer_pairs[step].append(alu_primer)
                            alu_count += 1

                    elif isinstance(coords, str):
                        primer = services.sanger_primer_factory.create_or_update_primer(
                            coords,
                            sequence,
                            name,
                            direction,
                            step
                        )
                        primer_pairs[step].append(primer)
                    else:
                        raise ValueError(f"Something went wrong processing {idx}")

                for _step, _primers in primer_pairs.items():
                    if len(_primers) != 2:
                        raise NotTwoPrimersError(f"Didn't get exactly 2 primers for row {idx} on step {step}, got {primers}")

                a = Amplicon.find_by_name(f"{row['symbol']} {exon}")
                if not a:
                    a = Amplicon(
                        amplicon_name=f'{row["symbol"]} {exon}',
                        gene_symbol=row["symbol"],
                        exon=exon
                    )
                    db.session.add(a)
                    db.session.flush()

                av = AmpliconVersion(
                    amplicon_version_name=f'{row["symbol"]} {exon}{version_suffix}',
                    pcr_primer_pair_name=row["pcr_primer_pair_name"],
                    version_number=int(row["version"]),
                    isoform_version=get_row_value(row, "nucleotide_id", alt="predicted_isoform_version"),
                    fragment_length=get_row_value(row, "fragment_length", default=""),
                    amplicon_id=a.id,
                    c_dot_start=get_row_value(row, "c_var_bgn", alt="Calculated c_var_bgn", default=-1),
                    c_start_extension=get_row_value(row, "c_var_bgn_ext", default=-100),
                    c_dot_end=get_row_value(row, "c_var_end", alt="Calculated c_var_end", default=-1),
                    c_end_extension=get_row_value(row, "c_var_end_ext", default=100),
                    batching_data={
                        "sheet": get_row_value(row, "sheet", default="SANGER"),
                        "folder": get_row_value(row, "folder", default="Sanger-Active"),
                        "workbook": get_row_value(row, "workbook", default="Sanger Worklists")
                    },
                    amp_module=get_row_value(row, "amp_module", default="-7 to 7"),
                    seq_module=get_row_value(row, "seq_primer_module", default="-7 to 7"),
                    sequencing_direction=get_row_value(row, "direction", default="Both"),
                    thermo_program=thermo_program,
                    pcr_method=pcr_method,
                    betaine=float(get_row_value(row, "pcr_betaine", default=0)),
                    qsol=0,
                    dmso=float(get_row_value(row, "pcr_DMSO", default=0)),
                    validation_status=row["status"],
                    priority=row["key_type"].capitalize(),
                    is_alubp=alu_flag,
                    pseudogene=False
                )
                db.session.add(av)
                db.session.flush()
                
                notes = get_row_value(row, "additional_information")
                if notes is not None:
                    comment = Comment(
                        message=notes,
                        pinned=False,
                        username=username,
                    )
                    comment.amplicon_versions.append(av)
                    db.session.add(comment)

                for step, sanger_primers in primer_pairs.items():
                    if step == "BDT" and pcr_method in ["Gel", "Long Range PCR"]:
                        # Gel and long range do not have BDT Primers
                        continue
                    fsp = list(filter(lambda p: p.design.direction == "forward", sanger_primers))[0]
                    rsp = list(filter(lambda p: p.design.direction == "reverse", sanger_primers))[0]
                    if fsp.id == stag.id and rsp.id == astag.id:
                        spp = tags
                    else:
                        spp = SangerPrimerPair(
                            forward_sanger_primer_id=fsp.id,
                            reverse_sanger_primer_id=rsp.id
                        )
                        db.session.add(spp)
                        db.session.flush()
                    av.primer_pairs.append(spp)
                    if step == "PCR":
                        g_start, g_end = get_genomic_start_and_end(sanger_primers)
                        target = Target.find_or_create(
                            parsed_coords.get("chromosome"),
                            g_start,
                            g_end
                        )
                        av.targets.append(target)
                        db.session.add(av)

                db.session.commit()

            except sqlalchemy.exc.OperationalError as e:
                services.log.error(f"Operational Error inserting row {row['ID']}")
                errors["operational_error"].append((idx, row["ID"], f'{row["symbol"]} {row["amplicon_name"]}', e))
                error_rows.append(row)
                db.session.rollback()
            except sqlalchemy.exc.ProgrammingError as e:
                if "nan" in str(e):
                    services.log.error(f"Cannot insert row with null values: {row['ID']}")
                    errors["null_values"].append((idx, row["ID"], f'{row["symbol"]} {row["amplicon_name"]}', "null values"))
                else:
                    services.log.error(f"Programming Error inserting row {row['ID']}")
                    errors["programming_error"].append((idx, row["ID"], f'{row["symbol"]} {row["amplicon_name"]}', e))
                error_rows.append(row)
                db.session.rollback()
            except sqlalchemy.exc.DataError as e:
                services.log.error(f"Data Error inserting row {row['ID']}")
                errors["data_error"].append((idx, row["ID"], f'{row["symbol"]} {row["amplicon_name"]}', e))
                error_rows.append(row)
                db.session.rollback()
            except NotTwoPrimersError as e:
                services.log.error(f"Didn't get exactly two primers on a given step for row {row['ID']}: '{e}'")
                errors["need_two_primers"].append((idx, row["ID"], f'{row["symbol"]} {row["amplicon_name"]}', e))
                error_rows.append(row)
                db.session.rollback()
            except InvalidCoordinatesError as e:
                services.log.error(f"Invalid coordinates for some primer on row {row['ID']}")
                errors["invalid_coordinates"].append((idx, row["ID"], f'{row["symbol"]} {row["amplicon_name"]}', e))
                error_rows.append(row)
                db.session.rollback()
            except InvalidVersionError as e:
                services.log.error(f"Invalid version for row {row['ID']}")
                errors["invalid_version"].append((idx, row["ID"], f'{row["symbol"]} {row["amplicon_name"]}', e))
                error_rows.append(row)
                db.session.rollback()
            except AlternateValueNotAvailableError as e:
                services.log.error(f"Alternate Value not available for row {row['ID']}")
                errors["alternate_value_not_available"].append((idx, row["ID"], f'{row["symbol"]} {row["amplicon_name"]}', e))
                error_rows.append(row)
                db.session.rollback()
            except UnknownThermocyclerProgramError as e:
                services.log.error(f"Unknown Thermocycler program for row {row['ID']}")
                errors["unknown_thermo_program"].append((idx, row["ID"], f'{row["symbol"]} {row["amplicon_name"]}', e))
                error_rows.append(row)
                db.session.rollback()
            except TypeError as e:
                services.log.error(f"Type Error inserting row {row['ID']}")
                errors["type_error"].append((idx, row["ID"], f'{row["symbol"]} {row["amplicon_name"]}', e))
                error_rows.append(row)
                db.session.rollback()
            except Exception as e:
                services.log.error(f"Unknown Error inserting row {row['ID']}")
                errors["unknown_error"].append((idx, row["ID"], f'{row["symbol"]} {row["amplicon_name"]}', e))
                error_rows.append(row)
                db.session.rollback()

        services.log.info("Processing complete")

        for key, value in errors.items():
            services.log.info(f"For type {key} got {len(value)} errors.")
            with open(os.path.join(upload_path, f"{key}.csv"), "w") as f:
                writer = csv.writer(f, delimiter=",")
                for v in value:
                    writer.writerow(v)

        error_export = pd.DataFrame(error_rows)
        error_export.to_csv(os.path.join(upload_path, "errors_to_retry_manually.csv"))
        dup_df.to_csv(os.path.join(upload_path, "duplicate_error.csv"))

    update_amplicon_versions.main(app)

def extract_isoform_version(isoform):
    try:
        return isoform.split(".")[1]
    except:
        return None

def get_row_value(row, value, alt=None, default=None, strict=True):
    if alt is not None:
        if row.get(value) is None:
            services.log.warning(f"Trying to use alternative {value} for {row['ID']}: {row.get(alt)}")
            if row.get(alt) is None:
                if default is not None:
                    return default
                elif not strict:
                    return None
                elif strict:
                    raise AlternateValueNotAvailableError(f"Couldn't fetch alternate value {alt} for row {row['ID']}")
            else:
                return row.get(alt)
    if default is not None:
        if row.get(value) is None:
            services.log.warning(f"Using default value for {value} {row['ID']}: {default}")
            return default
    return row.get(value)


def get_genomic_start_and_end(primers):
    first_diff = abs(primers[0].design.start_position - primers[1].design.end_position)
    second_diff = abs(primers[1].design.start_position - primers[0].design.start_position)

    if first_diff > second_diff:
        g_start = min(primers[1].design.start_position, primers[0].design.end_position) + 1
        g_end = max(primers[1].design.start_position, primers[0].design.end_position) - 1
    else:
        g_start = min(primers[0].design.start_position, primers[1].design.end_position) + 1
        g_end = max(primers[0].design.start_position, primers[1].design.end_position) - 1

    return g_start, g_end

def is_alu(name):
    """Returns boolean if 'alu' is in amplicon name"""
    try:
        alu_flag = "alu" in name.lower()
    except:
        alu_flag = False
    return alu_flag
