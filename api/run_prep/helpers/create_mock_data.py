from run_prep.core.extensions.db import db
from run_prep.app import create_app
from run_prep.models.gene import Gene
from run_prep.models.amplicon import Amplicon
from run_prep.models.amplicon_version import AmpliconVersion
from run_prep.models.primer_design import PrimerDesign
from run_prep.models.priority import Priority
from run_prep.models.category import Category
from run_prep.models.validation_status import ValidationStatus
from run_prep.models.additive_volume import AdditiveVolume
from pathlib import Path
import csv
import random

def main():
    app = create_app()
    with app.app_context():
        create_genes()
        create_amplicons()
        create_additive_volumes()
        create_validation_statuses()
        create_categories()
        create_priorities()
        create_primer_designs()
        create_amplicon_versions()

def create_genes():
    fixture_file = Path(__file__).parent.parent.parent.joinpath("tests", "fixtures", "_gene_list_short.csv")
    with open(fixture_file, "r") as f:
        gene_reader = csv.DictReader(f)
        for g in gene_reader:
            g = Gene(gene_symbol=g["symbol"], ava3_id=g["id"])
            db.session.add(g)
    db.session.commit()

def create_amplicons():
    exons = ["1", "4.5"]
    gs = db.session.query(Gene).all()
    for g in gs:
        for exon in exons:
            a = Amplicon(
                amplicon_name= f"{g.gene_symbol} {exon}",
                gene_id= g.id,
                exon=exon
            )
            db.session.add(a)
    db.session.commit()

def create_categories():
    _categories = [
        ("PCR PRIMER", "sanger", "pcr"),
    ]
    for name, assay, step in _categories:
        c = Category(category_name=name, assay=assay, assay_step=step)
        db.session.add(c)
    db.session.commit()

def create_additive_volumes():
    vols = [
        (0.0, 0.0, 0.0),
        (1.0, 0.0, 0.0),
        (0.0, 1.0, 1.0)
    ]
    for b, q, d in vols:
        v = AdditiveVolume(betaine=b, dmso=d, qsol=q)
        db.session.add(v)
    db.session.commit()

def create_priorities():
    _priorities = [
        ("PRIMARY", 10),
        ("BACKUP", 5),
        ("CUSTOM", 1)
    ]
    for priority, value in _priorities:
        p = Priority(priority_name=priority, priority_value=value)
        db.session.add(p)
    db.session.commit()

def create_validation_statuses():
    statuses = [
        "DESIGNED",
        "REVIEWED",
        "PRE_VALIDATION",
        "IN_VALIDATION",
        "VALIDATED",
        "APPROVED_SANGER",
        "APPROVED_CUSTOM",
        "REJECTED"
    ]
    for status in statuses:
        s = ValidationStatus(validation_status_name=status)
        db.session.add(s)
    db.session.commit()

def create_primer_designs():
    fixture_file = Path(__file__).parent.parent.parent.joinpath("tests", "fixtures", "_sequences.csv")
    with open(fixture_file, "r") as f:
        sequence_reader = csv.DictReader(f)
        for s in sequence_reader:
            pd = PrimerDesign(
                direction="s",
                sequence=s["sequence"],
                chromosome=s["chromosome"],
                start_position=s["start_position"],
                end_position=s["end_position"],
                validation_status_id=1,
                category_id=1,
                priority_id=1
            )
            db.session.add(pd)
    db.session.commit()

def create_amplicon_versions():
    versions = [1, 2]
    amps = db.session.query(Amplicon).all()
    pds = db.session.query(PrimerDesign).all()
    for amp in amps:
        for version in versions:
            av = AmpliconVersion(
                amplicon_version_name=f"{amp.amplicon_name}v{version}",
                version_number=version,
                isoform_version=1,
                fragment_length="100",
                amplicon_id=amp.id,
                c_dot_start=1,
                c_dot_end=100,
                batching_data=dict(),
                reportable_ranges="some ranges",
                sequencing_direction="s",
                thermo_program=50.5,
                additive_volume_id=1
            )
            av_pds = random.sample(pds, 4)
            for pd in av_pds:
                av.primer_design.append(pd)
            db.session.add(av)
    db.session.commit()
