from run_prep.core.extensions.db import db
from run_prep.app import create_app

def main():
    ctx = None
    app = create_app()
    with app.app_context():
        versions = list(db.engine.execute("SELECT version FROM schema_migrations").fetchall())
        with open("/home/app/amp/configs/DB_MATE_VERSIONS", "r") as f:
            expected_versions = f.read()
        if not str(versions) == expected_versions:
            raise ValueError(f"Database schema doesn't match expected. Expected: {expected_versions}, got: {str(versions)}")