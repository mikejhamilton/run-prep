import csv
import os
import re
from collections import defaultdict

import pandas as pd
import sqlalchemy
from run_prep.app import create_app
from run_prep.core.containers.service import services
from run_prep.core.extensions.db import db
from run_prep.models.amplicon import Amplicon
from run_prep.models.design import Design
from run_prep.models.rtpcr_amplicon_version import RtpcrAmpliconVersion
from run_prep.models.rtpcr_primer import RtpcrPrimer
from run_prep.models.target import Target

class UnknownUserError(Exception):
    pass

def main():

    errors = defaultdict(list)

    ctx = None
    app = create_app()
    with app.app_context():
        username = services.auth.get_current_username()
        if username == "unavailable":
            raise UnknownUserError("Failed to get worker user.")

        upload_path = os.path.join(app.config["FILE_UPLOAD_PATH"], "legacy_data")

        prod_data = pd.read_csv(
            os.path.join(upload_path, "rtpcr_amplicons.csv"),
            names=[
                "input_order",
                "gene",
                "isoform",
                "version",
                "exon",
                "amplicon",
                "primer_pair",
                "forward_primer_name",
                "forward_primer_sequence",
                "forward_primer_cstart",
                "forward_primer_cend",
                "forward_primer_coords",
                "forward_junction_coords",
                "reverse_primer_name",
                "reverse_primer_sequence",
                "reverse_primer_cstart",
                "reverse_primer_cend",
                "reverse_primer_coords",
                "reverse_junction_coords",
                "fragment_length",
                "betaine",
                "priority",
                "validation",
                "control_pool",
                "amplicon_c_range",
                "amplicon_coords",
                "sample_id",
                "multiplex_coords",
            ],
            header=0,
        )

        multiplex_list = []
        error_rows = []
        for c, row in enumerate(prod_data.itertuples(), 1):
            services.log.info(f"{round((c / len(prod_data)), 3) * 100}% Complete.")
            try:
                primers = {
                    "forward": {
                        "name": row.forward_primer_name.strip(),
                        "coords": row.forward_primer_coords,
                        "sequence": row.forward_primer_sequence,
                        "junction": row.forward_junction_coords,
                    },
                    "reverse": {
                        "name": row.reverse_primer_name.strip(),
                        "coords": row.reverse_primer_coords,
                        "sequence": row.reverse_primer_sequence,
                        "junction": row.reverse_junction_coords,
                    },
                }

                # Remove version string from exon number
                exon = re.sub(r"v\d+", "", row.exon).strip()
                # Clean any whitespace from gene symbol
                gene = row.gene.strip()
                sample_id = row.sample_id.strip()
                amplicon = row.amplicon.strip()
                if exon != row.exon:
                    services.log.info(
                        f"Replacing exon name for row {row.Index}: was {row.amplicon}, is now {exon}"
                    )

                # Create amplicon
                a = Amplicon.find_by_name(f"{gene} {exon}")
                if not a:
                    a = Amplicon(
                        amplicon_name=f"{gene} {exon}",
                        gene_symbol=gene,
                        exon=exon,
                    )
                db.session.add(a)
                db.session.flush()

                control_bool = True if row.control_pool.lower() == "yes" else False

                # Create rtpcr amplicon object
                rav = RtpcrAmpliconVersion(
                    amplicon_version_name=amplicon,
                    version_number=row.version,
                    amplicon_id=a.id,
                    isoform_version=row.isoform.strip(),
                    fragment_length=str(row.fragment_length).strip(),
                    c_dot_start=0,
                    c_start_extension=0,
                    c_dot_end=0,
                    c_end_extension=0,
                    betaine=row.betaine,
                    validation_status=row.validation.strip(),
                    priority=row.priority.strip(),
                    pcr_primer_pair_name=row.primer_pair.strip(),
                    control_pool=control_bool,
                    amplicon_sample_id=sample_id,
                )

                for direction, primer in primers.items():
                    # Attempt to find existing primer
                    rprimer = RtpcrPrimer.find_by_name(primer["name"]).one_or_none()
                    # If no existing primer create one
                    if rprimer is None:
                        primer_coords = services.parser.parse_genomic_coords(
                            primer["coords"]
                        )
                        primer_design = Design.find_or_create(
                            direction,
                            primer["sequence"],
                            primer_coords["chromosome"],
                            primer_coords["start"],
                            primer_coords["end"],
                        )
                        rprimer = RtpcrPrimer(name=primer["name"])
                        rprimer.designs.append(primer_design)
                        # If primer has junction design, add to primers designs
                        if pd.notna(primer["junction"]):
                            junction_coords = services.parser.parse_genomic_coords(
                                primer["junction"]
                            )
                            junction_design = Design.find_or_create(
                                direction,
                                primer["sequence"],
                                junction_coords["chromosome"],
                                junction_coords["start"],
                                junction_coords["end"],
                            )
                            rprimer.designs.append(junction_design)
                    db.session.add(rprimer)
                    db.session.flush()
                    rav.rtpcr_primers.append(rprimer)

                # Set up rtpcr amplicon target based on g.coords
                rav_coords = primer_coords = services.parser.parse_genomic_coords(
                    row.amplicon_coords
                )
                rav_target = Target.find_or_create(
                    rav_coords.get("chromosome"),
                    rav_coords.get("start"),
                    rav_coords.get("end"),
                )
                rav.targets.append(rav_target)

                # Add rtpcr amplicon to multiplex list if multiplex amplicon
                if "_M" in sample_id:
                    multiplex_list.append(rav)

                db.session.add(rav)
                db.session.commit()

            except sqlalchemy.exc.OperationalError as e:
                services.log.error(f"Operational Error inserting row {row.Index}: {e}")
                errors["operational_error"].append(
                    (row.Index, f"{gene} {amplicon}", e)
                )
                error_rows.append(row)
                db.session.rollback()
            except sqlalchemy.exc.ProgrammingError as e:
                if "nan" in str(e):
                    services.log.error(
                        f"Cannot insert row with null values: {row.Index}"
                    )
                    errors["null_values"].append(
                        (row.Index, f"{gene} {amplicon}", "null values")
                    )
                else:
                    services.log.error(f"Programming Error inserting row {row.Index}")
                    errors["programming_error"].append(
                        (row.Index, f"{gene} {amplicon}", e)
                    )
                error_rows.append(row)
                db.session.rollback()
            except sqlalchemy.exc.DataError as e:
                services.log.error(f"Data Error inserting row {row.Index}")
                errors["data_error"].append(
                    (row.Index, f"{gene} {amplicon}", e)
                )
                error_rows.append(row)
                db.session.rollback()
            except TypeError as e:
                services.log.error(f"Type Error inserting row {row.Index}")
                errors["type_error"].append(
                    (row.Index, f"{gene} {amplicon}", e)
                )
                error_rows.append(row)
                db.session.rollback()
            except Exception as e:
                services.log.error(f"Unknown Error inserting row {row.Index}")
                services.log.error(e)
                errors["unknown_error"].append(
                    (row.Index, f"{gene} {amplicon}", e)
                )
                error_rows.append(row)
                db.session.rollback()

        for idx, amp in enumerate(multiplex_list):

            # Try to find matching multiplex if foreign key is null
            if not amp.multiplex_rtpcr_amplicon_version_id:
                for matching_amp in multiplex_list[idx + 1 :]:
                    if matching_amp.amplicon_sample_id == amp.amplicon_sample_id:
                        amp.multiplex_rtpcr_amplicon_version_id = matching_amp.id
                        matching_amp.multiplex_rtpcr_amplicon_version_id = amp.id
                        db.session.add(amp)
                        db.session.add(matching_amp)
                        break
                else:
                    services.log.error(
                        f"Could not pair multiplex amplicon {amp.amplicon_sample_id}"
                    )
        db.session.commit()

        services.log.info("Processing complete")

        for key, value in errors.items():
            services.log.info(f"For type {key} got {len(value)} errors.")
            with open(os.path.join(upload_path, f"RTPCR_{key}.csv"), "w") as f:
                writer = csv.writer(f, delimiter=",")
                for v in value:
                    writer.writerow(v)

        error_export = pd.DataFrame(error_rows)
        error_export.to_csv(
            os.path.join(upload_path, "rtpcr_errors_to_retry_manually.csv")
        )