from run_prep.core.extensions.db import db
from run_prep.app import create_app
from run_prep.core.models.user import User
from run_prep.core.containers.service import services
from datetime import datetime

def main(app=None):
    if app is None:
        app = create_app()
    with app.app_context():
        if not User.query.filter(User.username == "worker").one_or_none():
            services.log.info(f"No worker user found, creating...")
            worker = User(
                username="worker",
                last_login=datetime.now()
            )
            db.session.add(worker)
            db.session.commit()
        else:
            services.log.info(f"Worker user found, skipping creation.")