import random
from run_prep.models.primer_design import PrimerDesign
from run_prep.core.extensions.db import db
from run_prep.app import create_app

def main():
    app = create_app()
    with app.app_context():
        create_primer_designs()

def create_primer_designs():
    log.info("Creating mock primer designs")
    for _ in range(20000):
        start = random.randint(1, 100000000)
        length = random.randint(50, 1000)
        pd = PrimerDesign(
            direction="s",
            sequence="ACTG",
            chromosome="1",
            start_position=start,
            end_position=start + length,
            validation_status_id=1,
            category_id=1,
            priority_id=1
        )
        db.session.add(pd)
    db.session.commit()