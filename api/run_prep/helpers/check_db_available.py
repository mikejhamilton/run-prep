from tenacity import retry, stop_after_attempt, wait_exponential, retry_if_exception_type
from run_prep.app import create_app
from MySQLdb._exceptions import OperationalError
from run_prep.core.extensions.db import db

@retry(
    stop=stop_after_attempt(2),
    wait=wait_exponential(multiplier=1, min=1, max=2),
    reraise=True,
    retry=retry_if_exception_type(OperationalError)
)
def main():
    ctx = None
    app = create_app()
    with app.app_context():
        db.engine.execute("SELECT 1")