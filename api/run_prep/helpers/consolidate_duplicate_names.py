import logging

import sqlalchemy
from run_prep.app import create_app
from run_prep.core.containers.service import services
from run_prep.models.amplicon_version import AmpliconVersion, AmpliconVersionSchema
from run_prep.models.design import Design, DesignSchema
from run_prep.models.sanger_primer import SangerPrimer
from run_prep.models.sanger_primer_pair import SangerPrimerPair
from run_prep.core.extensions.db import db


def get_genomic_range(design):
    return f'{design["chromosome"]}:{design["start_position"]}-{design["end_position"]}'


def get_av_object(sp_id, direction):
    if direction == 'reverse':
        avs = AmpliconVersion.query. \
            join(AmpliconVersion.primer_pairs). \
            join(SangerPrimerPair.reverse_sanger_primer). \
            filter(SangerPrimer.id == sp_id).all()
    else:
        avs = AmpliconVersion.query. \
            join(AmpliconVersion.primer_pairs). \
            join(SangerPrimerPair.forward_sanger_primer). \
            filter(SangerPrimer.id == sp_id).all()

    if not avs:
        return None

    return {
        'direction': direction,
        'ids': [AmpliconVersionSchema().dump(av)["id"] for av in avs],
        }


def main():
    """
    ONE-OFF: Can be safely deleted after LI-773 adds a unique constraint to sanger_primers.sanger_primer_name.

    Searches the sanger_primers table for duplicates on `sanger_primer_name`. This column is currently only used as
    a foreign key (in two columns) in sanger_primer_pairs. The script chooses the lowest SP id as the canonical id,
    updates references in SPP, and deletes the orphaned SPs.
    :return:
    """
    design_collection = {}
    app = create_app()
    with app.app_context():
        username = services.auth.get_current_username()
        if username == "unavailable":
            raise Exception("Failed to get worker user.")

        names = db.session.query(SangerPrimer.sanger_primer_name).\
            group_by(SangerPrimer.sanger_primer_name).\
            having(sqlalchemy.func.count(SangerPrimer.id) > 1).all()

        logging.info(f'{len(names)} duplicate sanger primer names found.')
        update_count = 0
        remove_count = 0

        for name_a in names:
            name = name_a[0]
            logging.info(f'NAME: {name}')
            ids_to_drop = db.session.query(SangerPrimer.id).filter(SangerPrimer.sanger_primer_name == name).all()
            max_sanger_primer_id = max(ids_to_drop)

            # Keep highest id as canonical
            ids_to_drop.remove(max_sanger_primer_id)

            for drop_id_raw in ids_to_drop:
                drop_id = drop_id_raw[0]

                # logging.info(f'min_id: {min_sanger_primer_id[0]} | drop id: {drop_id[0]}')
                drop_sp = SangerPrimer.query.get(drop_id)
                drop_design = drop_sp.design

                max_sp = SangerPrimer.query.get(max_sanger_primer_id)
                max_design = max_sp.design

                # designs linked from each SP do not match
                if drop_design.id != max_design.id:
                    design_collection[name] = {}
                    design_collection[name][f'drop_{drop_id}'] = {}
                    design_collection[name][f'drop_{drop_id}']['avs'] = []
                    design_collection[name][f'drop_{drop_id}']['id'] = drop_id

                    design_collection[name]['max'] = {}
                    design_collection[name]['max']['avs'] = []
                    design_collection[name]['max']['id'] = max_sanger_primer_id[0]

                    design_keys = ['id', 'chromosome', 'start_position', 'end_position']
                    design_collection[name][f'drop_{drop_id}']['design'] = {k: DesignSchema().dump(drop_design)[k] for k in design_keys}
                    design_collection[name]['max']['design'] = {k: DesignSchema().dump(max_design)[k] for k in design_keys}

                    rda = get_av_object(drop_id, 'reverse')
                    if rda:
                        design_collection[name][f'drop_{drop_id}']['avs'].append(rda)
                    fda = get_av_object(drop_id, 'forward')
                    if fda:
                        design_collection[name][f'drop_{drop_id}']['avs'].append(fda)
                    rma = get_av_object(max_sanger_primer_id, 'reverse')
                    if rma:
                        design_collection[name]['max']['avs'].append(rma)
                    fma = get_av_object(max_sanger_primer_id, 'forward')
                    if fma:
                        design_collection[name]['max']['avs'].append(fma)



                else:
                    # If designs match, find rows in SPP that reference the drop_id in either
                    # the forward or reverse # columns
                    fpairs = SangerPrimerPair.query.filter(SangerPrimerPair.forward_sanger_primer_id == drop_id).all()
                    rpairs = SangerPrimerPair.query.filter(SangerPrimerPair.reverse_sanger_primer_id == drop_id).all()

                    if fpairs:
                        for fp in fpairs:
                            fp.forward_sanger_primer_id = max_sanger_primer_id
                            logging.info(f'UPDATE (forward) SPP: {fp}')
                            update_count += 1
                            db.session.commit()
                    if rpairs:
                        for rp in rpairs:
                            rp.reverse_sanger_primer_id = max_sanger_primer_id
                            logging.info(f'UPDATE (reverse) SPP: {rp}')
                            update_count += 1
                            db.session.commit()
                    sp = SangerPrimer.query.get(drop_id)
                    logging.info(f'REMOVE SP {sp}')
                    db.session.delete(sp)
                    remove_count += 1
                    db.session.commit()

        names_after = db.session.query(SangerPrimer.sanger_primer_name). \
            group_by(SangerPrimer.sanger_primer_name). \
            having(sqlalchemy.func.count(SangerPrimer.id) > 1).all()

        logging.info(f'Updated {update_count} Sanger Primer Pairs.')
        logging.info(f'Removed {remove_count} Sanger Primers.')
        logging.info(f'Remaining duplicate names: {len(names_after)}')

        logging.warn(f'design mismatches ({len(design_collection.keys())}): {design_collection}')
        for name, data in design_collection.items():
            for _, datum in data.items():
                for obj in datum['avs']:
                    for av_id in obj['ids']:
                        logging.info(f'{name},{obj["direction"]},{datum["id"]},{av_id},{datum["design"]["id"]},{get_genomic_range(datum["design"])}')
