from run_prep.core.extensions.db import db
from run_prep.app import create_app

def main():
    app = create_app()
    with app.app_context():
        db.create_all()
