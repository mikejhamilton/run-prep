from run_prep.core.containers.service import services
from run_prep.core.extensions.db import db
from run_prep.app import create_app

def main():
    ctx = None
    app = create_app()
    with app.app_context():
        versions = list(db.engine.execute("SELECT version FROM schema_migrations").fetchall())
        services.log.info(f"Using versions {versions}")
        with open("/home/app/amp/configs/DB_MATE_VERSIONS", "w") as f:
            f.write(str(versions))