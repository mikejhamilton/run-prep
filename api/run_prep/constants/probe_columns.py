MLPA_DEFAULT_COLUMNS = [
    "name",
    "exon",
    "gene_symbol",
    "mapview",
    "mlpa_kit.name",
    "target.chromosome",
    "target.start_position",
    "target.end_position",
]

TARGETED_MICROARRAY_DEFAULT_COLUMNS = [
    "name",
    "design.sequence",
    "design.direction",
    "design.chromosome",
    "design.start_position",
    "design.end_position",
    "chip.name",
]

SNP_MICROARRAY_DEFAULT_COLUMNS = [
    "name",
    "design.sequence",
    "design.direction",
    "design.chromosome",
    "design.start_position",
    "design.end_position",
    "chip.name",
]

NGS_DEFAULT_COLUMNS = [
    "name",
    "design.direction",
    "design.chromosome",
    "design.start_position",
    "design.end_position",
]

TARGETED_MICROARRAY_TOTAL_COLUMNS = [
    {"dataField": "name", "text": "Name", "sort": True, },
    {"dataField": "design.direction", "text": "Direction", "sort": True, },
    {"dataField": "design.sequence", "text": "Sequence", "sort": True, },
    {"dataField": "design.chromosome", "text": "Chromosome", "sort": True, },
    {"dataField": "design.start_position", "text": "Start Pos", "sort": True, },
    {"dataField": "design.end_position", "text": "End Pos", "sort": True, },
    {"dataField": "chip.name", "text": "Chip", "sort": True, },
]

SNP_MICROARRAY_TOTAL_COLUMNS = [
    {"dataField": "name", "text": "Name", "sort": True, },
    {"dataField": "design.direction", "text": "Direction", "sort": True, },
    {"dataField": "design.sequence", "text": "Sequence", "sort": True, },
    {"dataField": "design.chromosome", "text": "Chromosome", "sort": True, },
    {"dataField": "design.start_position", "text": "Start Pos", "sort": True, },
    {"dataField": "design.end_position", "text": "End Pos", "sort": True, },
    {"dataField": "chip.name", "text": "Chip", "sort": True, },
]

NGS_TOTAL_COLUMNS = [
    {"dataField": "name", "text": "Name", "sort": True, },
    {"dataField": "gc_percent", "text": "GC Percent", "sort": True, },
    {"dataField": "design.direction", "text": "Direction", "sort": True, },
    {"dataField": "design.sequence", "text": "Sequence", "sort": True, },
    {"dataField": "design.chromosome", "text": "Chromosome", "sort": True, },
    {"dataField": "design.start_position", "text": "Start Pos", "sort": True, },
    {"dataField": "design.end_position", "text": "End Pos", "sort": True, },
    {"dataField": "ngs_probeset.name", "text": "Probeset", "sort": True, },
    {"dataField": "ngs_target_group.name", "text": "Target", "sort": True, },
    {"dataField": "ngs_target_group.target.chromosome", "text": "Target Chromosome", "sort": True, },
    {"dataField": "ngs_target_group.target.start_position", "text": "Target Start", "sort": True, },
    {"dataField": "ngs_target_group.target.end_position", "text": "Target End", "sort": True, },
]

MLPA_TOTAL_COLUMNS = [
    {"dataField": "name", "text": "Name", "sort": True, },
    {"dataField": "exon", "text": "Exon", "sort": True, },
    {"dataField": "genebank_exon", "text": "Genebank exon",},
    {"dataField": "gene_symbol", "text": "Gene",},
    {"dataField": "mapview", "text": "MapView",},
    {"dataField": "chromosome_position", "text": "CHR Position",},
    {"dataField": "mlpa_kit.name", "text": "Kit Name", "sort": True, },
    {"dataField": "target.chromosome", "text": "Chromosome",},
    {"dataField": "target.start_position", "text": "Start Pos", "sort": True, },
    {"dataField": "target.end_position", "text": "End Pos", "sort": True, },
    {"dataField": "primary_ligation_site", "text": "P. Ligation",},
    {"dataField": "secondary_ligation_site", "text": "S. Ligation",},
    {"dataField": "lpo_design.direction", "text": "LPO Direction", "sort": True, },
    {"dataField": "lpo_design.sequence", "text": "LPO Seq", "sort": True, },
    {"dataField": "lpo_design.chromosome", "text": "LPO Chomosome", "sort": True, },
    {"dataField": "lpo_design.start_position", "text": "LPO Start Pos", "sort": True, },
    {"dataField": "lpo_design.end_position", "text": "LPO End Pos", "sort": True, },
    {"dataField": "sp_design.direction", "text": "SP Direction", "sort": True, },
    {"dataField": "sp_design.sequence", "text": "SP Seq", "sort": True, },
    {"dataField": "sp_design.chromosome", "text": "SP Chomosome", "sort": True, },
    {"dataField": "sp_design.start_position", "text": "SP Start Pos", "sort": True, },
    {"dataField": "sp_design.end_position", "text": "SP End Pos", "sort": True, },
    {"dataField": "rpo_design.direction", "text": "RPO Direction", "sort": True, },
    {"dataField": "rpo_design.sequence", "text": "RPO Seq", "sort": True, },
    {"dataField": "rpo_design.chromosome", "text": "RPO Chomosome", "sort": True, },
    {"dataField": "rpo_design.start_position", "text": "RPO Start Pos", "sort": True, },
    {"dataField": "rpo_design.end_position", "text": "RPO End Pos", "sort": True, },
]
