from run_prep.models.comment import Comment
from run_prep.core.models.event import Event
from run_prep.core.models.user import User
from run_prep.models.design import Design
from run_prep.models.target import Target
from run_prep.models.targeted_array_probe import TargetedArrayProbe

EVENT_CLASS_BLACKLIST = (
    Comment,
    Event,
    User,
    Design,
    Target,
    TargetedArrayProbe
)