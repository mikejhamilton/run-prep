class ForbiddenModifyError(Exception):
    pass

class DesignFileUploadError(Exception):
    pass

class InvalidFileName(Exception):
    pass

class IncorrectFileExtension(Exception):
    pass

class UnknownQueryParamFilter(Exception):
    pass

class UnknownJobIDError(Exception):
    pass

class UnknownStatusIDError(Exception):
    pass

class MissingParameterError(Exception):
    pass

class TargetProbeMatchError(Exception):
    pass
