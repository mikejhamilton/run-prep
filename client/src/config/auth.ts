import { getConfigValue } from '../util';

export const OKTA_URL = getConfigValue('OKTA_URL');
export const OKTA_ISSUER = getConfigValue('OKTA_ISSUER');
export const OKTA_CLIENT_ID = getConfigValue('OKTA_CLIENT_ID');
export const OKTA_REDIRECT_URI = getConfigValue('OKTA_REDIRECT_URI');

export default {
  url: OKTA_URL,
  issuer: OKTA_ISSUER,
  clientId: OKTA_CLIENT_ID,
  redirectUri: OKTA_REDIRECT_URI,
  pkce: true,
  cookies: {
    secure: false
  }
};
