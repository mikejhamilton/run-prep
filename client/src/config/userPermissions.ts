import { getConfigValue } from '../util';

export const CREATE_AMPLICON_GROUPS = getConfigValue('CREATE_AMPLICON_GROUPS').split(',');
export const POST_COMMENT_GROUPS = getConfigValue('POST_COMMENT_GROUPS').split(',');
export const PROBE_DESIGN_GROUPS = getConfigValue('PROBE_DESIGN_GROUPS').split(',');
export const MIMICABLE_GROUPS = getConfigValue('MIMICABLE_GROUPS').split(',');
