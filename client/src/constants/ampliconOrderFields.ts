// Returns fields associated with Amplicon Version serialized object that can be used for sorting.
export const ampliconVersionOrderFields = [
    'amplicon.gene_symbol',
    'isoform_version',
    'pcr_primer_pair_name',
    'c_range',
    'priority',
    'validation_status',
];