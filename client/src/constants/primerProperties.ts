export type PrimerDirections = "forward" | "reverse";

export type PrimerSteps = "BDT" | "PCR";