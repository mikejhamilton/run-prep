import { toLower } from "lodash";
import getConfigValue from "util/getConfigValue";

export const APP_NAME = 'AMP';
export const IS_PRODUCTION = toLower(getConfigValue('APP_ENV')) === 'production';

// Keys used for storing data in localStorage. Always prefixed with APP_NAME.
export const USER_LOCALSTORAGE_KEY = `${APP_NAME.toLowerCase()}:user`;

// Time-related constant
export const TWO_WEEKS_MILLISECONDS = 14*24*3600*1000;
