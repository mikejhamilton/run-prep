// Associate different thermocycler programs (value) with each PCR method (key).
export default {
    "STANDARD": [
        '57.2',
        '61',
        '63.5',
        'GRAD',
        'SD',
        'TD',
    ],
    "Long Range PCR": [
        'HS_TAKLR',
    ],
    "Nested": [
        '57.2_N',
        '61_N',
        '63.5_N',
        '67_N',
    ],
    "Gel": [
        '61',
        '63.5_N',
        'ALU_61',
        'ALU1',
        'ALU2',
        'MSH2_INV',
        'TD',
    ],
    "Gel + Sequence": [
        '61',
        '63.5_N',
        'ALU_61',
        'TD',
    ],
};
