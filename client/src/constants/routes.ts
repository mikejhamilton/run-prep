type IDParam = ':id';

export const LOGIN = '/login';
export const AUTH = '/auth';
export const LOGOUT = '/logout';
export const SEARCH_SANGER_AMPLICON = '/amplicon/search';
export const JWT = '/jwt';
export const CREATE_SANGER_AMPLICON = '/amplicon/create'
export const UPDATE_SANGER_AMPLICON = (ampliconId: string = ':id') => `/amplicon/update/${ampliconId}`
export const DUPLICATE_SANGER_AMPLICON = (ampliconId: string = ':id') => `/amplicon/duplicate/${ampliconId}`
export const SEARCH_RTPCR_AMPLICON = '/rtpcr-amplicon/search';
export const INDEX = '/';
export const PROBE_UPLOAD = '/probe/upload';
export const PROBE_STATUS = '/probe/status';
export const PROBE_REVIEW = (jobId: string = ':id') => `/probe/upload/review/${jobId}`
export const EVENT_LOG = '/events';