export const BDT_COORD_REGEX = /(NA)|(?<chromosome>chr(\d+|[XY])):(?<start>[\d\,]+)-(?<end>[\d\,]+)$/;
export const COORD_REGEX = /(?<chromosome>chr(\d+|[XY])):(?<start>[\d\,]+)-(?<end>[\d\,]+)$/;
export const SEQUENCE_REGEX = /^[ACGTUWSMKRYBDHVNZ]{1,250}$/;
export const BASE_SEQUENCE_REGEX = /^[ACGT]{1,250}$/;
export const DEFAULT_BDT_NAME_REGEX = /^(?<default>((s)|(as))-tag)$/;
export const ISOFORM_FORMAT_REGEX = /\w+[iI]\d$/;