import {Action, configureStore, getDefaultMiddleware} from '@reduxjs/toolkit';
import {routerMiddleware} from 'connected-react-router';
import history from './history';
import rootReducer, {RootState} from './root';
import {ThunkAction} from 'redux-thunk';
import getConfigValue from "util/getConfigValue";

const store = configureStore({
    reducer: rootReducer,
    middleware: [...getDefaultMiddleware(), routerMiddleware(history)],
});

if (getConfigValue('APP_ENV') === 'development' && module.hot) {
    module.hot.accept('./root', () => {
        const newRootReducer = require('./root').default;
        store.replaceReducer(newRootReducer);
    });
}

export type AppDispatch = typeof store.dispatch
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>

export default store;
