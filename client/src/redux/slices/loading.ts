import {createSlice, PayloadAction} from '@reduxjs/toolkit';

type LoadingState = {
    loading: boolean,
    transparent: boolean,
}

const initialState: LoadingState = {
    loading: false,
    transparent: true,
};

const loadingSlice = createSlice({
    name: 'loading',
    initialState,
    reducers: {
        startLoading(state: LoadingState, action: PayloadAction<boolean | undefined>) {
            state.loading = true;
            state.transparent = action.payload === undefined ? true : action.payload;
        },
        endLoading(state: LoadingState) {
            state.loading = false;
        },
    },
});

export const {
    startLoading,
    endLoading,
} = loadingSlice.actions;

export default loadingSlice.reducer;
