import isArray from 'lodash/isArray';
import map from 'lodash/map';
import truncate from 'lodash/truncate';
import startCase from 'lodash/startCase';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppDispatch, AppThunk } from "../store";
import { appendAndReplace } from "../util";
import api from "../../services/api";
import { Event } from '../../../types';

type EventState = {
    items: Collection<Event>;
    pages: { [key: number]: number[] };
    currentPage: number;
    sizePerPage: number;
    totalSize: number;
}

type AppendPayload = {
    page: number | null;
    items: Event | Event[];
};

const initialState: EventState = {
    items: {},
    pages: {},
    currentPage: 1,
    sizePerPage: 30,
    totalSize: 0,
};

const eventsSlice = createSlice({
    name: 'events',
    initialState,
    reducers: {
        appendEvents(state: EventState, action: PayloadAction<AppendPayload>) {
            state.items = appendAndReplace(state.items, action.payload.items);
            if (action.payload.page != null) {
                state.pages[action.payload.page] = map(
                    isArray(action.payload.items) ? action.payload.items : [action.payload.items],
                    'id'
                );
            }
        },
        setPage(state: EventState, action: PayloadAction<number>) {
            state.currentPage = action.payload;
        },
        setSizePerPage(state: EventState, action: PayloadAction<number>) {
            state.sizePerPage = action.payload;
        },
        setTotalSize(state: EventState, action: PayloadAction<number>) {
            state.totalSize = action.payload;
        },
    },
});

export function fetchEventsByDateRange(setLoading: (arg0: boolean) => void, lowDate: string, highDate: string, page: number, sizePerPage: number): AppThunk {
    return async (dispatch: AppDispatch) => {
        await dispatch(eventsSlice.actions.setPage(page));
        await dispatch(eventsSlice.actions.setSizePerPage(sizePerPage));
        const response = await api.get('/api/v2/event/fetch-by-date-range', {
            params: {lowDate: lowDate, highDate: highDate, page, sizePerPage},
        });
        dispatch(eventsSlice.actions.appendEvents({
            page,
            items: response.data.api_data.items.map((item: Event) => {
                const summary = item.summary.join(', ');
                return {
                    ...item,
                    initial_values: JSON.stringify(item.initial_values),
                    final_values: JSON.stringify(item.final_values),
                    summary,
                };
            }),
        }));
        dispatch(eventsSlice.actions.setTotalSize(response.data.api_data.total));
        setLoading(false);
    }
}

export function setPagination(page: number, sizePerPage: number): AppThunk {
    return async (dispatch: AppDispatch) => {
        dispatch(eventsSlice.actions.setPage(page));
        dispatch(eventsSlice.actions.setSizePerPage(sizePerPage));
    };
}

export default eventsSlice.reducer;
