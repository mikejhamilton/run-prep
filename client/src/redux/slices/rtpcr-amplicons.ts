import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RtpcrAmplicon, ISearchFilter } from '../../../types';
import { appendAndReplace } from '../util';
import { AppDispatch, AppThunk } from '../store';
import map from 'lodash/map';
import isArray from 'lodash/isArray';
import api from '../../services/api';
import { getQueryParamsFromFilters } from '../../util';

type RtpcrAmpliconState = {
    items: Collection<RtpcrAmplicon>;
    pages: { [key: number]: number[] };
    currentPage: number;
    sizePerPage: number;
    totalSize: number;
    loading: boolean;
}

type AppendPayload = {
    page: number | null;
    items: RtpcrAmplicon | RtpcrAmplicon[];
};

const initialState: RtpcrAmpliconState = {
    items: {},
    pages: {},
    currentPage: 1,
    sizePerPage: 25,
    totalSize: 0,
    loading: false,
};

const rtpcrAmpliconsSlice = createSlice({
    name: 'rtpcr_amplicons',
    initialState,
    reducers: {
        appendRtpcrAmplicons(state: RtpcrAmpliconState, action: PayloadAction<AppendPayload>) {
            state.items = appendAndReplace(state.items, action.payload.items);
            if (action.payload.page != null) {
                state.pages[action.payload.page] = map(
                    isArray(action.payload.items) ? action.payload.items : [action.payload.items],
                    'id'
                );
            }
        },
        resetRtpcrAmplicons(state: RtpcrAmpliconState, action: PayloadAction) {
            state.items = {};
            state.pages = {};
            state.currentPage = 1;
            state.sizePerPage = 25;
            state.totalSize = 0;
            state.loading = false;
        },
        setPage(state: RtpcrAmpliconState, action: PayloadAction<number>) {
            state.currentPage = action.payload;
        },
        setSizePerPage(state: RtpcrAmpliconState, action: PayloadAction<number>) {
            state.sizePerPage = action.payload;
        },
        setTotalSize(state: RtpcrAmpliconState, action: PayloadAction<number>) {
            state.totalSize = action.payload;
        },
        setLoadingStatus(state: RtpcrAmpliconState, action: PayloadAction<boolean>) {
            state.loading = action.payload;
        }
    }
});

export function fetchRtpcrAmplicons(page: number, sizePerPage: number, filters: ISearchFilter[], sortField?: string, sortOrder?: string): AppThunk {
    return async (dispatch: AppDispatch) => {
        dispatch(rtpcrAmpliconsSlice.actions.setPage(page));
        dispatch(rtpcrAmpliconsSlice.actions.setSizePerPage(sizePerPage));
        dispatch(rtpcrAmpliconsSlice.actions.setLoadingStatus(true));
        const queryParams = getQueryParamsFromFilters(filters)
        queryParams?.append('page', String(page))
        queryParams?.append('sizePerPage', String(sizePerPage))
        if (sortField) {
            queryParams?.append('orderBy', String(sortField))
        }
        if (sortOrder === 'desc') {
            queryParams?.append('desc', '1');
        }
        api.get('/api/v2/rtpcr-amplicon-version', {
            params: queryParams,
        }).then(response => {
            dispatch(rtpcrAmpliconsSlice.actions.appendRtpcrAmplicons({
                page,
                items: response.data.api_data.items,
            }));
            dispatch(rtpcrAmpliconsSlice.actions.setTotalSize(response.data.api_data.total));
        }).finally(() => {
            dispatch(rtpcrAmpliconsSlice.actions.setLoadingStatus(false));
        })
    };
}

export function setPagination(page: number, sizePerPage: number): AppThunk {
    return async (dispatch: AppDispatch) => {
        dispatch(rtpcrAmpliconsSlice.actions.setPage(page));
        dispatch(rtpcrAmpliconsSlice.actions.setSizePerPage(sizePerPage));
    };
}

export const {resetRtpcrAmplicons, setPage, setSizePerPage, appendRtpcrAmplicons} = rtpcrAmpliconsSlice.actions;
export default rtpcrAmpliconsSlice.reducer;
