import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { UploadJob } from "../../../types/amp";
import { appendAndReplace } from "../util";
import { AppDispatch, AppThunk } from "../store";
import map from "lodash/map";
import isArray from "lodash/isArray";
import api from "../../services/api";
import { getParamsFromBootstrapFilters } from "../../util/queryStringUtils";

type UploadJobState = {
    items: Collection<UploadJob>;
    pages: { [key: number]: number[] };
    currentPage: number;
    sizePerPage: number;
    totalSize: number;
};

type AppendPayload = {
    page: number | null;
    items: UploadJob | UploadJob[];
};

const initialState: UploadJobState = {
    items: {},
    pages: {},
    currentPage: 1,
    sizePerPage: 25,
    totalSize: 0,
};

const UploadJobSlice = createSlice({
    name: "upload_jobs",
    initialState,
    reducers: {
        appendJobs(state: UploadJobState, action: PayloadAction<AppendPayload>) {
            state.items = appendAndReplace(state.items, action.payload.items);
            if (action.payload.page != null) {
                state.pages[action.payload.page] = map(
                    isArray(action.payload.items) ? action.payload.items : [action.payload.items],
                    "id"
                );
            }
        },
        setPage(state: UploadJobState, action: PayloadAction<number>) {
            state.currentPage = action.payload;
        },
        setSizePerPage(state: UploadJobState, action: PayloadAction<number>) {
            state.sizePerPage = action.payload;
        },
        setTotalSize(state: UploadJobState, action: PayloadAction<number>) {
            state.totalSize = action.payload;
        },
    },
});

export function fetchJobs(
    page: number,
    sizePerPage: number,
    setLoading: (arg0: boolean) => void,
    filters: any = {},
    sortField: string = "",
    sortOrder: string = ""
): AppThunk {
    return async (dispatch: AppDispatch) => {
        dispatch(UploadJobSlice.actions.setPage(page));
        dispatch(UploadJobSlice.actions.setSizePerPage(sizePerPage));
        const response = await api.get("/api/v2/upload/job", {
            params: { page, sizePerPage, sortField, sortOrder, ...getParamsFromBootstrapFilters(filters)},
        });
        dispatch(
            UploadJobSlice.actions.appendJobs({
                page,
                items: response.data.api_data.items,
            })
        );
        dispatch(UploadJobSlice.actions.setTotalSize(response.data.api_data.total));
        setLoading(false);
    };
}

export function fetchJobById(id: number): AppThunk {
    return async (dispatch: AppDispatch) => {
        const response = await api.get(`/api/v2/upload/job/${id}`);
        dispatch(
            UploadJobSlice.actions.appendJobs({
                page: null,
                items: [response.data.api_data],
            })
        );
    };
}

export function setPagination(page: number, sizePerPage: number): AppThunk {
    return async (dispatch: AppDispatch) => {
        dispatch(UploadJobSlice.actions.setPage(page));
        dispatch(UploadJobSlice.actions.setSizePerPage(sizePerPage));
    };
}

export const { appendJobs } = UploadJobSlice.actions;

export default UploadJobSlice.reducer;
