import { selectSangerAmpliconById } from "../selectors/sanger-amplicons";
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useSelector } from "react-redux";
import { SangerAmplicon, ISearchFilter } from '../../../types/amp';
import { appendAndReplace } from '../util';
import { AppDispatch, AppThunk } from '../store';
import map from 'lodash/map';
import isArray from 'lodash/isArray';
import api from '../../services/api';
import { getQueryParamsFromFilters } from '../../util';

type SangerAmpliconState = {
    items: Collection<SangerAmplicon>;
    pages: { [key: number]: number[] };
    currentPage: number;
    sizePerPage: number;
    totalSize: number;
    loading: boolean;
}


type AppendPayload = {
    page: number | null;
    items: SangerAmplicon | SangerAmplicon[];
};

const initialState: SangerAmpliconState = {
    items: {},
    pages: {},
    currentPage: 1,
    sizePerPage: 25,
    totalSize: 0,
    loading: false,
};

const sangerAmpliconSlice = createSlice({
    name: 'sanger_amplicons',
    initialState,
    reducers: {
        appendSangerAmplicons(state: SangerAmpliconState, action: PayloadAction<AppendPayload>) {
            state.items = appendAndReplace(state.items, action.payload.items);
            if (action.payload.page != null) {
                state.pages[action.payload.page] = map(
                    isArray(action.payload.items) ? action.payload.items : [action.payload.items],
                    'id'
                );
            }
        },
        resetSangerAmplicons(state: SangerAmpliconState, action: PayloadAction ) {
            state.items = {};
            state.pages = {};
            state.currentPage = 1;
            state.sizePerPage = 25;
            state.totalSize = 0;
            state.loading = false;
        },
        setPage(state: SangerAmpliconState, action: PayloadAction<number>) {
            state.currentPage = action.payload;
        },
        setSizePerPage(state: SangerAmpliconState, action: PayloadAction<number>) {
            state.sizePerPage = action.payload;
        },
        setTotalSize(state: SangerAmpliconState, action: PayloadAction<number>) {
            state.totalSize = action.payload;
        },
        setLoadingStatus(state: SangerAmpliconState, action: PayloadAction<boolean>) {
            state.loading = action.payload;
        }
    },
});



export function fetchSangerAmplicons(page: number, sizePerPage: number, filters: ISearchFilter[], sortField?: string, sortOrder?: string): AppThunk {
    return async (dispatch: AppDispatch) => {
        dispatch(sangerAmpliconSlice.actions.setPage(page));
        dispatch(sangerAmpliconSlice.actions.setSizePerPage(sizePerPage));
        dispatch(sangerAmpliconSlice.actions.setLoadingStatus(true));
        const queryParams = getQueryParamsFromFilters(filters)
        queryParams?.append('page', String(page))
        queryParams?.append('sizePerPage', String(sizePerPage))
        if (sortField) {
            queryParams?.append('orderBy', String(sortField))
        }
        if (sortOrder === 'desc') {
            queryParams?.append('desc', '1');
        }
        api.get('/api/v2/amplicon-version', {
            params: queryParams,
        }).then(response => {
            dispatch(sangerAmpliconSlice.actions.appendSangerAmplicons({
                page,
                items: response.data.api_data.items,
            }));
            dispatch(sangerAmpliconSlice.actions.setTotalSize(response.data.api_data.total));
        }).finally(() => {
            dispatch(sangerAmpliconSlice.actions.setLoadingStatus(false));
        })
    };
}

export function fetchSangerAmpliconById(id: number): AppThunk {
    return async (dispatch: AppDispatch) => {
        const response = await api.get(`/api/v2/amplicon-version/${id}`);
        dispatch(sangerAmpliconSlice.actions.appendSangerAmplicons({
            page: null,
            items: [response.data.api_data],
        }));
    };
}

// Perform separate fetch of comments for sanger amplicon
// version, add to existing Redux state of said.
export function fetchSangerAmpliconComments(setLoading: (a: boolean) => void, currentAmplicon: SangerAmplicon): AppThunk {
    return async (dispatch: AppDispatch) => {
        setLoading(true);
        api.get('/api/v2/comment/fetch-by-amplicon-version', {
            params: {ampliconVersionId: currentAmplicon.id}
        }).then(response => {
            const comments = response.data.api_data.items;
            const amplicon = Object.assign({}, currentAmplicon);
            amplicon.comments = comments;
            dispatch(sangerAmpliconSlice.actions.appendSangerAmplicons({
                page: null,
                items: [amplicon]
            }));
        }).finally(() => {
            setLoading(false);
        });
    };
}

export function postCommentForAmpliconVersion(resetFields: () => void, setLoading: (arg0: boolean) => void, ampliconVersion: SangerAmplicon, message: string, pinned: boolean): AppThunk {
    return async (dispatch: AppDispatch) => {
        setLoading(true);
        await api.post('/api/v2/comment/amplicon-version', {
            comment: {message, ampliconVersionName: ampliconVersion.amplicon_version_name, pinned}
        }).then(resp => {
            const amplicon = Object.assign({}, ampliconVersion);
            amplicon.comments = amplicon.comments ? [...amplicon.comments, resp.data.api_data] : [resp.data.api_data];
            dispatch(sangerAmpliconSlice.actions.appendSangerAmplicons({
                page: null,
                items: [amplicon]
            }));
        }).finally(() => {
            resetFields();
            setLoading(false);
        });
    }
}

export function setPagination(page: number, sizePerPage: number): AppThunk {
    return async (dispatch: AppDispatch) => {
        dispatch(sangerAmpliconSlice.actions.setPage(page));
        dispatch(sangerAmpliconSlice.actions.setSizePerPage(sizePerPage));
    };
}

export const { resetSangerAmplicons, setPage, setSizePerPage } = sangerAmpliconSlice.actions;
export default sangerAmpliconSlice.reducer;
