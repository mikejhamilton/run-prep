import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Probe } from "../../../types/amp";
import { appendAndReplace } from "../util";
import { AppDispatch, AppThunk } from "../store";
import map from "lodash/map";
import isArray from "lodash/isArray";
import api from "../../services/api";

type ProbeState = {
    items: Collection<Probe>;
    pages: { [key: number]: number[] };
    currentPage: number;
    sizePerPage: number;
    totalSize: number;
};

type AppendPayload = {
    page: number | null;
    items: Probe | Probe[];
};

const initialState: ProbeState = {
    items: {},
    pages: {},
    currentPage: 1,
    sizePerPage: 25,
    totalSize: 0,
};

const ProbeSlice = createSlice({
    name: "probes",
    initialState,
    reducers: {
        appendProbes(state: ProbeState, action: PayloadAction<AppendPayload>) {
            state.items = appendAndReplace(state.items, action.payload.items);
            if (action.payload.page != null) {
                state.pages[action.payload.page] = map(
                    isArray(action.payload.items) ? action.payload.items : [action.payload.items],
                    "id"
                );
            }
        },
        resetProbes(state: ProbeState, action: PayloadAction ) {
            state.items = {};
            state.pages = {};
            state.currentPage = 1;
            state.sizePerPage = 25;
            state.totalSize = 0;
        },
        setPage(state: ProbeState, action: PayloadAction<number>) {
            state.currentPage = action.payload;
        },
        setSizePerPage(state: ProbeState, action: PayloadAction<number>) {
            state.sizePerPage = action.payload;
        },
        setTotalSize(state: ProbeState, action: PayloadAction<number>) {
            state.totalSize = action.payload;
        },
    },
});

export function fetchProbesByJob(job_id: number, page: number, sizePerPage: number): AppThunk {
    return async (dispatch: AppDispatch) => {
        dispatch(ProbeSlice.actions.setPage(page));
        dispatch(ProbeSlice.actions.setSizePerPage(sizePerPage));
        const response = await api.get(`/api/v2/upload/job/${job_id}/probes`, {
            params: { page, sizePerPage },
        });
        dispatch(
            ProbeSlice.actions.appendProbes({
                page,
                items: response.data.api_data.items,
            })
        );
        dispatch(ProbeSlice.actions.setTotalSize(response.data.api_data.total));
    };
}

export function fetchProbeById(id: number): AppThunk {
    return async (dispatch: AppDispatch) => {
        const response = await api.get(`/api/v2/upload/job/${id}`);
        dispatch(
            ProbeSlice.actions.appendProbes({
                page: null,
                items: [response.data.api_data],
            })
        );
    };
}

export function setPagination(page: number, sizePerPage: number): AppThunk {
    return async (dispatch: AppDispatch) => {
        dispatch(ProbeSlice.actions.setPage(page));
        dispatch(ProbeSlice.actions.setSizePerPage(sizePerPage));
    };
}

export const { appendProbes, resetProbes } = ProbeSlice.actions;

export default ProbeSlice.reducer;
