import {createSlice, PayloadAction} from '@reduxjs/toolkit';

import { AppDispatch, AppThunk } from "../store";
import { appendAndReplace } from "../util";
import api from "../../services/api";
import { Comment } from "../../../types";


type CommentState = {
    items: Collection<Comment>;
    loading: boolean;
};

type AppendPayload = {
    items: Comment | Comment[];
};

const initialState: CommentState = {
    items: {},
    loading: false,
};


const commentSlice = createSlice({
    name: 'comments',
    initialState,
    reducers: {
        appendComments(state: CommentState, action: PayloadAction<AppendPayload>) {
            state.items = appendAndReplace(state.items, action.payload.items);
        },
        resetComments(state: CommentState, action: PayloadAction) {
            state.items = {};
        }
    }
});

export function fetchCommentsByAmpliconVersion(setLoading: (arg0: boolean) => void, ampliconVersionName: string): AppThunk {
    return async (dispatch: AppDispatch) => {
        setLoading(true);
        await api.get('/api/v2/comment/fetch-by-amplicon-version', {
            params: {ampliconVersionName}
        }).then(resp => {
            dispatch(commentSlice.actions.appendComments({
                items: resp.data.api_data.items,
            }));
        }).finally(() => {
            setLoading(false);
        });
    }
}

export function updateComments(comment: Comment): AppThunk {
    return async (dispatch: AppDispatch) => {
        dispatch(commentSlice.actions.appendComments({
            items: comment,
        }));
    }
}

export function postCommentForAmpliconVersion(resetFields: () => void, setLoading: (arg0: boolean) => void, ampliconVersionName: string, message: string, pinned: boolean): AppThunk {
    return async (dispatch: AppDispatch) => {
        setLoading(true);
        await api.post('/api/v2/comment/amplicon-version', {
            comment: {message, ampliconVersionName, pinned}
        }).then(resp => {
            dispatch(commentSlice.actions.appendComments({
                items: resp.data.api_data,
            }));
        }).finally(() => {
            resetFields();
            setLoading(false);
        });
    }
}

export const { resetComments } = commentSlice.actions;
export default commentSlice.reducer;