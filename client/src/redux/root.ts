import { combineReducers } from "@reduxjs/toolkit";
import { connectRouter } from "connected-react-router";
import { reducer as formReducer } from "redux-form";
import history from "./history";
import { comments, events, loading, sanger_amplicons, rtpcr_amplicons, uploadJobs, probes } from "./slices";

const rootReducer = combineReducers({
    sanger_amplicons,
    rtpcr_amplicons,
    comments,
    events,
    loading,
    uploadJobs,
    probes,
    router: connectRouter(history),
    form: formReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
