import { RootState } from '../root';
import { SangerAmplicon } from '../../../types/amp'
import map from 'lodash/map';

export function selectPage(state: RootState, page: number): SangerAmplicon[] {
    const ids = state.sanger_amplicons.pages[page];
    if (undefined === ids) {
        return [];
    }

    return map(ids, id => state.sanger_amplicons.items[id]);
}

export function selectCurrentPageItems(state: RootState) {
    return selectPage(state, state.sanger_amplicons.currentPage);
}

export function selectCurrentPage(state: RootState) {
    return state.sanger_amplicons.currentPage;
}

export function selectTotalSize(state: RootState) {
    return state.sanger_amplicons.totalSize;
}

export function selectSizePerPage (state: RootState) {
    return state.sanger_amplicons.sizePerPage;
}

export function selectLoadingStatus(state: RootState) {
    return state.sanger_amplicons.loading;
}

export function selectSangerAmpliconById(id: number | undefined) {
    
    return (state: RootState) => {
        if (undefined === id) {
            return null;
        } else {
            return state.sanger_amplicons.items[id];
        }
    }
}