import {RootState} from '../root';

export function selectLoading(state: RootState) {
    return state.loading.loading;
}

export function selectTransparent(state: RootState) {
    return state.loading.transparent;
}
