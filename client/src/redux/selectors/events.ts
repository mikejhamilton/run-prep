import map from 'lodash/map';
import { Event } from '../../../types';
import { RootState } from '../root';

export function selectPage(state: RootState, page: number): Event[] {
    const ids = state.events.pages[page];
    if (undefined === ids) {
        return [];
    }

    return map(ids, id => state.events.items[id]);
}

export function selectCurrentPageItems(state: RootState) {
    return selectPage(state, state.events.currentPage);
}

export function selectCurrentPage(state: RootState) {
    return state.events.currentPage;
}

export function selectTotalSize(state: RootState) {
    return state.events.totalSize;
}

export function selectSizePerPage(state: RootState) {
    return state.events.sizePerPage;
}

export function selectEventById(id: number) {
    return (state: RootState) => {
        return id === null ? null : state.events.items[id];
    }
}