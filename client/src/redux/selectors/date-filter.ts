import { RootState } from '../root';
import { formValueSelector } from "redux-form";

const selector = formValueSelector('date-filter');

export function selectLowDateFromFilter(state: RootState) {
    return selector(state, 'lowDate');
}

export function selectHighDateFromFilter(state: RootState) {
    return selector(state, 'highDate');
}