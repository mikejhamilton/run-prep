import { map } from "lodash";

import { RootState } from "../root";
import { Comment } from "../../../types";

export function selectComments(state: RootState): Comment[] {
    return sortComments(Object.values(state.comments.items));
}

export function sortComments(comments: Comment[] | undefined): Comment[] {
    if (comments === undefined) {
        return [];
    }
    return map(comments, (comment: Comment) => comment).sort((commentA: Comment, commentB: Comment) => {
        if (commentA.pinned === commentB.pinned) {
            return commentA.created_at < commentB.created_at ? -1 : 1;
        }
        return commentA.pinned ? -1 : 1;
    });
}