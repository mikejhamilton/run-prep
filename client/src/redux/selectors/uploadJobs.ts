import { RootState } from '../root';
import { UploadJob } from '../../../types/amp'
import map from 'lodash/map';

export function selectPage(state: RootState, page: number): UploadJob[] {
    const ids = state.uploadJobs.pages[page];
    if (undefined === ids) {
        return [];
    }

    return map(ids, id => state.uploadJobs.items[id]);
}

export function selectCurrentPageItems(state: RootState) {
    return selectPage(state, state.uploadJobs.currentPage);
}

export function selectCurrentPage(state: RootState) {
    return state.uploadJobs.currentPage;
}

export function selectTotalSize(state: RootState) {
    return state.uploadJobs.totalSize;
}

export function selectSizePerPage (state: RootState) {
    return state.uploadJobs.sizePerPage;
}

export function selectJobById(id: string) {
    return (state: RootState) => {
        if (undefined === id) {
            return null;
        } else {
            return state.uploadJobs.items[Number(id)];
        }
    }
}