import { RootState } from "../root";
import { Probe } from "../../../types/amp";
import map from "lodash/map";

export function selectPage(state: RootState, page: number): Probe[] {
    const ids = state.probes.pages[page];
    if (undefined === ids) {
        return [];
    }

    return map(ids, (id) => state.probes.items[id]);
}

export function selectCurrentPageItems(state: RootState) {
    return selectPage(state, state.probes.currentPage);
}

export function selectCurrentPage(state: RootState) {
    return state.probes.currentPage;
}

export function selectTotalSize(state: RootState) {
    return state.probes.totalSize;
}

export function selectSizePerPage(state: RootState) {
    return state.probes.sizePerPage;
}

export function selectProbesById(id: string) {
    return (state: RootState) => {
        if (undefined === id) {
            return null;
        } else {
            return state.probes.items[Number(id)];
        }
    };
}
