import map from 'lodash/map';

import { RtpcrAmplicon } from '../../../types/amp'
import { RootState } from '../root';

export function selectPage(state: RootState, page: number): RtpcrAmplicon[] {
    const ids = state.rtpcr_amplicons.pages[page];
    if (undefined === ids) {
        return [];
    }

    return map(ids, id => state.rtpcr_amplicons.items[id]);
}

export function selectCurrentPageItems(state: RootState) {
    return selectPage(state, state.rtpcr_amplicons.currentPage);
}

export function selectCurrentPage(state: RootState) {
    return state.rtpcr_amplicons.currentPage;
}

export function selectTotalSize(state: RootState) {
    return state.rtpcr_amplicons.totalSize;
}

export function selectSizePerPage(state: RootState) {
    return state.rtpcr_amplicons.sizePerPage;
}

export function selectLoadingStatus(state: RootState) {
    return state.rtpcr_amplicons.loading;
}

export const selectRtpcrAmpliconById = (id: number) => {
    return (state: RootState) => (id === null) ? null : state.rtpcr_amplicons.items[id];
}
