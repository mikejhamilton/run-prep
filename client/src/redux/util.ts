import forEach from 'lodash/forEach';
import { Identifiable } from '../../types/amp'

export function appendAndReplace<T extends Identifiable>(items: Collection<T>, itemsToAppend: T[] | T) {
    if (!(itemsToAppend instanceof Array)) {
        itemsToAppend = [itemsToAppend];
    }

    forEach(itemsToAppend, (item: T) => {
        items[item.id] = item;
    });

    return items;
}
