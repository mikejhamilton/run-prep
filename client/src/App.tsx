import React, { lazy } from 'react';
import { AuthProvider } from './auth';
import { RoutesGenerator } from './components';
import './styles/App.scss';

import { BrowserRouter as Router, Redirect} from 'react-router-dom';

import {
    LOGIN,
    LOGOUT,
    CREATE_SANGER_AMPLICON,
    SEARCH_SANGER_AMPLICON,
    JWT,
    INDEX,
    UPDATE_SANGER_AMPLICON,
    DUPLICATE_SANGER_AMPLICON,
    PROBE_UPLOAD,
    PROBE_REVIEW,
    PROBE_STATUS,
    SEARCH_RTPCR_AMPLICON,
    EVENT_LOG,
  } from './constants/routes';

const rootRoutes = [
    {
        path: INDEX,
        component: () => <Redirect to={SEARCH_SANGER_AMPLICON} />
    },
    {
        path: SEARCH_SANGER_AMPLICON,
        meta: { title: 'Search Amplicon' },
        component: lazy(() => import(`./pages/SearchSangerAmpliconPage`))
    },
    {
        path: CREATE_SANGER_AMPLICON,
        meta: { title: 'Create Amplicon' },
        component: lazy(() => import(`./pages/CreateSangerAmpliconPage`))
    },
    {
        path: UPDATE_SANGER_AMPLICON(),
        meta: { title: 'Update Amplicon' },
        component: lazy(() => import(`./pages/CreateSangerAmpliconPage`))
    },
    {
        path: DUPLICATE_SANGER_AMPLICON(),
        meta: { title: 'Update Amplicon' },
        component: lazy(() => import(`./pages/CreateSangerAmpliconPage`))
    },
    {
        path: PROBE_UPLOAD,
        meta: { title: 'Probe Design Upload' },
        component: lazy(() => import(`./pages/ProbeDesignUploadPage`))
    },
    {
        path: PROBE_REVIEW(),
        meta: { title: 'Probe Design Review' },
        component: lazy(() => import(`./pages/ProbeDesignReviewPage`))
    },
    {
        path: PROBE_STATUS,
        meta: { title: 'Probe Design Status' },
        component: lazy(() => import(`./pages/ProbeDesignStatusPage`))
    },
    {
        path: SEARCH_RTPCR_AMPLICON,
        meta: { title: 'Search RT PCR Amplicon' },
        component: lazy(() => import('pages/RtpcrAmpliconPage'))
    },
    {
        path: EVENT_LOG,
        meta: { title: 'Event Log' },
        component: lazy(() => import(`./pages/EventLogPage`))
    },
    {
        path: JWT,
        meta: { title: 'JWT' },
        component: lazy(() => import(`./pages/JwtPage`))
    },
    {
      path: LOGIN,
      authenticated: false,
      meta: { title: 'Login' },
      component: lazy(() => import(`./auth/Login`))
    },
    {
      path: LOGOUT,
      authenticated: false,
      meta: { title: 'Logout' },
      component: lazy(() => import(`./auth/Logout`))
    },
  ];

const App: React.FC = () => {
    return (
      <AuthProvider>
          <Router>
            <RoutesGenerator routes={rootRoutes} />
          </Router>
       </AuthProvider>
    );
  };

export default App;