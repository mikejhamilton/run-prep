import Noty from 'noty';

export function alertInfo(message: string, timeout=5000) {
    noty(message, 'info', timeout);
}

export function alertSuccess(message: string, timeout=5000) {
    noty(message, 'success', timeout);
}

export function alertError(message: string, timeout=0) {
    noty(sanitize(message), 'error', timeout)
}

// Remove escaped newlines, bookended double quotes
const sanitize = (message: string): string => message
        .replace(/\\n/g, '')
        .replace(/^"?(.*?)"?$/, '$1');

function noty(message: string, type: Noty.Type, timeout: number) {
    new Noty({
        type: type,
        text: message,
        layout: 'bottomRight',
        timeout: timeout,
        theme: 'relax',
    }).show()
}
