import axios from 'axios';
import {alertError} from './alert';
import AuthClient from '../auth/AuthClient';
import getConfigValue from "util/getConfigValue";


const api = axios.create({
    baseURL: process.env.AMP_BASE_URL,
});


const authClient = new AuthClient();

api.interceptors.request.use((config) => {
    return authClient.getAccessToken().then(token => {
        if (token !== undefined) {
            // @ts-ignore
            config.headers.Authorization = 'Bearer ' + token.accessToken;
        }
        if (getConfigValue('APP_ENV') !== 'production'){
            config.headers.AuthOverride = localStorage.getItem("backend-auth-override");
        }
        return config;
      });
});

api.interceptors.response.use(response => response, (error) => {
    if (error.response && error.response.data && !error.response.headers.interceptor_ignore) {
            alertError(JSON.stringify(error.response.data));
        }
        return Promise.reject(error);
    });

export default api;
