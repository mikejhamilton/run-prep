import { useEffect, useRef, EffectCallback } from 'react';

/**
 * Use the previous value of a prop or state var
 * Taken from: https://usehooks.com/usePrevious/
 */
export function usePrevious(value: any) {
    // The ref object is a generic container whose current property is mutable ...
    // ... and can hold any value, similar to an instance property on a class
    const ref = useRef();

    // Store current value in ref
    useEffect(() => {
        ref.current = value;
    }, [value]); // Only re-run if value changes

    // Return previous value (happens before update in useEffect above)
    return ref.current;
}

export function useTitle(title: string) {
    useEffect(() => {
        document.title = title ? `${title} | Ambry Lab` : 'Ambry Lab';
    }, [title]);
}


/**
 * This is mainly to explicitly identify we wanted a componentDidMount effect
 * It also removes eslint warnings for exhaustive dependencies in the dependency array
 */
export function useDidMount(effect: EffectCallback) {
    useEffect(effect, []);
}
