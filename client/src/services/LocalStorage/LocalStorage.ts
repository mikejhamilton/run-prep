/**
 * Local storage wrapper to handle the json parsing for us
 * Also retain the key so we only need to worry about the value
 */
class LocalStorage<T extends any = any> {
  constructor(private readonly key: string) {}

  get(): T | null {
    const data = localStorage.getItem(this.key);

    if (data !== null) {
      try {
        return JSON.parse(data);
      } catch (e) {
        console.error(
          `Couldn't parse item out of localStorage ${this.key}`,
          data
        );
      }
    }

    return null;
  }

  set(value: T): void {
    localStorage.setItem(this.key, JSON.stringify(value));
  }

  remove(): void {
    localStorage.removeItem(this.key);
  }

  /**
   * Retrieve and remove value from localstorage
   */
  pull(): T | null {
    const val = this.get();
    this.remove();
    return val;
  }
}

export default LocalStorage;
