import LocalStorage from './LocalStorage';
import { USER_LOCALSTORAGE_KEY } from '../../constants';

export interface UserData {
  username: string;
  groups: string[];
}

export default class UserStorage extends LocalStorage<UserData> {
  constructor() {
    super(USER_LOCALSTORAGE_KEY);
  }
}
