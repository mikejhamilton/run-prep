import './error-panel.scss';
import React from 'react';
import Separator from '../common/Separator';

export default function ErrorPanel({children}) {
    return (
        <div className="error-panel">
            {children}
        </div>
    );
}

ErrorPanel.Title = function ErrorPanelTitle({children}) {
    return (
        <>
            <h1 className="error-panel__title">
                {children}
            </h1>
            <Separator/>
        </>
    );
};

ErrorPanel.Body = function ErrorPanelBody({children}) {
    return (
        <div className="error-panel__title">
            {children}
        </div>
    );
};
