import React from 'react';
import ErrorPanel from './ErrorPanel';

const AccessDenied: React.FC = () => {
    return (
        <ErrorPanel>
            <ErrorPanel.Title>Access Denied</ErrorPanel.Title>
            <ErrorPanel.Body>
                <h4>Manager Approval Needed</h4>
                <p>
                    You do not have the correct permissions to access this application.<br/>
                    If you believe you should have access, please contact your manager so they can submit a request
                    to <a href="mailto:EvoAccess@ambrygen.com">EvoAccess@ambrygen.com</a>.
                </p>
            </ErrorPanel.Body>
        </ErrorPanel>
    );
};

export default AccessDenied;
