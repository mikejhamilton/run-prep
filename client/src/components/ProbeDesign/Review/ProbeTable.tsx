import { isEmpty } from "lodash";
import React, { useState } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import ToolkitProvider from "react-bootstrap-table2-toolkit";
import paginationFactory from "react-bootstrap-table2-paginator";
import Select from "react-select";
import { getConfigValue } from "../../../util";
import { Badge } from "react-bootstrap";

interface ProbeReviewTableProps {
    probes: any;
    technology: string;
    totalColumns: any[];
    defaultColumns: string[];
    page: number;
    sizePerPage: number;
    totalSize: number;
    handler: any;
}
interface ColumnValues {
    dataField: string;
    text: string;
}
interface OptionValues {
    value: string;
    label: string;
}
const ProbeReviewTable: React.FC<ProbeReviewTableProps> = (props) => {
    const [columnsSelected, setSelectedColumns] = useState(props.defaultColumns);

    const handleChange = (selectedOption: any) => {
        if (selectedOption === null) {
            setSelectedColumns([]);
        } else {
            setSelectedColumns(selectedOption.map((column: OptionValues) => column.value));
        }
    };

    const convertColumnsToOptions = (columns: any) => {
        return columns.map((column: ColumnValues) => ({
            value: column.dataField,
            label: column.text,
        }));
    };

    const addFormatters = (columns: any) => {
        return columns.map((column: any) => {
            // is_snp formatter
            if (column.dataField == "is_snp") {
                return {
                    ...column,
                    formatter: (cell: any) => (cell ? "True" : "False"),
                };
            } else {
                return column;
            }
        });
    };

    return (
        <ToolkitProvider
            keyField="id"
            data={props.probes}
            columns={addFormatters(
                props.totalColumns.filter((column) => columnsSelected.includes(column.dataField))
            )}
        >
            {(TKProps) => (
                <div>
                    <Badge>Number of probes: {props.totalSize}</Badge>
                    <Select
                        closeMenuOnSelect={false}
                        isMulti
                        onChange={handleChange}
                        defaultValue={convertColumnsToOptions(
                            props.totalColumns.filter((column: ColumnValues) =>
                                columnsSelected.includes(column.dataField)
                            )
                        )}
                        options={convertColumnsToOptions(props.totalColumns)}
                    />
                    <hr />
                    {isEmpty(columnsSelected) ? null : (
                        <BootstrapTable
                            {...TKProps.baseProps}
                            remote
                            bordered={false}
                            pagination={paginationFactory({
                                page: props.page,
                                sizePerPage: props.sizePerPage,
                                totalSize: props.totalSize,
                            })}
                            onTableChange={props.handler}
                        />
                    )}
                </div>
            )}
        </ToolkitProvider>
    );
};

export default ProbeReviewTable;
