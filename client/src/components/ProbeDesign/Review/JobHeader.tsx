import React from "react";
import { UploadJob } from "../../../../types/amp";
import styles from "./JobHeader.module.scss";
import { Col, Container, Row } from "react-bootstrap";
import ReviewJobButtonPanel from "./ReviewJobButtonPanel";
import UTCToLocaleDate from "../../../util/UTCToLocaleDate";

interface JobHeaderProps {
    job: UploadJob;
}

const JobHeader: React.FC<JobHeaderProps> = (props) => {
    const status_text =
        props.job.job_status == "RETIRED" ? (
            <p style={{ color: "red" }}>{props.job.job_status}</p>
        ) : (
            <p>{props.job.job_status}</p>
        );

    return (
        <Container className={styles.headerBox}>
            <hr className={styles.hr} />
            <Row>
                <Col className={styles.title}>
                    <h2>Job ID</h2>
                </Col>
                <Col className={styles.title}>
                    <p>{props.job.id}</p>
                </Col>
            </Row>
            <hr className={styles.hr} />
            <Row>
                <Col className={styles.title}>
                    <h2>Libraries</h2>
                </Col>
                <Col className={styles.title}>
                    <p>{props.job.libraries.map(library => library.name).join(", ")}</p>
                </Col>
            </Row>
            <hr className={styles.hr} />
            <Row>
                <Col className={styles.title}>
                    <h2>Technology</h2>
                </Col>
                <Col className={styles.title}>
                    <p>{props.job.technology}</p>
                </Col>
            </Row>
            <hr className={styles.hr} />
            <Row>
                <Col className={styles.title}>
                    <h2>Created</h2>
                </Col>
                <Col className={styles.title}>
                    <p>{UTCToLocaleDate(props.job.created_at)}</p>
                </Col>
            </Row>
            <hr className={styles.hr} />
            <Row>
                <Col className={styles.title}>
                    <h2>Files Uploaded</h2>
                </Col>
                <Col className={styles.title}>
                    <p>{props.job.number_of_files}</p>
                </Col>
            </Row>
            <hr className={styles.hr} />
            <Row>
                <Col className={styles.title}>
                    <h2>Status</h2>
                </Col>
                <Col className={styles.title}>{status_text}</Col>
            </Row>
            <hr className={styles.hr} />
            <ReviewJobButtonPanel job={props.job} />
        </Container>
    );
};

export default JobHeader;
