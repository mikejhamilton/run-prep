import React, { useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { hasAccess } from "../../../util";
import { UploadJob } from "../../../../types/amp";
import styles from "./ReviewJobButtonPanel.module.scss";
import api from "../../../services/api";
import { alertError, alertSuccess } from "../../../services/alert";
import { appendJobs } from "../../../redux/slices/uploadJobs";
import { PROBE_DESIGN_GROUPS } from "../../../config/userPermissions";
import { useDispatch, useSelector } from "react-redux";
import PermissionOverlayButton from "../../common/PermissionOverlayButton";
import { useAuth } from "auth";

interface ReviewJobButtonPanelProps {
    job: UploadJob;
}
/**
 * Returns review buttons to transition a job to a new status
 */
const ReviewJobButtonPanel: React.FC<ReviewJobButtonPanelProps> = (
    props: ReviewJobButtonPanelProps
) => {
    const dispatch = useDispatch();
    const {getUserGroups} = useAuth();
    const userGroups = getUserGroups();
    const probeDesignAccess = hasAccess(userGroups, PROBE_DESIGN_GROUPS);
    const [loading, setLoading] = useState(false);

    /**
     * Updates a job to a new staus in the backend
     */
    const updateJobStatus = (job: UploadJob, status: string) => {
        setLoading(true);
        api.put("/api/v2/upload/job/update-status", { job: job, status: status })
            .then((res) => {
                alertSuccess("Job Updated");
                dispatch(appendJobs({ page: null, items: [res.data.api_data] }));
            })
            .catch((e) => {
                alertError("Error: " + e.response.data.errors);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    // Only display approve button if pending review
    if (props.job.job_status == "PENDING_REVIEW") {
        return (
            <Container className={styles.buttonBox}>
                <div>
                    {PermissionOverlayButton(
                        probeDesignAccess,
                        loading,
                        "Approve",
                        () => updateJobStatus(props.job, "PENDING_ACTIVATION"),
                        { width: "100%" }
                    )}
                </div>
            </Container>
        );
    } else {
        // Only display validate, retire or both buttons depending on job status
        return (
            <Container className={styles.buttonBox}>
                <Row>
                    {props.job.job_status == "PENDING_ACTIVATION" ? (
                        <Col>
                            {PermissionOverlayButton(
                                probeDesignAccess,
                                loading,
                                "Make Active",
                                () => updateJobStatus(props.job, "ACTIVATED"),
                                { width: "100%" }
                            )}
                        </Col>
                    ) : null}
                    {props.job.job_status == "PENDING_ACTIVATION" ||
                    props.job.job_status == "ACTIVATED" ? (
                        <div>
                            <Col>
                                {PermissionOverlayButton(
                                    probeDesignAccess,
                                    loading,
                                    "Retire",
                                    () => updateJobStatus(props.job, "RETIRED"),
                                    { width: "100%" }
                                )}
                            </Col>
                        </div>
                    ) : null}
                </Row>
            </Container>
        );
    }
};

export default ReviewJobButtonPanel;
