import React, { Dispatch, useState } from "react";
import { Field, getFormValues } from "redux-form";
import { ReduxFileFormGroup } from "../../form/redux-form-group";
import { useSelector } from "react-redux";
import { Col, Row } from "react-bootstrap";
import api from "../../../services/api";
import SubmitUploadButton from "./SubmitUploadButton";
import {alertSuccess, alertError, alertInfo } from "../../../services/alert";
import { getConfigValue } from "../../../util";
import { isArray } from "lodash";

const TargetedMicroArrayUploadForm: React.FC = () => {
    const [designFile, setDesignFile] = useState([]);

    const formValues: any = useSelector((state) => {
        return getFormValues("probeUpload")(state);
    });

    const handleSubmit = (setLoading: Dispatch<React.SetStateAction<boolean>>) => {
        setLoading(true);
        if (!formValues.library) {
            alertError("Library field cannot be blank.")
            setLoading(false);
            return
        }
        let data = new FormData();
        data.append("design_file", designFile[0]);
        data.append("technology", formValues.technology.value);
        isArray(formValues.library)
            ? data.append("library", formValues.library.map((library: {value: string}) => library.value))
            : data.append("library", formValues.library.value)


        alertInfo("Starting upload, please don't navigate away from this page");
        api.post("/api/v2/probe_upload/", data, {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }).then((res) => {
            alertSuccess(`Job Created (ID: ${res.data.api_data.job_id})`, 6000)
        }).catch((err) => {
            alertError(err.response.data.errors)
        }).finally(() => {
            setLoading(false);
        });
    };

    return (
        <React.Fragment>
            <Row className="justify-content-center">
                <Col xl="6">
                    <Field
                        label="Designs - .xml"
                        name="design_file"
                        component={ReduxFileFormGroup}
                        setFiles={setDesignFile}
                        accept=".xml"
                        maxSize={getConfigValue("MAX_PROBE_UPLOAD_SIZE")}
                    />
                </Col>
            </Row>
            <div className="d-flex justify-content-center">
                <SubmitUploadButton handleSubmit={handleSubmit} />
            </div>
        </React.Fragment>
    );
};

export default TargetedMicroArrayUploadForm;
