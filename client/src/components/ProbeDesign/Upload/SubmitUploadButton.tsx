import React, { useState, Dispatch } from "react";
import { Button, Spinner } from "react-bootstrap";

type SubmitProps = {
    handleSubmit: (setLoading: Dispatch<React.SetStateAction<boolean>>) => void;
};

/**
 * Custom Submit button for probe uploads
 * Gets onClick function from props and passes loading setter to onClick function.
 */
const SubmitUploadButton: React.FC<SubmitProps> = (props: SubmitProps) => {
    const [loading, setLoading] = useState(false);
    const submitText = loading ? (
        <Spinner animation="border" role="status" aria-hidden="true" />
    ) : (
        "Submit"
    );
    return <Button disabled={loading} onClick={() => props.handleSubmit(setLoading)}>{submitText}</Button>;
};

export default SubmitUploadButton;
