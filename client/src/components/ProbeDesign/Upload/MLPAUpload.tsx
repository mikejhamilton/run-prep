import React, { useState, Dispatch } from "react";
import { Field, getFormValues } from "redux-form";
import { ReduxFileFormGroup, ReduxInputFormGroup } from "../../form/redux-form-group";
import { useSelector } from "react-redux";
import { Col, Row } from "react-bootstrap";
import api from "../../../services/api";
import SubmitUploadButton from "./SubmitUploadButton";
import {alertSuccess, alertError} from "../../../services/alert";
import { getConfigValue } from "../../../util";
import { isArray } from "lodash";
import { maxLength, required } from "components/form/validators";

const MLPAUploadForm: React.FC = () => {
    const [seqFiles, setSeqFiles] = useState([]);
    const [isoformFile, setIsoformFile] = useState([]);

    const formValues: any = useSelector((state) => {
        return getFormValues("probeUpload")(state);
    });

    const handleSubmit = (setLoading: Dispatch<React.SetStateAction<boolean>>) => {
        setLoading(true);
        if (!formValues.library) {
            alertError("Library field cannot be blank.")
            setLoading(false);
            return
        }
        let data = new FormData();
        data.append("technology", formValues.technology.value);
        data.append("isoform_file", isoformFile[0]);
        seqFiles.map((f: any, idx: number) => {
            data.append('seq_file_'+idx, f);
        });

        isArray(formValues.library)
            ? data.append("library", formValues.library.map((library: {value: string}) => library.value))
            : data.append("library", formValues.library.value);

        api.post("/api/v2/probe_upload/", data, {
            headers: {
                "Content-Type": "multipart/form-data",
            },
        }).then((res) => {
            alertSuccess(`Job Created (ID: ${res.data.api_data.job_id})`, 6000)
        }).catch((err) => {
            alertError(err.response.data.errors)
        }).finally(() => {
            setLoading(false);
        });
    };

    return (
        <React.Fragment>
            <Row className="justify-content-center">
                <Col xl="6">
                    <Field
                        label="Probe sequence - .xls"
                        name="sequence_files"
                        component={ReduxFileFormGroup}
                        setFiles={setSeqFiles}
                        accept=".xls,.xlsx"
                        maxSize={getConfigValue("MAX_PROBE_UPLOAD_SIZE")}
                    />
                </Col>
            </Row>
            <Row className="justify-content-center">
                <Col xl="6">
                    <Field
                        label="Isoforms - .txt"
                        name="isoform_files"
                        component={ReduxFileFormGroup}
                        setFiles={setIsoformFile}
                        accept=".txt"
                        maxSize={getConfigValue("MAX_PROBE_UPLOAD_SIZE")}
                    />
                </Col>
            </Row>
            <div className="d-flex justify-content-center">
                <SubmitUploadButton handleSubmit={handleSubmit} />
            </div>
        </React.Fragment>
    );
};

export default MLPAUploadForm;
