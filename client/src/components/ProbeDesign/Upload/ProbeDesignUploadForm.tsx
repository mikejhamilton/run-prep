import React from "react";
import { Field, FormSection, getFormValues, InjectedFormProps} from "redux-form";
import { ReduxSelectFormGroup } from "../../form/redux-form-group";
import { reduxForm } from "redux-form";
import { useSelector } from "react-redux";
import MLPAUploadForm from "./MLPAUpload";
import TargetedMicroArrayUploadForm from "./TargetedMicroArrayUpload";
import NGSUploadForm from "./NGSUpload";
import { Col, Container, Row } from "react-bootstrap";
import styles from "./ProbeDesignUpload.module.scss";
import { ReduxFormMultiSelect } from "../../form/redux-form-group";
import { getConfigValue } from '../../../util';

const required = (value: string) => value ? undefined : 'Required'
const maxLength = (max: number) => (value: string) =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined;

/**
 * Form to upload probes for NGS, MLPA, and MicroArray
 */
const ProbeDesignUploadForm: React.FC<InjectedFormProps> = () => {
    const formValues: any = useSelector((state) => {
        return getFormValues("probeUpload")(state);
    });
    let uploadForm = <MLPAUploadForm />;
    if (formValues?.technology.value == "MLPA") {
        uploadForm = <MLPAUploadForm />;
    } else if (formValues?.technology.value == "Targeted MicroArray") {
        uploadForm = <TargetedMicroArrayUploadForm />;
    }

    const libraryOptions = getConfigValue("PROBE_LIBRARIES").split(',').map((library) => {
        return {
            label: library,
            value: library,
        }
    })

    return (
        <Container fluid className={styles.uploadForm}>
            <form>
                <Row className="justify-content-center" style={{paddingBottom: "20px"}}>
                    <Col xl="4">
                        <Field
                            label="Technology"
                            name="technology"
                            component={ReduxFormMultiSelect}
                            options={[
                                {label: "MLPA", value: "MLPA"},
                                {label: "Targeted MicroArray", value: "Targeted MicroArray"},
                            ]}
                        />
                    </Col>
                    <Col xl="4">
                        <Field
                            isMulti
                            label="Library"
                            name="library"
                            component={ReduxFormMultiSelect}
                            options={libraryOptions}
                        />
                    </Col>
                </Row>
                <FormSection name="upload">{uploadForm}</FormSection>
            </form>
        </Container>
    );
};

export default reduxForm<{}>({
    form: "probeUpload",
})(ProbeDesignUploadForm);
