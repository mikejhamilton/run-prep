import React from "react";
import { Badge, Button } from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import filterFactory, { textFilter, Comparator } from "react-bootstrap-table2-filter";
import styles from "./StatusTable.module.scss";
import { useHistory } from "react-router";
import UTCToLocaleDate from "../../../util/UTCToLocaleDate";

interface JobStatusTableProps {
    jobs: Array<object>;
    page: number;
    sizePerPage: number;
    totalSize: number;
    handler: any;
}

const ProbeDesignStatusTable: React.FC<JobStatusTableProps> = (props: JobStatusTableProps) => {
    
    const history = useHistory()
    const ReviewButton = (cell: any, row: any) => {
        let buttonText = "Details"
        if (row.job_status == "PENDING_REVIEW") {
            buttonText = "Approve";
        } else if (row.job_status == "PENDING_ACTIVATION") {
            buttonText = "Activate";
        }
        return <Button variant="secondary" onClick={() => history.push(`/probe/upload/review/${row.id}`)}>{buttonText}</Button>;
    };

    function statusFormatter(value: string) {
        let variant = "info";
        switch (value) {
            case "PENDING_REVIEW":
                variant = "warning";
                break;
            case "PENDING_ACTIVATION":
                variant = "warning";
                break;
            case "ACTIVATED":
                variant = "success";
                break;
            case "RETIRED":
                variant = "danger";
                break;
            default:
                variant = "info";
        }
        return (
            <Badge className={styles.validationBadge} variant={variant}>
                {value}
            </Badge>
        );
    }
    function libraryFormatter(values: [{name: string}]) {
        return values.map(value => value.name).join(', ')
    }

    const columns = [
        {
            dataField: "id",
            text: "ID",
            sort: true,
            filter: textFilter({ className: styles.idFilter, comparator: Comparator.EQ }),
        },
        {
            dataField: "libraries",
            text: "Libraries",
            formatter: libraryFormatter
        },
        {
            dataField: "technology",
            text: "Technology",
            sort: true,
            filter: textFilter({ className: styles.idFilter }),
        },
        {
            dataField: "created_at",
            text: "Date",
            sort: true,
            formatter: UTCToLocaleDate
        },
        {
            dataField: "number_entries_created",
            text: "Number of Probes",
            sort: true,
        },
        {
            dataField: "job_status",
            text: "Status",
            sort: true,
            filter: textFilter({ className: styles.idFilter }),
            formatter: statusFormatter,
        },
        {
            dataField: "review",
            text: "Review",
            formatter: ReviewButton,
        },
    ];
    return (
        <BootstrapTable
            remote
            keyField="id"
            data={props.jobs}
            columns={columns}
            bordered={false}
            pagination={paginationFactory({
                page: props.page,
                sizePerPage: props.sizePerPage,
                totalSize: props.totalSize,
            })}
            onTableChange={props.handler}
            filter={filterFactory()}
        />
    );
};

export default ProbeDesignStatusTable;
