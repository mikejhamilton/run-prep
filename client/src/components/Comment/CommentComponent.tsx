import React from "react";

import { POST_COMMENT_GROUPS } from "config/userPermissions";
import { useLocation, useParams } from "react-router-dom";
import { useAuth } from "auth";
import hasAccess from "util/hasAccess";
import { useSelector } from "react-redux";
import { selectSangerAmpliconById } from "../../redux/selectors/sanger-amplicons";
import CommentForm from "./CommentForm";
import CommentFormList from "./CommentFormList";

const CommentComponent: React.FC = () => {
    const location = useLocation();
    const newAmplicon = location.pathname.search("create") !== -1;

    // Get amplicon version ID from the URL
    const {id} = useParams();
    const ampliconVersionName = useSelector(selectSangerAmpliconById(id))?.amplicon_version_name;

    const {getUserGroups} = useAuth();
    const userGroups = getUserGroups();
    const hasPostCommentAccess = hasAccess(userGroups, POST_COMMENT_GROUPS);

    return newAmplicon ? null : <>
        {ampliconVersionName && (
            <>
                {hasPostCommentAccess && <CommentForm ampliconVersionName={ampliconVersionName}/>}
                <CommentFormList ampliconVersionName={ampliconVersionName} hasPostCommentAccess={hasPostCommentAccess}/>
            </>
        )}
    </>;
};

export default CommentComponent;