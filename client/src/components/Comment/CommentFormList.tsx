import React, { useEffect, useState } from 'react'

import { map } from "lodash";
import { Col, Row, Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { resetComments } from "redux/slices/comments";
import "font-awesome/css/font-awesome.min.css";
import UTCToLocaleDate from "util/UTCToLocaleDate";

import { Comment, SangerAmplicon } from "../../../types";
import { selectComments } from "../../redux/selectors/comments";
import { fetchCommentsByAmpliconVersion } from "../../redux/slices/comments";
import ampliconDesignStyles from "../AmpliconVersion/AmpliconPrimerDesign/AmpliconPrimerDesign.module.scss";
import Loading from "../common/Loading";
import styles from "./Comments.module.scss";
import PinComponent from "./PinComponent";

interface CommentListProps {
    ampliconVersionName: string;
    hasPostCommentAccess?: boolean;
}

const CommentFormList: React.FC<CommentListProps> = ({
                                                     ampliconVersionName,
                                                     hasPostCommentAccess = false
                                                 }) => {
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch();

    const comments: Comment[] = useSelector(selectComments);
    useEffect(() => {
        dispatch(fetchCommentsByAmpliconVersion(setLoading, ampliconVersionName));
    }, [dispatch]);

    // Reset on dismount
    useEffect(() => {
        return () => {
            dispatch(resetComments());
        }
    }, []);

    if (!loading && comments.length === 0) {
        return null;
    }

    return loading ? (
        <Loading />
    ) : (
        <Row>
            <Col>
                {!hasPostCommentAccess && <h4 className={ampliconDesignStyles.primerTitle}>Comments</h4>}
            </Col>
            <Col lg={9}>
                <div className={styles.commentTableContainer}>
                    <Table className={styles.commentTable} borderless striped>
                        <tbody>
                        {map(comments, (comment: Comment) => (
                            <tr key={comment.id}>
                                <td className={styles.pinTd}>
                                    <PinComponent
                                        comment={comment}
                                        editable={hasPostCommentAccess}
                                    />
                                </td>
                                <td className={styles.dateTd}>{comment.created_at ? UTCToLocaleDate(comment.created_at) : ''}</td>
                                <td>{comment.message}</td>
                                <td>{comment.username}</td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                </div>
            </Col>
            <Col/>
        </Row>
    );
};

export default CommentFormList;