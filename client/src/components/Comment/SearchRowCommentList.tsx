import React, { useEffect, useState } from 'react'

import { map } from "lodash";
import { Col, Row, Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import "font-awesome/css/font-awesome.min.css";
import UTCToLocaleDate from "util/UTCToLocaleDate";

import { Comment } from "../../../types";
import { sortComments } from "../../redux/selectors/comments";
import { selectSangerAmpliconById } from "../../redux/selectors/sanger-amplicons";
import { fetchSangerAmpliconComments } from "../../redux/slices/sanger-amplicons";
import { ListGroupCard } from "../common/cards/ListGroup";
import Loading from "../common/Loading";
import styles from "./Comments.module.scss";
import PinComponent from "./PinComponent";

interface CommentListProps {
    ampliconVersionId: number;
}

const SearchRowCommentList: React.FC<CommentListProps> = ({ampliconVersionId}) => {
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch();

    const currentAmplicon = useSelector(selectSangerAmpliconById(ampliconVersionId));
    useEffect(() => {
        if (currentAmplicon) {
            dispatch(fetchSangerAmpliconComments(setLoading, currentAmplicon));
        }
    }, [dispatch]);

    const comments = sortComments(currentAmplicon?.comments);

    if (!loading && comments.length === 0) {
        return null;
    }

    return <ListGroupCard title="Comments">
        <Row>
            <Col>
                {loading ? (<Loading />) : <Table className={styles.commentTable} borderless striped>
                    <tbody>
                    {map(comments, (comment: Comment) => (
                        <tr key={comment.id}>
                            <td className={styles.pinTd}><PinComponent comment={comment} /></td>
                            <td className={styles.dateTd}>{comment.created_at ? UTCToLocaleDate(comment.created_at) : ''}</td>
                            <td>{comment.message}</td>
                            <td>{comment.username}</td>
                        </tr>
                    ))}
                    </tbody>
                </Table>}
            </Col>
        </Row>
    </ListGroupCard>;
};

export default SearchRowCommentList;