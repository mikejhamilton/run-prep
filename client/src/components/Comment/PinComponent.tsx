import React, { useState } from "react";
import { useDispatch } from "react-redux";

import styles from "components/Comment/Comments.module.scss";
import Loading from "components/common/Loading";
import { updateComments } from "redux/slices/comments";
import { alertError } from "services/alert";
import api from "services/api";
import { Comment } from "../../../types";

interface PinComponentProps {
    comment: Comment;
    editable?: boolean;
}

const PinComponent: React.FC<PinComponentProps> = ({comment, editable = false}) => {
    const [loading, setLoading] = useState(false);

    const dispatch = useDispatch();

    const postCommentPin = (comment: Comment) => {
        setLoading(true);
        api.put(
            `/api/v2/comment/pin/${comment.id}`,
            {pinned: !comment.pinned},
            {headers: {'Content-Type': 'application/json'}}
        ).then(() => {
            dispatch(updateComments({...comment, pinned: !comment.pinned}))
        }).catch((err) => {
            alertError(err.response.data.errors);
        }).finally(() => {
            setLoading(false);
        });
    };

    return loading ? (
        <Loading size='sm'/>
    ) : (
        <SimplePinComponent pinned={comment.pinned} editable={editable} onClick={() => postCommentPin(comment)} />
    );
};

interface SimplePinComponentProps {
    pinned: boolean;
    onClick: any;
    editable?: boolean;
}

export const SimplePinComponent: React.FC<SimplePinComponentProps> = ({pinned, editable = true, onClick}) => {
    return <span
        className={`${pinned ? styles.pinned : styles.unpinned} ${editable ? styles.pinEditable : ''}`}
        onClick={editable ? onClick : null}
    >
        <i className={`fa fa-thumb-tack ${styles.commentPin} `}/>
    </span>;
}

export default PinComponent;