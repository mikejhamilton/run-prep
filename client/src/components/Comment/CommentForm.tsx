import React, { useState } from 'react';
import { Button, Col, Row, Spinner } from "react-bootstrap";
import { FormSection } from "redux-form";
import { useDispatch } from "react-redux";

import { postCommentForAmpliconVersion } from "../../redux/slices/comments";
import ampliconDesignStyles from "../AmpliconVersion/AmpliconPrimerDesign/AmpliconPrimerDesign.module.scss";
import { SimplePinComponent } from "./PinComponent";

interface CommentFormProps {
    ampliconVersionName: string,
}

const CommentForm: React.FC<CommentFormProps> = ({ampliconVersionName}) => {
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState('');
    const [pinned, setPinned] = useState(false);

    const dispatch = useDispatch();

    const handleSubmit = () => {
        dispatch(postCommentForAmpliconVersion(
            resetFields, setLoading, ampliconVersionName, message, pinned
        ));
    }

    const resetFields = () => {
        setMessage('');
        setPinned(false);
    }

    const updateMessage = (e: any) => {
        setMessage(e.target.value);
    }

    const updatePinned = () => {
        setPinned(!pinned);
    }

    return <FormSection name="comment">
            <Row>
                <Col>
                    <h4 className={ampliconDesignStyles.primerTitle}>Comment</h4>
                </Col>
                <Col xs={7}>
                    <Row>
                        <Col xs={1} style={{textAlign: "center"}}>
                            <SimplePinComponent pinned={pinned} onClick={updatePinned} />
                        </Col>
                        <Col xs={11}>
                            <input
                                value={message}
                                disabled={loading}
                                className="form-control"
                                name="message"
                                type="text"
                                onChange={updateMessage}
                                required
                            />
                        </Col>
                    </Row>
                </Col>
                <Col xs={1}>
                    <Button disabled={loading || message === ''} className="btn-sm" onClick={handleSubmit}>{loading ? <Spinner animation="border"/> : "Add"}</Button>
                </Col>
                <Col xs={1} />
                <Col />
            </Row>
        </FormSection>;
};

export default CommentForm;