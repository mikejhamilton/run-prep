import React, { DetailedHTMLProps, FC } from 'react';
import { Row, Col } from 'react-bootstrap';
import Logo from '../../images/logo.png';
import './index.scss';

const Index: FC<DetailedHTMLProps<
  React.ImgHTMLAttributes<HTMLImageElement>,
  HTMLImageElement
>> = ({ children, ...props }) => (
  <Row className="ambry-page-loader justify-content-center align-items-center">
    <Col xs="auto">
      <img {...props} src={Logo} alt="Ambry Genetics" className="ambry-logo" />
      {children}
    </Col>
  </Row>
);

export default Index;
