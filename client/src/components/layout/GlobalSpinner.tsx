import './global-spinner.scss';
import React from 'react';
import {useSelector} from 'react-redux';
import {selectLoading, selectTransparent} from '../../redux/selectors/loading';

const GlobalSpinner: React.FC = () => {
    const isLoading = useSelector(selectLoading);
    const isTransparent = useSelector(selectTransparent);

    if (!isLoading) {
        return null;
    }

    const style = {
        backgroundColor: isTransparent ? 'rgba(255, 255, 255, .5)' : '#fff',
    };

    return (
        <div className="global-spinner" style={style}>
            <div className="global-spinner__spinner spinner-border text-primary"/>
        </div>
    );
};

export default GlobalSpinner;
