import React, { useEffect, useState } from 'react';
import Logo from '../../images/logo.png';
import {Navbar, Button } from 'react-bootstrap';
import useAuth from '../../auth/useAuth';
import {Link, useHistory} from 'react-router-dom';
import "./header.scss"
import Mimic from "./Mimic";
import getConfigValue from "util/getConfigValue";
import Loading from "components/common/Loading";
import { IS_PRODUCTION } from '../../constants';
import { toLower } from 'lodash';

interface HeaderProps {
    className?: string;
}

interface EnvironmentBannerProps {
    environment: string;
}

const EnvironmentBanner: React.FC<EnvironmentBannerProps> = ({environment}) => {
    return (
        <div className="environment_banner">
            {environment} SERVER - DO NOT USE FOR PRODUCTION
        </div>
    ) 
}

const Header: React.FC<HeaderProps> = ({className}) => {
    const history = useHistory();
    const { authenticated, getUsername, userData } = useAuth();

    const [username, setUsername] = useState(getUsername());
    const [showMimicModal, setShowMimicModal] = useState(false);

    useEffect(() => {
        setUsername(getUsername());
    }, [userData]);

    const handleCloseMimicModal = () => setShowMimicModal(false);
    const handleShowMimicModal = () => setShowMimicModal(true);

    const logout = () => {
        history.push('/logout')
    }

    const getEnvironmentBanner = (environment: string | undefined) => {
        if (toLower(environment) === 'production') {
            return null;
        }
        let environmentDisplayName = ""
        environment = toLower(environment)

        switch (environment) {
            case "development":
                environmentDisplayName = "DEVELOPMENT";
                break;
            case "testing":
                environmentDisplayName = "TESTING";
                break;
            case "staging":
                environmentDisplayName = "STAGING";
                break;
        }

        return (
            <EnvironmentBanner environment={environmentDisplayName} />
        )
    }

    return (
        <div className={className}>
            <Navbar className="p-0">
                <Link to="/" className="navbar-brand">
                    <img height={52} src={Logo} alt="Ambry Genetics"/>
                </Link>
                {getEnvironmentBanner(getConfigValue("APP_ENV"))}
                {authenticated && (
                    <div className="ml-auto">
                        {username ? (
                            <span className="mr-2">Logged in as: <strong>{username}</strong></span>
                        ) : (
                            <Loading />
                        )}
                        {' '}
                        {toLower(getConfigValue('APP_ENV')) === 'production' ? null : <Button color="dark" size="sm" className="border" onClick={handleShowMimicModal}>Mimic</Button>}
                        <Button color="light" size="sm" className="border" onClick={logout}>Logout</Button>
                    </div>
                )}
            </Navbar>
            {toLower(getConfigValue('APP_ENV')) === 'production' ? null : <Mimic show={showMimicModal} handleClose={handleCloseMimicModal} />}
        </div>
    );
};

export default Header;
