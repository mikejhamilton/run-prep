import './navigation.scss';
import React from 'react';
import { NavLink as BaseNavLink } from 'react-router-dom';
import Footer from './Footer';
import PermissionToolTip from "components/common/PermissionToolTip";
import { PROBE_DESIGN_GROUPS } from "config/userPermissions";
import useAuth from 'auth/useAuth';
import hasAccess from "util/hasAccess";
import { OverlayTrigger } from "react-bootstrap";

const NavLink: React.FC<any> = (props) => {
    return <BaseNavLink className="navigation__link" activeClassName="navigation__link--active" {...props} />
};

const DisabledNavLink: React.FC<any> = (props) => (
    <OverlayTrigger
        placement="right"
        delay={{ show: 250, hide: 400 }}
        overlay={PermissionToolTip}
    >
        <span className="navigation__link navigation__link--disabled">{props.children}</span>
    </OverlayTrigger>
)

const Sidebar: React.FC<{ className?: string }> = ({ className }) => {
    const {getUserGroups} = useAuth();
    const userGroups = getUserGroups();
    const hasProbeDesignAccess = hasAccess(userGroups, PROBE_DESIGN_GROUPS);
    return (
        <div className={className}>
            <div className="navigation">
                <div className="navigation__top">
                    <h2>AMP</h2>
                </div>

                <h2 className="navigation__header">Sanger Amplicon Version</h2>
                <NavLink to="/amplicon/search">Search / Update</NavLink>
                <NavLink to="/amplicon/create">Create</NavLink>

                <h2 className="navigation__header">Probe Design</h2>
                {hasProbeDesignAccess ? (
                    <NavLink to="/probe/upload">Upload</NavLink>
                ) : (
                    <DisabledNavLink>Upload</DisabledNavLink>
                )}
                <NavLink to="/probe/status">Status</NavLink>

                <h2 className="navigation__header">RT-PCR Amplicon Version</h2>
                <NavLink to="/rtpcr-amplicon/search">Search / Update</NavLink>

                <h2 className="navigation__header">Event Log</h2>
                <NavLink to="/events">Events</NavLink>

            </div>
            <Footer className="layout__footer" />
        </div>
    );
};

export default Sidebar;
