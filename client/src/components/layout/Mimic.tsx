import React, { useEffect, useState } from "react";
import { Modal, ListGroup, Button, FormCheck } from "react-bootstrap";
import { MIMICABLE_GROUPS } from "../../config/userPermissions";
import { useAuth } from "auth";

interface MimicProps {
    show: Boolean;
    handleClose: any;
}

interface permissionGroupProp {
    group: string;
    userGroups: string[];
}

const PermissionGroupItem: React.FC<permissionGroupProp> = ({group, userGroups}) => {
    const checked: boolean = userGroups.includes(group);
    const {userData, setUserData} = useAuth();

    const handleClick = () => {
        // Add to, or remove from state, depending on checked status
        const newGroups = checked ? userGroups.filter(g => g !== group) : [...userGroups, group];
        if (userData) {
            setUserData({...userData, groups: newGroups});
        }
    };
    return (
        <ListGroup.Item>
            {group}
            <FormCheck
                checked={checked}
                inline
                style={{float: "right"}}
                onChange={() => {
                }}
                onClick={handleClick}
            />
        </ListGroup.Item>
    );
};

const Mimic: React.FC<MimicProps> = ({show, handleClose}) => {
    const [backendOverride, setBackendOverride] = useState(localStorage.getItem('backend-auth-override') === 'True');
    const [userGroups, setUserGroups] = useState<string[]>([]);
    const {getUserGroups, userData} = useAuth();

    useEffect(() => {
        setUserGroups(getUserGroups());
    }, [userData]);

    const handleBackendOverrideClick = () => {
        if (!backendOverride) {
            setBackendOverride(true);
            localStorage.setItem("backend-auth-override", "True");
        } else {
            setBackendOverride(false);
            localStorage.setItem("backend-auth-override", "False");
        }
    };

    return (
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Alter Permissions</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <ListGroup>
                    {MIMICABLE_GROUPS.map((group: string, idx: number) => (
                        <PermissionGroupItem key={idx} group={group} userGroups={userGroups}/>
                    ))}
                    <ListGroup.Item>
                        Override backend auth
                        <FormCheck
                            checked={backendOverride}
                            inline
                            style={{float: "right"}}
                            onChange={() => {
                            }}
                            onClick={handleBackendOverrideClick}
                        />
                    </ListGroup.Item>
                </ListGroup>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={handleClose}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default Mimic;
