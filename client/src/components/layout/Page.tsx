import './page.scss';
import React from 'react';
import Layout from './Layout';

interface PageInterface extends React.FC {
    Title: React.FC,
    Body: React.FC,
    Break: React.FC,
    Spinner: React.FC<{ show: boolean }>
}

const Page: PageInterface = ({children}) => {
    return (
        <Layout.Default>
            <div className="page">{children}</div>
        </Layout.Default>
    );
};

Page.defaultProps = {
    layout: 'default',
};

Page.Break = ({children}) => {
    return (
        <div className="page__break"><h1>{children}</h1></div>
    );
} 

Page.Title = ({children}) => {
    return (
        <div className="page__title"><h1>{children}</h1></div>
    );
};

Page.Body = ({children}) => {
    return (
        <div className="page__body">{children}</div>
    );
};

Page.Spinner = ({show}) => {
    return show ? (
        <div className="page__spinner">
            <div className="global-spinner__spinner spinner-border text-primary"/>
        </div>
    ) : null;
};

export default Page;
