import React from 'react';

const Footer: React.FC<{className?: string}> = ({className}) => {
    return (
        <div className={className}>
            &copy; Ambry Genetics {(new Date()).getFullYear()}
        </div>
    );
};

export default Footer;
