import './layout.scss';
import React from 'react';
import Sidebar from './Sidebar';
import Header from './Header';
import Footer from './Footer';
import GlobalSpinner from './GlobalSpinner';

const ErrorBoundary: React.FC<any> = ({children}) => children;
const EnvironmentBanner: React.FC<any> = () => null;

const Default: React.FC = ({children}) => {
    return (
        <div className="layout">
            <Header className="layout__header"/>
            <EnvironmentBanner className="layout__env"/>
            <div className="layout__body">
                <Sidebar className="layout__sidebar"/>
                <div className="layout__content">
                    <ErrorBoundary>
                        {children}
                    </ErrorBoundary>
                </div>
            </div>
            <GlobalSpinner/>
        </div>
    );
};

const Login: React.FC = ({children}) => {
    return (
        <div className="layout">
            <Header className="layout__header"/>
            <EnvironmentBanner className="layout__env"/>
            <div className="layout__body">
                <ErrorBoundary>
                    {children}
                </ErrorBoundary>
            </div>
            <Footer className="layout__footer"/>
            <GlobalSpinner/>
        </div>
    );
};

export default {
    Default,
    Login,
};
