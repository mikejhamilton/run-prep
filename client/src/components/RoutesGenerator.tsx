import React, { memo, Suspense } from 'react';
import { uid } from 'react-uid';
import { RouteProps, Switch, Route } from 'react-router';
import { SecureRoute } from '../auth';

type MetaObject = { title?: string };

export interface RouteDefinition extends Require<RouteProps, 'component'> {
  meta?: MetaObject;
  name?: string;
  authenticated?: boolean;
  exact?: boolean;
}

interface RoutesGeneratorProps {
  loader?: React.ReactNode;
  routes: RouteDefinition[];
}

const RoutesGenerator: React.FC<RoutesGeneratorProps> = ({
  loader = null,
  routes
}) => (
  <Suspense fallback={loader}>
    <Switch>
      {routes.map(({ authenticated = true, exact = true, ...props }) => {
        const RouteComponent = !!authenticated ? SecureRoute : Route;

        return <RouteComponent {...props} key={uid(props)} exact={exact} />;
      })}
    </Switch>
  </Suspense>
);

export default memo(RoutesGenerator);
