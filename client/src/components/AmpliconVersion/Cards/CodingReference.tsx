import { ListGroupCard, StyledListGroupItem } from "../../common/cards/ListGroup";
import { ReduxInputFormGroup, renderSwitchInput } from "../../form/redux-form-group";
import React from "react";
import { ListGroupItem } from "react-bootstrap";
import { change, Field, getFormValues } from "redux-form";
import styles from "./CardStyles.module.scss";
import { isInteger } from "../../form/validators";
import { useDispatch, useSelector } from "react-redux";
import { getCRange } from "../../../util/AmpliconVersion/AmpliconVersionUtils";

type ThermoProps = {
    readonly?: boolean,
    row?: any,
}

type SearchRow = Collection<Collection<SerializablePayloadFields> & SerializablePayloadFields>;

interface SearchRowDetailProps {
    row: SearchRow; 
}

/**
 * Validates g.start/end values, allows for null values if isAluBP flag is checked
 */
const validateGenomicValues = (value: string, allValues: any) => {
    const required = !allValues?.data?.is_alubp
    if (!required && !value) {
        return null
    } else {
        return isInteger(value)
    }
}

/**
 * Validates fragment length is either an integer or a list of positive integers delineated by ';'
 */
const validateFragmentLength = (value: string) => {
    return String(value).match(/^([0-9]+(;\s*[0-9]+)*)$/) ? null : "Required: Must be a zero or positive integer (or multiple integers separated by ';')";
}

const fieldStyles = {
    controlClassName: styles.controlStyle,
    errorClassName: styles.ErrorStyle
};


const ReadOnlyFields: React.FC<SearchRowDetailProps> = (props: SearchRowDetailProps) => (
    <React.Fragment>
        <StyledListGroupItem label="c. Start" value={props.row.c_dot_start} />
        <StyledListGroupItem label="c. End" value={props.row.c_dot_end} />
        <StyledListGroupItem label="c. Start Extension" value={props.row.c_start_extension} />
        <StyledListGroupItem label="c. End Extension" value={props.row.c_end_extension} />
        <StyledListGroupItem label="g. Start" value={props.row.target?.start_position ?? ""} />
        <StyledListGroupItem label="g. End" value={props.row.target?.end_position ?? ""} />
        <StyledListGroupItem label="Fragment Length" value={props.row.fragment_length} />
    </React.Fragment>
)

const FormFields: React.FC = () => {
    const dispatch = useDispatch();
    const formValues: any = useSelector((state) => {
        return getFormValues("amplicon")(state);
    });
    const handleRangeChange = () => {
        // Calculate c_range for IVS amplicons after user changes values
        if (!!formValues?.data?.c_dot_start && !!formValues?.data?.c_dot_end) {
            dispatch(
                change(
                    "amplicon",
                    "topbar.c_range",
                    getCRange(formValues.data)
                )
            );
        }
    }
    return (
        <React.Fragment>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="c. Start"
                    name="c_dot_start"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={isInteger}
                    onBlur={handleRangeChange}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="c. End"
                    name="c_dot_end"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={isInteger}
                    onBlur={handleRangeChange}
                    {...fieldStyles}
                />
            </ListGroupItem>

            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="c. Start Ext"
                    name="c_start_extension"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={isInteger}
                    onBlur={handleRangeChange}
                    {...fieldStyles}
                />
            </ListGroupItem>

            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="c. End Ext"
                    name="c_end_extension"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={isInteger}
                    onBlur={handleRangeChange}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="g. Start"
                    name="g_start"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={validateGenomicValues}
                    tipText="Generated from PCR Coordinates"
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="g. End"
                    name="g_end"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={validateGenomicValues}
                    tipText="Generated from PCR Coordinates"
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="Fragment Length"
                    name="fragment_length"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={validateFragmentLength}
                    tipText="Generated from PCR Coordinates"
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
            <div style={{display: "flex", justifyContent: "space-between"}}>
                <label className="form-label">Is AluBP?</label>
                <Field
                    style={{paddingTop: "50px"}}
                    name="is_alubp"
                    id="aluBPSwitch"
                    extraHandler={(checked: boolean) => {
                        if (!checked) {
                            dispatch(change("amplicon", "primerDesign.forward_pcr_alubp", false));
                            dispatch(change("amplicon", "primerDesign.reverse_pcr_alubp", false));
                        }
                    }}
                    component={renderSwitchInput}
                />
            </div>
            </ListGroupItem>
        </React.Fragment>
    )
}

export const CodingReferenceCard: React.FC<ThermoProps> = ({readonly, row}: ThermoProps) => (
    <ListGroupCard title="Coding Reference Sequence">
        {readonly? <ReadOnlyFields row={row} /> : <FormFields />}
    </ListGroupCard>
)