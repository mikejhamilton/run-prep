import { ListGroupCard, StyledListGroupItem } from "../../common/cards/ListGroup";
import { ReduxInputFormGroup, ReduxSelectFormGroup, renderSwitchInput } from "../../form/redux-form-group";
import React from "react";
import { ListGroupItem } from "react-bootstrap";
import { Field, change } from "redux-form";
import styles from "./CardStyles.module.scss";
import { useDispatch } from "react-redux";

const MODULE_MAX_LENGTH = 50;

const validateModuleContent = (value: any): string | null => {
    if (!value || !/^[a-zA-Z0-9 -:]+$/.test(value)) {
        return "Invalid Module"
    } else if (value.length > MODULE_MAX_LENGTH) {
        return "Invalid length"
    } else {
        return null
    }
};

type InfoProps = {
    readonly?: boolean;
    row?: any;
};

type SearchRow = Collection<Collection<SerializablePayloadFields> | SerializablePayloadFields>;

interface SearchRowDetailProps {
    row: SearchRow;
}

const fieldStyles = {
    controlClassName: styles.controlStyle,
    errorClassName: styles.ErrorStyle
};


const ReadOnlyFields: React.FC<SearchRowDetailProps> = (props: SearchRowDetailProps) => (
    <React.Fragment>
        <StyledListGroupItem label="Amp Module" value={props.row.amp_module} />
        <StyledListGroupItem label="Seq Module" value={props.row.seq_module} />
        <StyledListGroupItem label="Direction" value={props.row.sequencing_direction} />
        <StyledListGroupItem label="Validation Status" value={props.row.validation_status} />
        <StyledListGroupItem label="Priority" value={props.row.priority} />
        <StyledListGroupItem label="Pseudogene" value={props.row.pseudogene? "True" : "False"} />
    </React.Fragment>
);

const FormFields: React.FC = () => {
    const dispatch = useDispatch();
    return (
        <React.Fragment>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="AMP Module"
                    name="amp_module"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={validateModuleContent}
                    controlName={styles.controlStyle}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="SEQ Module"
                    name="seq_module"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={validateModuleContent}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="Direction"
                    name="sequencing_direction"
                    component={ReduxSelectFormGroup}
                    type="select"
                    options={["Both", "Forward", "Reverse"]}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="Sheet"
                    name="sheet"
                    component={ReduxSelectFormGroup}
                    type="select"
                    options={["Sanger", "RDS", "LONGRANGE"]}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="Validation"
                    name="validation_status"
                    component={ReduxSelectFormGroup}
                    type="select"
                    options={[
                        "Validated",
                        "Pending Approval",
                        "Approved CUSTOM",
                        "Approved SANGER",
                        "Approved NGS",
                        "Retired",
                        "Failed",
                    ]}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="Priority"
                    name="priority"
                    component={ReduxSelectFormGroup}
                    type="select"
                    options={["Backup", "Primary"]}
                    {...fieldStyles}
                />

            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <div style={{display: "flex", justifyContent: "space-between"}}>
                    <label className="form-label">Pseudogene</label>
                    <Field
                        style={{paddingTop: "50px"}}
                        name="pseudogene"
                        id="pseudogeneSwitch"
                        extraHandler={(checked: boolean) => {
                            if (checked) {
                                dispatch(change("amplicon", "topbar.isoform_version", ""));
                            }
                        }}
                        component={renderSwitchInput}
                    />
                </div>
                </ListGroupItem>
        </React.Fragment>
    );
}

export const AdditionalSangerInformationCard: React.FC<InfoProps> = ({
    readonly,
    row,
}: InfoProps) => (
    <ListGroupCard title="Additional Information">
        {readonly ? <ReadOnlyFields row={row} /> : <FormFields />}
    </ListGroupCard>
);
