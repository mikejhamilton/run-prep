import React from "react";
import { RtpcrAmplicon } from "../../../../types";
import { ListGroupCard, StyledListGroupItem } from "../../common/cards/ListGroup";

type CodingProps = {
    amplicon: RtpcrAmplicon,
}

export const RtpcrCodingReferenceCard: React.FC<CodingProps> = ({amplicon}: CodingProps) => (
    <ListGroupCard title="Coding Reference Sequence">
        <StyledListGroupItem label="g. Start" value={amplicon.targets[0]?.start_position ?? ""} />
        <StyledListGroupItem label="g. End" value={amplicon.targets[0]?.end_position ?? ""} />
        <StyledListGroupItem label="Fragment Length" value={amplicon.fragment_length} />
    </ListGroupCard>
)