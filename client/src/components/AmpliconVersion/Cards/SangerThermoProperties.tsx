import { ListGroupCard, StyledListGroupItem } from "../../common/cards/ListGroup";
import { ReduxInputFormGroup, ReduxSelectFormGroup } from "../../form/redux-form-group";
import React, { useEffect, useState } from "react";
import { ListGroupItem } from "react-bootstrap";
import { change, Field, getFormInitialValues, touch } from "redux-form";
import styles from "./CardStyles.module.scss";
import { isNumber, required } from "../../form/validators";
import { useDispatch, useSelector } from "react-redux";
import nullTagPCRMethods from "../../../constants/nullBDTTagPCRMethods";
import thermocyclerPrograms from "../../../constants/thermocyclerPrograms";


type ThermoProps = {
    readonly?: boolean;
    row?: any;
};

type SearchRow = Collection<Collection<SerializablePayloadFields> | SerializablePayloadFields>;

type PcrMethod = keyof typeof thermocyclerPrograms;

interface SearchRowDetailProps {
    row: SearchRow;
}

const fieldStyles = {
    controlClassName: styles.controlStyle,
    errorClassName: styles.ErrorStyle,
};

const ReadOnlyFields: React.FC<SearchRowDetailProps> = (props: SearchRowDetailProps) => (
    <React.Fragment>
        <StyledListGroupItem label="PCR Method" value={props.row.pcr_method} />
        <StyledListGroupItem label="Thermocycler Program" value={props.row.thermo_program} />
        <StyledListGroupItem label="GC Content" value={props.row.gc_content} />
        <StyledListGroupItem label="Betaine Volume" value={String(props.row.betaine) + " uL"} />
        <StyledListGroupItem label="DMSO Volume" value={String(props.row.dmso) + " uL"} />
        <StyledListGroupItem label="Qsol Volume" value={String(props.row.qsol) + " uL"} />
    </React.Fragment>
);

const mapPcrMethodToThermocyclerProgram = (value: PcrMethod | undefined): string[] => {
    if (value === undefined) {
        return [];
    }
    return thermocyclerPrograms[value] ?? [];
}

const FormFields: React.FC = () => {
    const dispatch = useDispatch();
    const [pcrState, setPcrState] = useState<PcrMethod | undefined>();
    const formValues = useSelector(getFormInitialValues("amplicon"));
    let pcrMethod = '';
    if ('data' in formValues) {
        pcrMethod = formValues['data']['pcr_method'];
    }

    useEffect(() => {
        if (pcrMethod in thermocyclerPrograms) {
            setPcrState((pcrMethod as keyof typeof thermocyclerPrograms));
        }
    }, [])

    const handlePCRMethodChange = (_: any, value: PcrMethod, c: any, d: any) => {
        if (nullTagPCRMethods.includes(value)) {
            dispatch(change("amplicon", "primerDesign.forward_seq_name", "NA"));
            dispatch(change("amplicon", "primerDesign.forward_seq_seq", "NA"));
            dispatch(change("amplicon", "primerDesign.forward_seq_coord", "NA"));
            dispatch(change("amplicon", "primerDesign.reverse_seq_name", "NA"));
            dispatch(change("amplicon", "primerDesign.reverse_seq_seq", "NA"));
            dispatch(change("amplicon", "primerDesign.reverse_seq_coord", "NA"));
        }

        // Update the thermocycler program (based on the new PCR method) in Redux
        dispatch(change("amplicon", "data.thermo_program", mapPcrMethodToThermocyclerProgram(value)[0]));

        setPcrState(value);
    };

    return (
        <React.Fragment>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="PCR Method"
                    name="pcr_method"
                    component={ReduxSelectFormGroup}
                    type="select"
                    options={Object.keys(thermocyclerPrograms)}
                    onChange={handlePCRMethodChange}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="Thermocycler Program"
                    name="thermo_program"
                    component={ReduxSelectFormGroup}
                    type="select"
                    {...fieldStyles}
                    options={mapPcrMethodToThermocyclerProgram(pcrState)}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="GC Content"
                    name="gc_content"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={isNumber}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="Betaine"
                    name="betaine"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={[required, isNumber]}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="DMSO"
                    name="dmso"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={[required, isNumber]}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="QSOL"
                    name="qsol"
                    component={ReduxInputFormGroup}
                    type="text"
                    validate={[required, isNumber]}
                    {...fieldStyles}
                />
            </ListGroupItem>
        </React.Fragment>
    );
};

export const SangerThermodynamicPropertiesCard: React.FC<ThermoProps> = ({
    readonly,
    row,
}: ThermoProps) => (
    <ListGroupCard title="Thermodynamic Properties">
        {readonly ? <ReadOnlyFields row={row} /> : <FormFields />}
    </ListGroupCard>
);
