import React from "react";

import { RtpcrAmplicon } from "../../../../types";
import { ListGroupCard, StyledListGroupItem } from "../../common/cards/ListGroup";

type ThermoProps = {
    amplicon: RtpcrAmplicon;
};

export const RtpcrThermodynamicPropertiesCard: React.FC<ThermoProps> = ({amplicon}: ThermoProps) => (
    <ListGroupCard title="Thermodynamic Properties">
        <StyledListGroupItem label="Betaine Volume" value={String(amplicon.betaine) + " \u03bcL"} />
    </ListGroupCard>
);
