import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { submit } from "redux-form";
import { useDispatch } from "react-redux";

import { RtpcrAmplicon } from "../../../../types";
import { useAuth } from "../../../auth";
import { CREATE_AMPLICON_GROUPS } from "../../../config/userPermissions";
import { appendRtpcrAmplicons } from "../../../redux/slices/rtpcr-amplicons";
import { alertError, alertSuccess } from "../../../services/alert";
import api from "../../../services/api";
import hasAccess from "../../../util/hasAccess";
import { ListGroupCardHeaderButton, StyledListGroupItem } from "../../common/cards/ListGroup";
import Loading from "../../common/Loading";
import RtpcrAdditionalDataForm from "../AmpliconSearchRowForm/RtpcrAdditionalDataForm";
import styles from "./CardStyles.module.scss";

type InfoProps = {
    readonly: boolean;
    amplicon: RtpcrAmplicon;
};

const ReadOnlyFields: React.FC<RtpcrAmplicon> = ({
        control_pool,
        multiplex_rtpcr_amplicon_version,
        priority,
        validation_status
}) => (
    <React.Fragment>
        <StyledListGroupItem label="Validation Status" value={validation_status} />
        <StyledListGroupItem label="Priority" value={priority} />
        <StyledListGroupItem label="Control Pool" value={control_pool} />
        <StyledListGroupItem
            label="Multiplex"
            value={multiplex_rtpcr_amplicon_version?.pcr_primer_pair_name ?? false}
        />
    </React.Fragment>
);

export const AdditionalRtpcrInformationCard: React.FC<InfoProps> = ({
    readonly,
    amplicon,
}) => {
    const {getUserGroups} = useAuth();
    const userGroups = getUserGroups();
    const createAccess = hasAccess(userGroups, CREATE_AMPLICON_GROUPS);
    const dispatch = useDispatch();
    const formName = `rtpcr_amplicon_${amplicon.id}`;

    const [ampliconState, setAmpliconState] = useState<RtpcrAmplicon>(amplicon);
    const [editState, setEditState] = useState<boolean>(!readonly);
    const [initialFormValues, setInitialFormValues] = useState<{ data: RtpcrAmplicon }>({data: amplicon});
    const [saveLoading, setSaveLoading] = useState<boolean>(false);

    // Redux-form doesn't pick up changes to its data after submitting the form the first time,
    // so an explicit useEffect() call is required to re-render the component with new data.
    useEffect(() => {
        setInitialFormValues({data: ampliconState})
    }, [ampliconState]);

    const editButton = (
        !createAccess ? undefined : (
            <Button
                variant="outline-secondary"
                size="sm"
                onClick={() => setEditState(!editState)}
                className={styles.headerButtonStyle}
            >
                <i className="fa fa-pencil-square-o"/>
            </Button>
        )
    );

    const handleUpdate = () => {
        if (!createAccess) {
            alertError("Insufficient Privileges. Need Group: " + CREATE_AMPLICON_GROUPS);
            return;
        }
        dispatch(submit(formName));
        setSaveLoading(true);
    }

    const handleSubmit = (values: any) => {
        api.put("/api/v2/rtpcr-amplicon-version", {rtpcrAmpliconVersionDesign: {...values.data}})
            .then(({data: {api_data: {updated_at: updatedAt}}}) => {
                alertSuccess("Amplicon Modified");
                setEditState(false);
                const ampliconVersion = {...values.data, updated_at: updatedAt};
                setAmpliconState(ampliconVersion);
                // Send new items data, leave page null so pagination information is left alone.
                dispatch(appendRtpcrAmplicons({page: null, items: [ampliconVersion]}));
            })
            .catch((e) => {
                alertError("Error: " + e);
                console.error(e);
            })
            .finally(() => {
                setSaveLoading(false);
            });
    }

    return (
        <ListGroupCardHeaderButton title="Additional Information" titleButton={editButton}>
            {
                editState ?
                    <>
                        <RtpcrAdditionalDataForm
                            initialValues={initialFormValues}
                            form={formName}
                            onSubmit={handleSubmit}
                        />
                        <Button disabled={saveLoading} variant="outline-primary" type="submit" onClick={handleUpdate}>
                            {saveLoading ? (
                                <Loading />
                            ) : (
                                "Update"
                            )}
                        </Button>
                    </>
                    :
                    <ReadOnlyFields {...ampliconState}/>
            }
        </ListGroupCardHeaderButton>
    )
};
