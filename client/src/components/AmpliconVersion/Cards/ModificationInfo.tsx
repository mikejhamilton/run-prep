import React, { useEffect, useState } from "react";
import { AmpliconVersion } from "../../../../types";
import { ListGroupCard, StyledListGroupItem } from "../../common/cards/ListGroup";
import UTCToLocaleDate from "../../../util/UTCToLocaleDate";

interface ModificationInfoProps {
    ampliconVersion: AmpliconVersion;
}

const ModificationInfo: React.FC<ModificationInfoProps> = ({ampliconVersion}: ModificationInfoProps) => {
    const [ampliconState, setAmpliconState] = useState<AmpliconVersion>(ampliconVersion);

    useEffect(() => {
        setAmpliconState(ampliconVersion);
    }, [ampliconVersion]);

    return (
        <ListGroupCard title="Dates">
            <StyledListGroupItem
                label="Created"
                value={
                    `${ampliconState.created_by?.username ?? 'Imported'}, at ${UTCToLocaleDate(ampliconState.created_at)}`
                }/>
            <StyledListGroupItem
                label="Updated"
                value={
                    `${ampliconState.updated_by?.username ?? 'Imported'}, at ${UTCToLocaleDate(ampliconState.updated_at)}`
                }/>
        </ListGroupCard>
    )
};

export default ModificationInfo;