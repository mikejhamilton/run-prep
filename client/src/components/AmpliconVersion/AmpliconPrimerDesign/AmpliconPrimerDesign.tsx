import React from "react";
import styles from "./AmpliconPrimerDesign.module.scss";
import { getFormValues, getFormMeta, change } from "redux-form";
import { Field } from "redux-form";
import { useSelector, useDispatch } from "react-redux";
import { ReduxInputFormGroup, renderSwitchInput } from "../../form/redux-form-group";
import { Row, Col, Container, Button, Tooltip, OverlayTrigger } from "react-bootstrap";
import { required } from "../../form/validators";
import { BDT_COORD_REGEX, COORD_REGEX, SEQUENCE_REGEX, DEFAULT_BDT_NAME_REGEX } from "../../../constants/regexes";
import parseCoordinates from "../../../util/parseCoordinates";
import { isEmpty, valuesIn } from "lodash";
import CommentComponent from "../../Comment/CommentComponent";
import nullTagPCRMethods from "../../../constants/nullBDTTagPCRMethods";

const COORD_RANGE = 300000000;

// Primer Specific Validators
const isInvalidPrimerSequence = (value: string): string | null => {
    return !value || !SEQUENCE_REGEX.test(value) ? "Invalid Primer Sequence" : null;
};
const isInvalidPCRName = (value: string): string | null => {
    return !value || value=="NA" ? "Invalid Primer Name" : null;
};
const isInvalidBDTName = (value: string, allValues: any): string | null => {
    if (isEmpty(allValues)) {
        return null;
    }
    // BDT name gets autoset if specific PCR method is chosen
    if (nullTagPCRMethods.includes(allValues["data"]["pcr_method"])) {
        return null;
    }
    return !value || value=="NA" ? "Invalid Primer Name" : null;
};
const isInvalidSEQPrimerSequence = (value: string, allValues: any): string | null => {
    if (isEmpty(allValues)) {
        return null;
    }
    if (nullTagPCRMethods.includes(allValues["data"]["pcr_method"])) {
        return null;
    }
    return !value || !SEQUENCE_REGEX.test(value) ? "Invalid Primer Sequence" : null;
};
const isCoordinateInvalid = (coord: string): string | null => {
    return !coord || !COORD_REGEX.test(coord) ? "Invalid Coordinate" : null;
};
const isBdtCoordinateInvalid = (coord: string): string | null => {
    return !coord || !BDT_COORD_REGEX.test(coord) ? "Invalid Coordinate" : null;
};
const isInvalidPCRCoord = (coord: string, allValues: any, props: any, name: string): string | null => {
    if (isEmpty(allValues)) return null;
    if (coord == null) return "Invalid Coordinate";
    const direction = name.includes("forward") ? "forward" : "reverse";
    const directionAluSwitch = allValues["primerDesign"][`${direction}_pcr_alubp`];
    const mainAluSwitch = allValues["data"][`is_alubp`];

    if (mainAluSwitch && directionAluSwitch) {
        return null;
    } else return isCoordinateInvalid(coord);
};
const isInvalidSeqCoord = (coord: string, allValues: any, props: any, name: string): string | null => {
    if (isEmpty(allValues) || coord == null) {
        return null;
    }
    if (nullTagPCRMethods.includes(allValues["data"]["pcr_method"])) {
        return null;
    }
    const direction = name.includes("forward") ? "forward" : "reverse";
    const seqNameValue = allValues["primerDesign"][`${direction}_seq_name`];
    // If seq name == s-tag or as-tag, restrict coords to NA only
    if (DEFAULT_BDT_NAME_REGEX.test(seqNameValue)) {
        return coord !== "NA" ? "Invalid Coord for default Primer" : null;
    } else {
        return isBdtCoordinateInvalid(coord) ? "Invalid Coordinate" : null;
    }
};
const isCoordOutOfRange = (coord: string): string | null => {
    if (coord == undefined) {
        return null;
    }
    // If sequence primer, allow 'NA'
    if (coord === "NA") return null;
    const match = coord.match(COORD_REGEX);
    if (!match) return "Coordinate out of range";
    const start = Number(match!.groups!.start);
    const end = Number(match!.groups!.end);
    return start < 0 || start > COORD_RANGE || end < 0 || end > COORD_RANGE ? "Coordinate out of range" : null;
};

/**
 * Primer design form for creating and modifying PCR/SEQ Primer data
 */
const AmpliconPrimerDesign: React.FC = () => {
    const dispatch = useDispatch();

    const resetForwardSeq = () => {
        dispatch(change("amplicon", "primerDesign.forward_seq_name", "s-tag"));
        dispatch(change("amplicon", "primerDesign.forward_seq_seq", "TCTGCCTTTTTCTTCCATCGGG"));
        dispatch(change("amplicon", "primerDesign.forward_seq_coord", "NA"));
    };
    const resetReverseSeq = () => {
        dispatch(change("amplicon", "primerDesign.reverse_seq_name", "as-tag"));
        dispatch(change("amplicon", "primerDesign.reverse_seq_seq", "TCCCCAACCCCCTAAAGCGA"));
        dispatch(change("amplicon", "primerDesign.reverse_seq_coord", "NA"));
    };

    const formValues: any = useSelector((state) => {
        return getFormValues("amplicon")(state);
    });
    const formMeta: any = useSelector((state) => {
        return getFormMeta("amplicon")(state);
    });

    const handleAluPrimerChange = (name: string, checked: boolean) => {
        if (checked) {
            if (name == "forward_pcr_alubp") {
                dispatch(change("amplicon", "primerDesign.forward_pcr_coord", "NA"));
                if (formValues.primerDesign?.reverse_pcr_alubp == true) {
                    dispatch(change("amplicon", "primerDesign.reverse_pcr_alubp", false));
                }
            } else if (name == "reverse_pcr_alubp") {
                dispatch(change("amplicon", "primerDesign.reverse_pcr_coord", "NA"));
                if (formValues.primerDesign?.forward_pcr_alubp == true) {
                    dispatch(change("amplicon", "primerDesign.forward_pcr_alubp", false));
                }
            }
        }
    };

    /**
     * Updates the PCR primer pair name based on the given primer data names.
     * Calls to backend api to get version data
     */
    const handlePrimerNameChange = async () => {
        if (
            formValues?.topbar?.amplicon &&
            formValues?.primerDesign?.forward_pcr_name &&
            formValues?.primerDesign?.reverse_pcr_name &&
            !formMeta?.topbar?.pcr_primer_pair_name?.visited
        ) {
            const forwardDirection = formValues.primerDesign.forward_pcr_name.split(" ").pop();
            const reverseDirection = formValues.primerDesign.reverse_pcr_name.split(" ").pop();
            const combinedPairName = formValues.topbar.amplicon + " " + forwardDirection + "/" + reverseDirection;
            dispatch(change("amplicon", "topbar.pcr_primer_pair_name", combinedPairName));
        }
    };

    /**
     * Update g. Start/End and fragment length based on PCR primer coordinates
     */
    const handleCoordinateChange = () => {
        if (
            typeof formValues !== "undefined" &&
            formValues.primerDesign.forward_pcr_coord &&
            formValues.primerDesign.reverse_pcr_coord
        ) {
            // Prevent Calculating g.start/end if a primer is marked as AluBP
            if (formValues.primerDesign?.forward_pcr_alubp || formValues.primerDesign?.reverse_pcr_alubp) {
                return;
            }
            const forwardCoord = parseCoordinates(formValues.primerDesign.forward_pcr_coord);
            const reverseCoord = parseCoordinates(formValues.primerDesign.reverse_pcr_coord);
            if (!valuesIn(forwardCoord).includes(null) && !valuesIn(reverseCoord).includes(null)) {
                const forwardDiff = Math.abs(forwardCoord.startPosition! - reverseCoord.endPosition!);
                const reverseDiff = Math.abs(reverseCoord.startPosition! - forwardCoord.endPosition!);
                let fragmentLength = 0;
                let gStart = 0;
                let gEnd = 0;
                if (forwardDiff > reverseDiff) {
                    fragmentLength = forwardDiff + 1;
                    gStart = Math.min(reverseCoord.startPosition!, forwardCoord.endPosition!) + 1;
                    gEnd = Math.max(reverseCoord.startPosition!, forwardCoord.endPosition!) - 1;
                } else {
                    fragmentLength = reverseDiff + 1;
                    gStart = Math.min(forwardCoord.startPosition!, reverseCoord.endPosition!) + 1;
                    gEnd = Math.max(forwardCoord.startPosition!, reverseCoord.endPosition!) - 1;
                }
                dispatch(change("amplicon", "data.fragment_length", String(fragmentLength)));
                dispatch(change("amplicon", "data.g_start", gStart));
                dispatch(change("amplicon", "data.g_end", gEnd));
            }
        }
    };

    return (
        <Container fluid className={styles.primerDesign}>
            <Row>
                <Col>
                    <h4 className={[styles.primerTitle, styles.forwardPCR].join(" ")}>Forward PCR</h4>
                </Col>
                <Col xs={2}>
                    <Field
                        label="Name"
                        name="forward_pcr_name"
                        component={ReduxInputFormGroup}
                        onBlur={handlePrimerNameChange}
                        type="text"
                        validate={isInvalidPCRName}
                    />
                </Col>
                <Col xs={5}>
                    <Field
                        label="Sequence"
                        name="forward_pcr_seq"
                        component={ReduxInputFormGroup}
                        type="text"
                        onClear={() => dispatch(change("amplicon", "primerDesign.forward_pcr_seq", ""))}
                        validate={isInvalidPrimerSequence}
                    />
                </Col>
                <Col xs={2}>
                    <Field
                        label="Genomic Coordinates"
                        name="forward_pcr_coord"
                        component={ReduxInputFormGroup}
                        onBlur={handleCoordinateChange}
                        type="text"
                        onClear={() => dispatch(change("amplicon", "primerDesign.forward_pcr_coord", ""))}
                        clearDisabled={formValues.primerDesign?.forward_pcr_alubp}
                        disabled={formValues.data?.is_alubp && formValues.primerDesign?.forward_pcr_alubp}
                        validate={[isInvalidPCRCoord, isCoordOutOfRange]}
                    />
                </Col>
                <Col>
                    <Row>
                        <p style={{ marginBottom: "5px" }}>&nbsp;</p>
                    </Row>
                    {formValues.data?.is_alubp ? (
                        <div style={{ display: "flex" }}>
                            <Field
                                name="forward_pcr_alubp"
                                id="forward_pcr_alubp"
                                component={renderSwitchInput}
                                extraHandler={(checked: boolean) => handleAluPrimerChange("forward_pcr_alubp", checked)}
                            />
                            <label style={{ paddingLeft: "10px", paddingTop: "2px" }}>
                                AluBP
                                <OverlayTrigger
                                    placement="left"
                                    delay={{ show: 50, hide: 200 }}
                                    overlay={
                                        <Tooltip style={{ fontSize: "14px" }} id="t">
                                            Denotes which primer is AluBP
                                        </Tooltip>
                                    }
                                >
                                    <i style={{ padding: "3px" }} className="fa fa-info-circle" />
                                </OverlayTrigger>
                            </label>
                        </div>
                    ) : null}
                </Col>
            </Row>
            <Row>
                <Col>
                    <h4 className={styles.primerTitle}>Reverse PCR</h4>
                </Col>
                <Col xs={2}>
                    <Field
                        name="reverse_pcr_name"
                        component={ReduxInputFormGroup}
                        onBlur={handlePrimerNameChange}
                        type="text"
                        validate={isInvalidPCRName}
                    />
                </Col>
                <Col xs={5}>
                    <Field
                        name="reverse_pcr_seq"
                        component={ReduxInputFormGroup}
                        type="text"
                        onClear={() => dispatch(change("amplicon", "primerDesign.reverse_pcr_seq", ""))}
                        validate={isInvalidPrimerSequence}
                    />
                </Col>
                <Col xs={2}>
                    <Field
                        name="reverse_pcr_coord"
                        component={ReduxInputFormGroup}
                        onBlur={handleCoordinateChange}
                        type="text"
                        onClear={() => dispatch(change("amplicon", "primerDesign.reverse_pcr_coord", ""))}
                        clearDisabled={formValues.primerDesign?.reverse_pcr_alubp}
                        disabled={formValues.data?.is_alubp && formValues.primerDesign?.reverse_pcr_alubp}
                        validate={[isInvalidPCRCoord, isCoordOutOfRange]}
                    />
                </Col>
                <Col>
                    {formValues.data?.is_alubp ? (
                        <div style={{ display: "flex" }}>
                            <Field
                                name="reverse_pcr_alubp"
                                id="reverse_pcr_alubp"
                                component={renderSwitchInput}
                                extraHandler={(checked: boolean) => handleAluPrimerChange("reverse_pcr_alubp", checked)}
                            />
                            <label style={{ paddingLeft: "10px", paddingTop: "2px" }}>
                                AluBP
                                <OverlayTrigger
                                    placement="left"
                                    delay={{ show: 50, hide: 200 }}
                                    overlay={
                                        <Tooltip style={{ fontSize: "14px" }} id="t">
                                            Denotes which primer is AluBP
                                        </Tooltip>
                                    }
                                >
                                    <i style={{ padding: "3px" }} className="fa fa-info-circle" />
                                </OverlayTrigger>
                            </label>
                        </div>
                    ) : null}
                </Col>
            </Row>
            <Row>
                <Col>
                    <h4 className={styles.primerTitle}>Forward BDT</h4>
                </Col>
                <Col xs={2}>
                    <Field
                        name="forward_seq_name"
                        component={ReduxInputFormGroup}
                        type="text"
                        disabled={nullTagPCRMethods.includes(formValues.data.pcr_method)}
                        validate={isInvalidBDTName}
                    />
                </Col>
                <Col xs={5}>
                    <Field
                        name="forward_seq_seq"
                        component={ReduxInputFormGroup}
                        type="text"
                        disabled={nullTagPCRMethods.includes(formValues.data.pcr_method)}
                        validate={isInvalidSEQPrimerSequence}
                    />
                </Col>
                <Col xs={2}>
                    <Field
                        name="forward_seq_coord"
                        component={ReduxInputFormGroup}
                        type="text"
                        disabled={nullTagPCRMethods.includes(formValues.data.pcr_method)}
                        validate={[isInvalidSeqCoord, isCoordOutOfRange]}
                    />
                </Col>
                <Col>
                    <Button
                        disabled={nullTagPCRMethods.includes(formValues.data.pcr_method)}
                        size="sm"
                        onClick={resetForwardSeq}
                    >
                        s-tag
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    <h4 className={styles.primerTitle}>Reverse BDT</h4>
                </Col>
                <Col xs={2}>
                    <Field
                        name="reverse_seq_name"
                        component={ReduxInputFormGroup}
                        type="text"
                        disabled={nullTagPCRMethods.includes(formValues.data.pcr_method)}
                        validate={isInvalidBDTName}
                    />
                </Col>
                <Col xs={5} className={styles.centerI}>
                    <Field
                        name="reverse_seq_seq"
                        component={ReduxInputFormGroup}
                        type="text"
                        disabled={nullTagPCRMethods.includes(formValues.data.pcr_method)}
                        validate={isInvalidSEQPrimerSequence}
                    />
                </Col>
                <Col xs={2}>
                    <Field
                        name="reverse_seq_coord"
                        component={ReduxInputFormGroup}
                        type="text"
                        disabled={nullTagPCRMethods.includes(formValues.data.pcr_method)}
                        validate={[isInvalidSeqCoord, isCoordOutOfRange]}
                    />
                </Col>
                <Col>
                    <Button
                        disabled={nullTagPCRMethods.includes(formValues.data.pcr_method)}
                        size="sm"
                        onClick={resetReverseSeq}
                    >
                        as-tag
                    </Button>
                </Col>
            </Row>
            <CommentComponent />
        </Container>
    );
};

export default AmpliconPrimerDesign;
