import React, { useState } from "react";
import { Button, Col, Container, Form, FormControl, Row } from "react-bootstrap";
import styles from "./AmpliconSearchBar.module.scss";
import { ISearchFilter } from "../../../../types/amp";
import "font-awesome/css/font-awesome.min.css";
import { ampliconSearchFilters, ampliconSearchConditions } from "../../../constants/ampliconSearchFilters";

interface AmpliconSearchBarProps {
    filters: ISearchFilter[];
    setFilters: any;
    handleAddFilter: any;
    handleRemoveFilter: any;
    setExpandedRows: any;
}

const AmpliconSearchBar: React.FC<AmpliconSearchBarProps> = (props: AmpliconSearchBarProps) => {
    const [filterValue, setFilterValue] = useState("General");
    const [filterCondition, setFilterCondition] = useState("Contains");
    const [input, setInput] = useState("");

    const handleInputChange = (e: any) => {
        setInput(e.target.value);
    };

    const displaySearchFilters = () => {
        return props.filters.map((searchFilter: ISearchFilter) => {
            const { filter, condition, value } = searchFilter;
            return (
                <Col md="3" className={styles.searchFilters} key={`${filter}${condition}${value}`}>
                    <div className={styles.filterText}>{`${filter} ${condition} ${value}`}</div>
                    <button
                        onClick={() => props.handleRemoveFilter(searchFilter)}
                        className={styles.removeFilterButton}
                    >
                        <i className="fa fa-times-circle" aria-hidden="true" />
                    </button>
                </Col>
            );
        });
    };

    const handleEnterPress = (target: any) => {
        if (target.key === "Enter") {
            props.handleAddFilter({value: input, filter: filterValue, condition: filterCondition});
            setInput("");
        }
    };

    return (
        <Container fluid className={styles.searchBarContainer}>
            <Row>
                <Col md="2">
                    <Form.Control
                        as="select"
                        value={filterValue}
                        onChange={(e: any) => {
                            setFilterValue(e.target.value);
                        }}
                        custom
                        className={styles.selectControl}
                    >   
                        {ampliconSearchFilters.map((filter: string) => <option key={filter}>{filter}</option>)}
                    </Form.Control>
                </Col>
                <Col md="2">
                    <Form.Control
                        as="select"
                        value={filterCondition}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                            setFilterCondition(e.target.value);
                        }}
                        custom
                        className={styles.selectControl}
                    >
                        {ampliconSearchConditions.map((filter: string) => <option key={filter}>{filter}</option>)}
                    </Form.Control>
                </Col>
                <Col md="5">
                    <FormControl
                        type="text"
                        value={input}
                        onKeyPress={handleEnterPress}
                        onChange={handleInputChange}
                        placeholder="Add Filter"
                    />
                </Col>
                <Col className={styles.filterButtonColumn}>
                    <Button
                        className={styles.filterButton}
                        onClick={() => {
                            props.handleAddFilter({value: input, filter: filterValue, condition: filterCondition});
                            setInput("");
                        }}
                        variant="secondary"
                    >
                        Add Filter
                    </Button>
                    <Button
                        className={styles.filterButton}
                        onClick={() => {
                            setInput("");
                            props.setFilters([]);
                            props.setExpandedRows([]);
                        }}
                        variant="outline-secondary"
                    >
                        Clear Filters
                    </Button>
                </Col>
            </Row>
            <Row className={styles.searchFilterisplayRow}>{displaySearchFilters()}</Row>
        </Container>
    );
};

export default AmpliconSearchBar;
