import React from "react";
import { Modal, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { submit } from "redux-form";
import { isEmpty, map } from "lodash";

interface SaveModalProps {
    show?: boolean;
    primers: object[];
    overlappedPrimers: string[];
    errors: string[];
    handleClose: any;
}

const SaveModal: React.FC<SaveModalProps> = (props: SaveModalProps) => {

    const dispatch = useDispatch();

    const displayOverWrittenPrimers = () => {
        if (!isEmpty(props.primers)) {
            const primers = map(
                props.primers,
                (primer: { name: string; affectedAmplicons: any }) => {
                    return (
                        <div key={primer.name}>
                            <p>
                                Primer '{primer.name}' has been modified and will affect the
                                Amplicons: '{primer.affectedAmplicons.join("' '")}'
                            </p>
                        </div>
                    );
                }
            );
            return primers;
        } else {
            return null;
        }
    };
    const displayOverlappedPrimers = () => {
        if (!isEmpty(props.overlappedPrimers)) {
            const primers = map(
                props.overlappedPrimers,
                (primerString: string, index: number) => {
                    return (
                        <div key={index}>
                            <p>
                                {primerString}
                            </p>
                        </div>
                    );
                }
            );
            return primers;
        } else {
            return null;
        }
    };
    const displayOverWrittenPrimerErrors = () => {
        if (!isEmpty(props.errors)) {
            return (
                <div>
                    {map(props.errors, (name: string) => (
                        <p key={name}>{name}</p>
                    ))}
                    <p>An error occured while checking if these primers will be modified.</p>
                </div>
            );
        } else {
            return null;
        }
    };
    return (
        <Modal centered show={props.show} onHide={() => props.handleClose(true)}>
            <Modal.Header closeButton>
                <Modal.Title>Warning!</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {displayOverWrittenPrimers()}
                {displayOverWrittenPrimerErrors()}
                {displayOverlappedPrimers()}
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => props.handleClose(true)}>
                    Cancel
                </Button>
                <Button
                    variant="primary"
                    onClick={() => {
                        dispatch(submit("amplicon"));
                        props.handleClose();
                    }}
                >
                    Continue
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default React.memo(SaveModal);
