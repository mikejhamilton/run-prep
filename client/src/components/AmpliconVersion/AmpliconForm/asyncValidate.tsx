import api from "../../../services/api";

const asyncValidate = (values: { topbar: { gene: string } }): Promise<any> => {
    return api.get("/api/v2/gene/" + values.topbar.gene + "/").catch(() => {
        throw {
            topbar: {
                gene: "Gene not found",
            },
        };
    });
};

export default asyncValidate;
