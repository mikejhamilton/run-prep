import React from "react";
import { reduxForm, InjectedFormProps, FormSection } from "redux-form";
import { Page } from "../../layout";
import AmpliconTopBar from "../AmpliconTopBar/AmpliconTopBar";
import AmpliconData from "../AmpliconData/AmpliconData";
import AmpliconPrimerDesign from "../AmpliconPrimerDesign/AmpliconPrimerDesign";
import asyncValidate from "./asyncValidate";
import { warn } from "./warn";

const AmpliconForm: React.FC<InjectedFormProps> = (
) => {
    return (
        <form>
            <Page.Title>Amplicon Design</Page.Title>
            <FormSection name="topbar">
                <AmpliconTopBar />
            </FormSection>
            <FormSection name="data">
                <AmpliconData />
            </FormSection>
            <Page.Title>Primer Design</Page.Title>
            <FormSection name="primerDesign">
                <AmpliconPrimerDesign />
            </FormSection>
        </form>
    );
};

export default reduxForm<{}>({
    form: "amplicon",
    enableReinitialize: true,
    asyncValidate,
    warn,
    asyncBlurFields: ["topbar.gene"],
})(AmpliconForm);
