import { BASE_SEQUENCE_REGEX, SEQUENCE_REGEX } from "../../../constants/regexes";
import { isEmpty } from "lodash";
import nullTagPCRMethods from "../../../constants/nullBDTTagPCRMethods";

const isNonStandardSequence = (value: string | undefined): boolean => {
    if (value && !BASE_SEQUENCE_REGEX.test(value) && SEQUENCE_REGEX.test(value)) {
        return true;
    } else {
        return false;
    }
};

export const warn = (values: any, allValues: any) => {
    
    const warnings = { primerDesign: {} as any } as any;
    if (isEmpty(values)) return warnings;
    if (isNonStandardSequence(values.primerDesign.forward_pcr_seq)) {
        warnings.primerDesign.forward_pcr_seq = "Non Standard Sequence Symbol";
    }
    if (isNonStandardSequence(values.primerDesign.reverse_pcr_seq)) {
        warnings.primerDesign.reverse_pcr_seq = "Non Standard Sequence Symbol";
    }
    if (isNonStandardSequence(values.primerDesign.forward_seq_seq)) {
        if (!nullTagPCRMethods.includes(allValues?.values?.data?.pcr_method)) {
            warnings.primerDesign.forward_seq_seq = "Non Standard Sequence Symbol";
        }
    }
    if (isNonStandardSequence(values.primerDesign.reverse_seq_seq)) {
        if (!nullTagPCRMethods.includes(allValues?.values?.data?.pcr_method)) {
            warnings.primerDesign.reverse_seq_seq = "Non Standard Sequence Symbol";
        }
    }
    return warnings;
};
