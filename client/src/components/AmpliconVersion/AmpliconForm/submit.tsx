import { alertSuccess, alertError } from "../../../services/alert";
import api from "../../../services/api";

const submitModify = (values: any, setLoading: (arg0: boolean) => void) => {
    const ampliconDesign = { ...values.data, ...values.topbar };
    ampliconDesign.batching_data = {
        folder: "Sanger Worklists",
        workbook: "Sanger-Active",
        sheet: values.data.sheet,
    };
    // Must convert empty redux field from empty string to null
    if (ampliconDesign.gc_content == "") {
        ampliconDesign.gc_content = null;
    }
    const data = {
        ampliconVersionDesign: ampliconDesign,
        primerDesign: values.primerDesign,
    };

    api.put("/api/v2/amplicon-version", data)
        .then((response) => {
            // We use PUT for update and duplicate; use the status code to determine what alert to give the user.
            if (response.status === 201) {
                alertSuccess("Amplicon Created");
            }
            else {
                alertSuccess("Amplicon Modified");
            }
        })
        .catch((e) => {
            alertError("Error: " + e.response.data.errors);
        })
        .finally(() => {
            setLoading(false);
        });
};

const submitCreate = (values: any, setLoading: (arg0: boolean) => void) => {
    const ampliconDesign = { ...values.data, ...values.topbar };
    ampliconDesign.batching_data = {
        folder: "Sanger Worklists",
        workbook: "Sanger-Active",
        sheet: values.data.sheet,
    };
    // Must convert empty redux field from empty string to null
    if (ampliconDesign.gc_content == "") {
        ampliconDesign.gc_content = null;
    }
    const data = {
        ampliconVersionDesign: ampliconDesign,
        primerDesign: values.primerDesign,
    };
    api.post("/api/v2/amplicon-version", data)
        .then(() => {
            alertSuccess("Amplicon Created");
        })
        .catch((e) => {
            alertError("Error: " + e.response.data.errors);
        })
        .finally(() => {
            setLoading(false);
        });
};

export { submitModify, submitCreate };
