import React from "react";
import { ListGroupItem } from "react-bootstrap";
import { reduxForm, InjectedFormProps, FormSection, Field } from "redux-form";

import { RtpcrAmplicon } from "../../../../types";
import { ReduxSelectFormGroup, renderSwitchInput } from "../../../components/form/redux-form-group";
import styles from "../Cards/CardStyles.module.scss";

interface FormPropsWithPassthrough extends InjectedFormProps {
    row?: RtpcrAmplicon,
}

const fieldStyles = {
    controlClassName: styles.controlStyle,
    errorClassName: styles.ErrorStyle
};

const FormFields: React.FC = () => {
    return (
        <React.Fragment>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="Validation"
                    name="validation_status"
                    component={ReduxSelectFormGroup}
                    type="select"
                    options={[
                        "Imported",
                        "Validated",
                        "Approved",
                        "Pending Approval",
                        "Approved CUSTOM",
                        "Approved SANGER",
                        "Approved NGS",
                        "Retired",
                        "Failed",
                    ]}
                    {...fieldStyles}
                />
            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <Field
                    label="Priority"
                    name="priority"
                    component={ReduxSelectFormGroup}
                    type="select"
                    options={["Backup", "Primary"]}
                    {...fieldStyles}
                />

            </ListGroupItem>
            <ListGroupItem className={styles.list_group_item}>
                <div style={{display: "flex", justifyContent: "space-between"}}>
                    <label className="form-label">Control Pool</label>
                    <Field
                        style={{paddingTop: "50px"}}
                        name="control_pool"
                        id="controlPoolSwitch"
                        component={renderSwitchInput}
                    />
                </div>
            </ListGroupItem>
        </React.Fragment>
    );
}

const RtpcrAdditionalDataForm: React.FC<FormPropsWithPassthrough> = () => (
    <form>
        <FormSection name="data">
            <FormFields />
        </FormSection>
    </form>
)

export default reduxForm<{}>({})(RtpcrAdditionalDataForm);