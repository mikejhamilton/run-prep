import styles from "./AmpliconData.module.scss";
import React from "react";
import { Row, Col, Container } from "react-bootstrap";
import { SangerThermodynamicPropertiesCard } from "../Cards/SangerThermoProperties";
import { CodingReferenceCard } from "../Cards/CodingReference";
import { AdditionalSangerInformationCard } from "../Cards/AdditionalSangerInformation";

const AmpliconData: React.FC = () => {
    return (
        <Container fluid className={styles.dataform}>
            <Row>
                <Col>
                    <CodingReferenceCard readonly={false}/>
                </Col>
                <Col>
                    <SangerThermodynamicPropertiesCard readonly={false}/>
                </Col>
                <Col>
                    <AdditionalSangerInformationCard readonly={false}/>
                </Col>
            </Row>
        </Container>
    );
};

export default AmpliconData;
