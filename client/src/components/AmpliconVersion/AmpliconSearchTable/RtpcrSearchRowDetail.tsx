import React from "react";
import { Card, Table, Container, Row, Col } from "react-bootstrap";
import { isEmpty } from "lodash";

import { Design, RtpcrAmplicon, RtpcrPrimer } from "../../../../types";
import { AdditionalRtpcrInformationCard } from "../Cards/AdditionalRtpcrInformation";
import { RtpcrCodingReferenceCard } from "../Cards/RtpcrCodingReference";
import { RtpcrThermodynamicPropertiesCard } from "../Cards/RtpcrThermoProperties";
import styles from "./SearchRowDetail.module.scss";
import ModificationInfo from "../Cards/ModificationInfo";

interface SearchRowDetailProps {
    amplicon: RtpcrAmplicon;
}

const PrimerDesignTable: React.FC<Collection<RtpcrPrimer[]>> = (props: Collection<RtpcrPrimer[]>) => {

    const getLabel = (design: Design) => {
        if (design.direction === "forward") {
            return "Forward Primer"
        }
        else if (design.direction === "reverse") {
            return "Reverse Primer"
        } else {
            return "Unknown Primer Direction"
        }
    }

    /**
     * Given an rtpcr primer, returns a table row with the appropriate data cells.
     * The Junction primer cell will be null if the primer does not contain a second design.
     * Function assumes that junction designs are stored as multiple designs on a single primer.
     */
    const getPrimerRowElement = (primer: RtpcrPrimer) => {

        const genomicPosition =
        primer.designs[0].chromosome +
        ":" +
        String(primer.designs[0].start_position) +
        "-" +
        String(primer.designs[0].end_position);
        
        let junctionPosition = null;
        if (primer.designs.length > 1) {
          junctionPosition =
            primer.designs[1].chromosome +
            ":" +
            String(primer.designs[1].start_position) +
            "-" +
            String(primer.designs[1].end_position);
        }

        return (
            <tr key={primer.name}>
                <td>{primer.name}</td>
                <td>{getLabel(primer.designs[0])}</td>
                <td>{primer.designs[0].sequence}</td>
                <td>{genomicPosition}</td>
                {junctionPosition? <td>{junctionPosition}</td> : <td />}
            </tr>
        )
    }

    return (
        <Card>
            <Card.Header>Primer Designs</Card.Header>
            <Table striped hover>
                <thead>
                    <tr>
                        <td><strong>Name</strong></td>
                        <td><strong>Category</strong></td>
                        <td><strong>Sequence</strong></td>
                        <td><strong>Genomic Coordinates</strong></td>
                        <td><strong>Junction Coordinates</strong></td>
                    </tr>
                </thead>
                <tbody>
                    {props.primers.map((primer: RtpcrPrimer) => getPrimerRowElement(primer))}
                </tbody>
            </Table>
        </Card>
    )
}

const RtpcrSearchRowDetail: React.FC<SearchRowDetailProps> = ({amplicon}: SearchRowDetailProps) => {

    return (
        <Container className={styles.search_row_detail}>
            <Row>
                <Col><RtpcrCodingReferenceCard amplicon={amplicon} /></Col>
                <Col><AdditionalRtpcrInformationCard readonly={true} amplicon={amplicon}/></Col>
                <Col>
                    <RtpcrThermodynamicPropertiesCard amplicon={amplicon} />
                    <ModificationInfo ampliconVersion={amplicon} />
                </Col>
            </Row>
            <Row>
                <Col>{isEmpty(amplicon.rtpcr_primers)? <div className={styles.noPrimers}>No Primers Found</div> : <PrimerDesignTable primers={amplicon.rtpcr_primers} />}</Col>
            </Row>
        </Container>
    )
}

export default RtpcrSearchRowDetail;