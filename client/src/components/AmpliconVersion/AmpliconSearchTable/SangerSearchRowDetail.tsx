import React from "react";
import { isEmpty } from "lodash";
import { Button, Card, Table, Container, Row, Col } from "react-bootstrap";

import { ISangerPrimerDesign } from "../../../../types/amp";
import LinkToDuplicateAmplicon from "../../common/links/DuplicateSangerAmplicon";
import LinkToUpdateAmplicon from "../../common/links/UpdateSangerAmplicon";
import { ListGroupCard } from "../../common/cards/ListGroup";
import { AdditionalSangerInformationCard } from "../Cards/AdditionalSangerInformation";
import { CodingReferenceCard } from "../Cards/CodingReference";
import { SangerThermodynamicPropertiesCard } from "../Cards/SangerThermoProperties";
import styles from "./SearchRowDetail.module.scss";
import SearchRowCommentList from "components/Comment/SearchRowCommentList";
import { SangerAmplicon } from "../../../../types/amp";
import ModificationInfo from "../Cards/ModificationInfo";

interface SangerSearchRowDetailProps {
    row: SangerAmplicon;
    primers: Array<ISangerPrimerDesign>
}

const ActionCard: React.FC<SangerSearchRowDetailProps> = (props: SangerSearchRowDetailProps) => (
    <ListGroupCard title="Actions">
                    <Row>
                        <Col>
                            <div style={{display: "flex", justifyContent: "center"}}>
                                <div style={{marginLeft: "1vw", marginRight: "1vw"}}>
                                    <LinkToUpdateAmplicon id={props.row.id}>
                                        <Button variant="secondary">Update</Button>
                                    </LinkToUpdateAmplicon>
                                </div>
                                <div style={{marginLeft: "1vw", marginRight: "1vw"}}>
                                    <LinkToDuplicateAmplicon id={props.row.id}>
                                        <Button variant="primary">Duplicate</Button>
                                    </LinkToDuplicateAmplicon>
                                </div>
                            </div>
                        </Col>
                    </Row>
    </ListGroupCard>
    )

const PrimerDesignTable: React.FC<Collection<Array<ISangerPrimerDesign>>> = (props: Collection<Array<ISangerPrimerDesign>>) => {

    const getLabel = (primer: ISangerPrimerDesign) => {
        if (primer.direction === "forward") {
            return "Forward " + primer.step + " Primer"
        }
        else if (primer.direction === "reverse") {
            return "Reverse " + primer.step + " Primer"
        } else {
            return "Unknown Primer Direction"
        }
    }

    const getRowElement = (primer: ISangerPrimerDesign, idx: number) => {

        let genomicPosition = "";

        if ((primer.start_position !== -1) && (primer.end_position !== -1)) {
            genomicPosition = ":" + String(primer.start_position) + "-" + String(primer.end_position);
        }

        return (
            <tr key={idx}>
                <td>{getLabel(primer)}</td>
                <td>{primer.sanger_primer_name}</td>
                <td>{primer.sequence}</td>
                <td>{primer.chromosome}{genomicPosition}</td>
            </tr>
        )
    }

    const displayPrimers = () => {
        const primerSortOrder = ['Forward PCR Primer', 'Reverse PCR Primer', 'Forward BDT Primer', 'Reverse BDT Primer']
        const primersCopy =[...props.primers]
        const sortedPrimers = primersCopy.sort((a,b) => {
            return primerSortOrder.indexOf(getLabel(a)) - primerSortOrder.indexOf(getLabel(b))
        })
        return sortedPrimers.map((primer: ISangerPrimerDesign, idx: number) => getRowElement(primer, idx))
    }

    return (
        <Card>
            <Card.Header>Primer Designs</Card.Header>
            <Table striped hover>
                <thead>
                    <tr>
                        <td><strong>Category</strong></td>
                        <td><strong>Name</strong></td>
                        <td><strong>Sequence</strong></td>
                        <td><strong>Genomic Coordinates</strong></td>
                    </tr>
                </thead>
                <tbody>
                    {displayPrimers()}
                </tbody>
            </Table>
        </Card>
    )
}

const SangerSearchRowDetail: React.FC<SangerSearchRowDetailProps> = (props: SangerSearchRowDetailProps) => {
    return (
        <Container className={styles.search_row_detail}>
            <Row>
                <Col><CodingReferenceCard readonly={true} row={props.row} /></Col>
                <Col><SangerThermodynamicPropertiesCard readonly={true} row={props.row} /></Col>
                <Col><AdditionalSangerInformationCard readonly={true} row={props.row} /></Col>
                <Col>
                    <Row>
                        <div className={styles.dateBox}>
                            <ActionCard row={props.row} primers={props.primers}/>
                        </div>
                    </Row>
                    <Row>
                        <div className={styles.dateBox}>
                            <ModificationInfo ampliconVersion={props.row} />
                        </div>
                    </Row>
                </Col>
            </Row>
            <Row>
                <Col>
                    <SearchRowCommentList ampliconVersionId={props.row.id} />
                </Col>
            </Row>
            <Row>
                <Col>{isEmpty(props.primers)? <div className={styles.noPrimers}>No Primers Found</div> : <PrimerDesignTable primers={props.primers} />}</Col>
            </Row>
        </Container>
    )
}

export default SangerSearchRowDetail;