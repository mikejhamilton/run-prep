import React, { useState } from "react";
import { Button, Modal, Spinner } from "react-bootstrap";
import api from "../../../../services/api";
import fileDownload from "js-file-download";
import styles from "./ExportModal.module.scss";
import { ISearchFilter } from "../../../../../types/amp";
import { getQueryParamsFromFilters } from "../../../../util";

interface exportModalProps {
    show: boolean;
    handleClose: () => void;
    searchFilters: ISearchFilter[];
    endpoints: {fullExport: string, searchExport: string};
}

const ExportModal: React.FC<exportModalProps> = ({endpoints, handleClose, searchFilters, show}: exportModalProps) => {
    const [waitingDownload, setWaitingDownload] = useState(false);

    const handleFullTableExport = () => {
        setWaitingDownload(true);
        api.get(endpoints.fullExport, {
            responseType: "blob",
        })
            .then((res) => {
                fileDownload(res.data, "fulldatabase.tsv");
            })
            .finally(() => {
                setWaitingDownload(false);
                handleClose();
            });
    };

    const handleCurrentTableExport = () => {
        setWaitingDownload(true);

        const queryParams = getQueryParamsFromFilters(searchFilters)
        api.get(endpoints.searchExport, {
            responseType: "blob",
            params: queryParams,
        })
            .then((res) => {
                fileDownload(res.data, "currentdatabase.tsv");
            })
            .finally(() => {
                setWaitingDownload(false);
                handleClose();
            });
    };

    const fullTableDownloadButton = () => {
        const text = waitingDownload ? (
            <Spinner animation="border" role="status" aria-hidden="true" />
        ) : (
            "Export Full Table"
        );
        return (
            <Button disabled={waitingDownload} className={styles.downloadButton} onClick={handleFullTableExport}>
                {text}
            </Button>
        );
    };

    const searchTableDownloadButton = () => {
        const text = waitingDownload ? (
            <Spinner animation="border" role="status" aria-hidden="true" />
        ) : (
            "Export Current Table"
        );
        return (
            <Button disabled={waitingDownload} className={styles.downloadButton} onClick={handleCurrentTableExport}>
                {text}
            </Button>
        )

    };

    return (
        <Modal centered show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Warning!</Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles.modalText}>
                <h4>Are you sure you want to export the table?</h4>
            </Modal.Body>
            <Modal.Footer>
                {fullTableDownloadButton()}
                {searchTableDownloadButton()}
            </Modal.Footer>
        </Modal>
    );
};

export default ExportModal;
