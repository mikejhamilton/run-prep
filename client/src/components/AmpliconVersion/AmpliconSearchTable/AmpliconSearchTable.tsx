import React from "react";
import { Badge } from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import styles from "./SearchRowDetail.module.scss";
import { AmpliconVersion } from "../../../../types";
import { ISOFORM_FORMAT_REGEX } from '../../../constants/regexes';
import { getConfigValue, getPrimerByDirectionAndStep } from '../../../util';
import { getCRange } from "../../../util/AmpliconVersion/AmpliconVersionUtils";

function linkGeneToAVAFormatter(value: string) {
    let evaLink = getConfigValue('AVA_BASE_URL') + 'genepage?gene=';

    if(ISOFORM_FORMAT_REGEX.test(value))
        evaLink += value.substring(0, value.length - 2);
    else
        evaLink += value;

    return (
        <a href={evaLink} target="_blank">
            {value}
        </a>
    )
}

function primerNameFormatter(value: string, row: AmpliconVersion) {
    // Since TypeScript (currently) does not afford a sensible instanceof-like method to check interfaces or types,
    // use the primer key name to differentiate between Sanger and Rtpcr Amplicon Versions.
    if ('rtpcr_primers' in row) {
        return (
            <div>{value}</div>
        )
    } else {
        let forwardBDT = getPrimerByDirectionAndStep(row.primers, 'BDT', 'forward');
        let reverseBDT = getPrimerByDirectionAndStep(row.primers, 'BDT', 'reverse');

        if(forwardBDT == null || reverseBDT == null || (forwardBDT.sanger_primer_name == 's-tag' && reverseBDT.sanger_primer_name == 'as-tag')) {
            return (
                <div>{value}</div>
            )
        } else {
            return (
                <div>{value}
                    <Badge className={styles.intBadge} variant="info">
                        int
                    </Badge>
                </div>
            )
        }
    }
}

function cRangeFormatter(value: any, row: AmpliconVersion, index: any) {
    return getCRange(row);
}

const sortCaret = (order: string) => {
    if (order === undefined) {
        return null;
    }
    return <span className={`pull-right mr-2 fa ${order === 'desc' ? 'fa-caret-down' : 'fa-caret-up'}`}/>;
}

function statusFormatter(value: string) {
    let variant = "light";
    switch (value) {
        case "Pending Approval":
            variant = "warning";
            break;
        case "Failed":
            variant = "danger";
            break;
        case "Retired":
            variant = "secondary";
            break;
        case "Imported":
            variant = "info";
            break;
        case "Validated":
            variant = "success";
            break;
        default:
            variant = "success-light";
    }
    return (
        <Badge className={styles.validationBadge} variant={variant}>
            {value}
        </Badge>
    );
}

interface AmpliconSearchTableProps {
    amplicons: Array<object>;
    page: number;
    sizePerPage: number;
    totalSize: number;
    handler: any;
    setExpandedRows: React.Dispatch<React.SetStateAction<number[]>>;
    expandedRows: Array<number>;
    sorting: {
        orderField: string,
        setOrderField: React.Dispatch<React.SetStateAction<string>>,
        isDesc: boolean,
        setIsDesc: React.Dispatch<React.SetStateAction<boolean>>,
    }
    rowDetail: (row: AmpliconVersion) => React.ReactNode;
}

const AmpliconSearchTable: React.FC<AmpliconSearchTableProps> = (
    props: AmpliconSearchTableProps
) => {

    const handleOnSort = (field: string, order: string) => {
        props.sorting.setOrderField(field);
        props.sorting.setIsDesc(order === 'desc');
    }

    const columns = [
        {
            dataField: 'id',
            hidden: true,
        },
        {
            dataField: "amplicon.gene_symbol",
            sort: true,
            onSort: handleOnSort,
            text: "Gene",
            formatter: linkGeneToAVAFormatter,
        },
        {
            dataField: "isoform_version",
            sort: true,
            onSort: handleOnSort,
            text: "Isoform",
        },
        {
            dataField: "pcr_primer_pair_name",
            sort: true,
            onSort: handleOnSort,
            text: "PCR Primer Pair Name",
            formatter: primerNameFormatter,
        },
        {
            dataField: "c_range",
            sort: true,
            onSort: handleOnSort,
            text: "C. Range",
            formatter: cRangeFormatter,
        },
        {
            dataField: "priority",
            sort: true,
            onSort: handleOnSort,
            text: "Priority",
        },
        {
            dataField: "validation_status",
            sort: true,
            onSort: handleOnSort,
            text: "Validation Status",
            formatter: statusFormatter,
        },
    ];

    const handleOnExpand = (row: any, isExpand: boolean, rowIndex: number, e: any) => {
        if (isExpand) {
            // @ts-ignore
            props.setExpandedRows(prevExpanded => [...prevExpanded, row.id])
        } else {
            // @ts-ignore
            props.setExpandedRows(prevExpanded => prevExpanded.filter(x => x !== row.id))
        }
    }

    const expandRow = {
        renderer: props.rowDetail,
        showExpandColumn: true,
        expandByColumnOnly: true,
        expandColumnPosition: "right",
        onExpand: handleOnExpand,
        expanded: props.expandedRows,
    };
    return (
        <BootstrapTable
            remote
            keyField="id"
            data={props.amplicons}
            columns={columns}
            bordered={false}
            expandRow={expandRow}
            pagination={paginationFactory({
                page: props.page,
                sizePerPage: props.sizePerPage,
                totalSize: props.totalSize,
                showTotal: true,
                hidePageListOnlyOnePage: true,
            })}
            sort={{
                sortCaret,
                onSort: handleOnSort,
            }}
            onTableChange={props.handler}
            defaultSortDirection={'asc'}
            defaultSorted={[{
                dataField: props.sorting.orderField,
                order: props.sorting.isDesc ? 'desc' : 'asc',
            }]}
        />
    );
};

export default AmpliconSearchTable;
