import React from "react";
import { Button, OverlayTrigger, Spinner } from "react-bootstrap";
import PermissionToolTip from "./PermissionToolTip";

/**
 * Returns a button that performs an function if the correct permissions are granted.
 * If access is false, returns an overlay trigger with a tooltip and a disabled button.
 * If access is true, returns a standard button which triggers the function on click.
 */
const PermissionOverlayButton = (
    access: boolean,
    loading: boolean,
    btnText: string,
    btnFunc: any,
    btnStyle: any,
) => {
    return access ? (
        <Button style={btnStyle} onClick={btnFunc}>
            {loading ? <Spinner animation="border" /> : btnText}
        </Button>
    ) : (
        <OverlayTrigger
            placement="left"
            delay={{ show: 250, hide: 400 }}
            overlay={PermissionToolTip}
        >
            <div>
                <Button style={{ ...btnStyle, cursor: "not-allowed" }} disabled={true}>
                    {btnText}
                </Button>
            </div>
        </OverlayTrigger>
    );
};

export default PermissionOverlayButton;
