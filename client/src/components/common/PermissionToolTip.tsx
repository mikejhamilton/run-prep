import React from "react";
import { Tooltip } from "react-bootstrap";

const PermissionToolTip = (props: any) => (
    <Tooltip id="button-tooltip" {...props}>
        <p style={{ fontSize: "14px" }}>Insufficient Privileges</p>
    </Tooltip>
);

export default PermissionToolTip