import React from "react";
import { Spinner, SpinnerProps } from "react-bootstrap";
import './loading.scss';

const spinnerStyle = (delay: string) => ({
    animationDelay: delay,
});

const Loading: React.FC<any> = (spinnerProps: SpinnerProps | null) => {
    return <div style={{textAlign: 'center'}}>
    {['0', '150ms', '300ms'].map(delay => (
        <Spinner key={delay} animation='grow' className='loading' style={spinnerStyle(delay)} {...spinnerProps}/>
    ))}
</div>};

export default Loading;
