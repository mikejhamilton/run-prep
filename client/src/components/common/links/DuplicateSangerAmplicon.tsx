import React, { memo } from 'react';
import { NavLink } from 'react-router-dom';
import { DUPLICATE_SANGER_AMPLICON } from '../../../constants/routes';

const LinkToDuplicateSangerAmplicon: LinkTypeWithQueryParams = ({
  id,
  ...props
}) => <NavLink to={DUPLICATE_SANGER_AMPLICON(String(id))} {...props} />;

export default memo(LinkToDuplicateSangerAmplicon);