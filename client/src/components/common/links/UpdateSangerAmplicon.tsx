import React, { memo } from 'react';
import { NavLink } from 'react-router-dom';
import { UPDATE_SANGER_AMPLICON } from '../../../constants/routes';

const LinkToUpdateSangerAmplicon: LinkTypeWithQueryParams = ({
  id,
  ...props
}) => <NavLink to={UPDATE_SANGER_AMPLICON(String(id))} {...props} />;

export default memo(LinkToUpdateSangerAmplicon);