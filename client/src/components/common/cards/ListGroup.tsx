import React from "react";
import { Card, ListGroup } from "react-bootstrap";


interface StyledListGroupItemProps<T> {
    label: string;
    value: T;
}

/**
 * Helper function for creating list group items
 */
export const StyledListGroupItem: React.FC<StyledListGroupItemProps<any>> = (props: StyledListGroupItemProps<any>) => {
    return (
        <ListGroup.Item style={{backgroundColor: "#F2F5F7", marginBottom: "0px"}}>
            <strong>{props.label}</strong>:
            {typeof props.value === "boolean" ?
                    <span style={{float: "right"}}>
                        {props.value ? <i className="fa fa-check-square-o"/> : <i className="fa fa-square-o"/>}
                    </span>
            :
                    <span style={{float: "right"}}>{unescape(props.value)}</span>
            }
        </ListGroup.Item>
    )
}

type ListGroupCardProps = {
    title: string
}

export const ListGroupCard: React.FC<ListGroupCardProps> = ({title, children}) => {
    return (
        <Card>
            <Card.Header>{title}</Card.Header>
            <Card.Body>
                <ListGroup>
                    {children}
                </ListGroup>
            </Card.Body>
        </Card>
    )
}

type ListGroupCardHeaderButtonProps = {
    title: string;
    titleButton?: JSX.Element;
}

export const ListGroupCardHeaderButton: React.FC<ListGroupCardHeaderButtonProps> = ({
    title,
    titleButton,
    children,
}) => (
    <Card>
        <Card.Header>{title}
            {titleButton}
        </Card.Header>
        <Card.Body>
            <ListGroup>
                {children}
            </ListGroup>
        </Card.Body>
    </Card>
)