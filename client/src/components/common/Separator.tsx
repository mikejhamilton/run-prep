import './separator.scss';
import React from 'react';

const Separator: React.FC = () => {
    return <hr className="separator"/>
};

export default Separator;
