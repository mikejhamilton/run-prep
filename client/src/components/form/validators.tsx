import {isNil} from "lodash";

export const isInteger = (value: any) => {
    return (!isNaN(value) &&
        parseInt(value) == value && 
        !isNaN(parseInt(value, 10))) ? "" : "Integer Required"
}

export const isNumber = (value: number | string) => {
    if (!value) {
        return ""
    }
    return (!(value) || isNaN(Number(value)))? "Number required" : null;
}

export const required = (value: string) => {
    if (isNil(value) || value === '') {
        return "This field is required";
    }
    return null;
}

export const maxLength = (max: number) => (value: string) =>
    value && value.length > max ? `Must be ${max} characters or fewer.` : undefined;

export const maxLength50 = maxLength(5);