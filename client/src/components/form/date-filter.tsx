import React from "react";
import { Col, Row } from "react-bootstrap";
import { reduxForm, Field, InjectedFormProps } from "redux-form";

import { ReduxInputFormGroup } from "./redux-form-group";

interface DateFilterProps {
    disabled: boolean;
    handleSubmit: () => void;
}

interface DateFilterValues {
    lowDate: string;
    highDate: string;
}

const DateFilter: React.FC<DateFilterProps & InjectedFormProps<{}, DateFilterProps>> = (props: DateFilterProps) => {
    const {disabled, handleSubmit} = props;
    return (
        <form>
            <Row>
                <Col sm={6}>
                    <Field
                        name="lowDate"
                        label="From"
                        type="date"
                        disabled={disabled}
                        component={ReduxInputFormGroup}
                        onBlur={handleSubmit}
                    />
                </Col>
                <Col sm={6}>
                    <Field
                        name="highDate"
                        label="To"
                        type="date"
                        disabled={disabled}
                        component={ReduxInputFormGroup}
                        onBlur={handleSubmit}
                    />
                </Col>
            </Row>
        </form>
    );
};

const validate = (values: DateFilterValues) => {
    const errors: any = {};
    if (values.lowDate && values.highDate) {
        if (values.lowDate > values.highDate) {
            errors['lowDate'] = 'From date cannot be after to date';
        }
    }
    return errors;
};

export default reduxForm<{}, DateFilterProps>({
    form: 'date-filter',
    validate,
})(DateFilter);
