import React, { CSSProperties } from "react";
import {
    Button,
    FormControl,
    FormGroup,
    FormLabel,
    FormText,
    OverlayTrigger,
    Tooltip,
    InputGroup,
} from "react-bootstrap";
import { alertError } from "../../services/alert";
import "font-awesome/css/font-awesome.min.css";
import { forEach } from "lodash";
import Switch from "react-switch";
import Select from "react-select";

const renderSwitchInput = (field: any) => (
    <Switch
        onChange={(checked: any, event: any) => {
            if ("extraHandler" in field) {
                field.extraHandler(checked);
            }
            field.input.onChange(checked, event);
        }}
        checkedIcon={false}
        uncheckedIcon={false}
        checked={field.input.value}
    />
);

function renderFormControl(control: any, meta: any, label: any, tipText = "", errorClassName = "") {
    let comment = null;
    let warningComment = null;
    if (meta.error && meta.touched) {
        comment = meta.error;
    }
    if (meta.warning && meta.touched) {
        warningComment = <i className="fa fa-exclamation-triangle"> {meta.warning}</i>;
    }
    label = label ? <FormLabel>{label}</FormLabel> : null;
    return tipText ? (
        <FormGroup>
            {label}
            <OverlayTrigger
                placement="right"
                delay={{ show: 50, hide: 200 }}
                overlay={
                    <Tooltip style={{ fontSize: "14px" }} id="t">
                        {tipText}
                    </Tooltip>
                }
            >
                <i style={{ padding: "3px" }} className="fa fa-info-circle" />
            </OverlayTrigger>
            {control}
            <FormText className={[errorClassName, "error-text"].join(" ")}>{comment}</FormText>
            <FormText className={[errorClassName, "error-text"].join(" ")}>{warningComment}</FormText>
        </FormGroup>
    ) : (
        <FormGroup>
            {label}
            {control}
            <FormText className={[errorClassName, "error-text"].join(" ")}>{comment}</FormText>
            <FormText className={[errorClassName, "error-text"].join(" ")}>{warningComment}</FormText>
        </FormGroup>
    );
}

const ReduxInputFormGroup: React.FC = (field: any) => {
    const control = field.onClear ? (
        <InputGroup>
            <FormControl
                className={field.controlClassName}
                disabled={field.disabled}
                isInvalid={field.meta.error && field.meta.touched}
                type={field.type}
                {...field.input}
            />
            <InputGroup.Append>
                <Button disabled={field.clearDisabled} variant="outline-secondary" onClick={field.onClear} style={{ border: "1px solid #003e69" }}>
                    <i className="fa fa-times-circle fa-md" style={{ padding: "5px" }}></i>
                </Button>
            </InputGroup.Append>
        </InputGroup>
    ) : (
        <FormControl
            className={field.controlClassName}
            disabled={field.disabled}
            isInvalid={field.meta.error && field.meta.touched}
            type={field.type}
            {...field.input}
        />
    );

    return renderFormControl(control, field.meta, field.label, field.tipText, field.errorClassName);
};

const ReduxSelectFormGroup: React.FC = (field: any) => {
    let control = (
        <FormControl
            className={field.controlClassName}
            isInvalid={field.meta.error && field.meta.touched}
            {...field.input}
            as="select"
        >
            {field.options.map((val: string) => (
                <option value={val} key={val}>
                    {val}
                </option>
            ))}
        </FormControl>
    );
    return renderFormControl(control, field.meta, field.label, "", field.errorClassName);
};

const ReduxFormMultiSelect: React.FC = (props: any) => {
    const colourStyles = {
        control: (styles: CSSProperties) => ({
            ...styles,
            borderColor: "#003e69",
        }),
        option: (styles: CSSProperties, state: any) => ({
            ...styles,
            color: state.isSelected ? "White" : "#003e69",
            backgroundColor: state.isSelected ? "#00aee7" : "white",
        }),
    };
    const { input, options } = props;
    const control = (
        <Select
            {...input}
            style={{ borderColor: "Green" }}
            isMulti={props.isMulti}
            onChange={(value) => input.onChange(value)}
            onBlur={() => input.onBlur(input.value)}
            options={options}
            styles={colourStyles}
        />
    );
    return renderFormControl(control, props.meta, props.label, "", props.errorClassName);
};

const ReduxFileFormGroup: React.FC = (props: any) => {
    const multiple = props.multiple ? true : false;
    let control = (
        <div>
            <input
                accept={props.accept}
                multiple={multiple}
                type="file"
                onChange={(e) => {
                    e.preventDefault();
                    const files = [...e.target.files];
                    let maxSizeReached = false;
                    if (props.maxSize) {
                        forEach(files, (f) => {
                            if (f.size / 1024 > props.maxSize) {
                                alertError(`File ${f.name} size greater than ${props.maxSize} KB`);
                                maxSizeReached = true;
                            }
                        });
                        if (maxSizeReached) e.target.value = "";
                    }
                    // Files should not be stored in redux store, state dispatch is passed through props
                    props.setFiles(files);
                }}
            />
        </div>
    );

    return renderFormControl(control, props.meta, props.label, props.tipText, props.errorClassName);
};

export { ReduxFormMultiSelect, ReduxInputFormGroup, ReduxSelectFormGroup, ReduxFileFormGroup, renderSwitchInput };
