import React from 'react';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";

import UTCToLocaleDate from "../../util/UTCToLocaleDate";
import EventRowDetail from "./EventRowDetail";
import { Event } from "../../../types";
import style from "./EventLogTable.module.scss";

const columns = [{
    dataField: 'id',
    hidden: true,
}, {
    dataField: 'created_at',
    text: 'Occurred At',
    classes: style.logTableDateCell,
    formatter: UTCToLocaleDate
}, {
    dataField: 'summary',
    text: 'Message',
    formatter: (cell: string, row: string, rowIndex: number) => <span>{cell}</span>,
    classes: style.logTableSummaryCell,
}, {
    dataField: 'username',
    text: 'User',
}];

interface EventLogTableProps {
    events: Array<Event>;
    page: number;
    sizePerPage: number;
    totalSize: number;
    handlePagination: any;
    setExpandedRows: any;
    expandedRows: any;
}

const EventLogTable: React.FC<EventLogTableProps> = (props: EventLogTableProps) => {

    const handleOnExpand = (row: any, isExpand: boolean, rowIndex: number, e: any) => {
        if (isExpand) {
            // @ts-ignore
            props.setExpandedRows(prevExpanded => [...prevExpanded, row.id])
        } else {
            // @ts-ignore
            props.setExpandedRows(prevExpanded => prevExpanded.filter(x => x !== row.id))
        }
    }
    const expandRow = {
        renderer: (event: Event) => <EventRowDetail event={event} />,
        className: style.detailRow,
        showExpandColumn: true,
        expandColumnPosition: "left",
        onExpand: handleOnExpand,
        expanded: props.expandedRows,
    };

    return <BootstrapTable
        remote
        keyField="id"
        data={props.events}
        columns={columns}
        bordered={false}
        expandRow={expandRow}
        pagination={paginationFactory({
            page: props.page,
            sizePerPage: props.sizePerPage,
            totalSize: props.totalSize,
            showTotal: true,
            hidePageListOnlyOnePage: true,
        })}
        onTableChange={props.handlePagination}
        rowClasses={style.logTableRow}
    />
}

export default EventLogTable;
