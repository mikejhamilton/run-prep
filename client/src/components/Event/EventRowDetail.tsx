import React from "react";

import { Event } from "../../../types";
import ReactJsonViewCompare from "react-json-view-compare";

interface EventRowDetailProps {
    event: Event;
}

const EventRowDetail: React.FC<EventRowDetailProps> = ({event}) => (
    <div>
        <span>{event.summary}</span>
        <ReactJsonViewCompare
            oldData={JSON.parse(event.initial_values)}
            newData={JSON.parse(event.final_values)}
        />
    </div>
);

export default EventRowDetail;