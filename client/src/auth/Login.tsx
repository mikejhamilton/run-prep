import React, { memo } from 'react';
import useAuth from './useAuth';
import { useDidMount } from '../services/hooks';
import { AmbryLoadingLogo } from '../components';

const Login: React.FC = () => {
  const { authenticateAndRedirect } = useAuth();
  useDidMount(() => {
    authenticateAndRedirect();
  });

  return (
    <AmbryLoadingLogo>
      <h1 className="text-center">Redirecting...</h1>
    </AmbryLoadingLogo>
  );
};

export default memo(Login);
