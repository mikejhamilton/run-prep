import React, { memo } from 'react';
import useAuth from './useAuth';
import { useDidMount } from '../services/hooks';
import { AmbryLoadingLogo } from '../components';

const Logout: React.FC = () => {
  const { logoutAndRedirect } = useAuth();

  useDidMount(() => {
    logoutAndRedirect();
  });

  return (
    <AmbryLoadingLogo>
      <h1 className="text-center">Redirecting...</h1>
    </AmbryLoadingLogo>
  );
};

export default memo(Logout);
