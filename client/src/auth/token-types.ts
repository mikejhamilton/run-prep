export enum TOKEN_TYPES {
  ID_TOKEN = 'idToken',
  ACCESS_TOKEN = 'accessToken'
}
