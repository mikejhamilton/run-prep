// Roughly based on: https://github.com/okta/okta-oidc-js/blob/40155af38840f4a66540ddb12589aa0398c50845/packages/okta-react/src/Auth.js
import AUTH_CONFIG from '../config/auth';
import { TOKEN_TYPES } from './token-types';
import { OktaAuth, Token } from '@okta/okta-auth-js';
import { LocalStorage, UserStorage } from '../services/LocalStorage';
import { UserData } from '../services/LocalStorage/UserStorage';

type MaybeToken = Maybe<Token>;
interface ILocation {
  pathname: string;
}

class AuthClient {
  private readonly _auth: OktaAuth;
  private readonly userStorage: UserStorage;
  private readonly previousLocation: LocalStorage<ILocation>;

  constructor() {
    this._auth = new OktaAuth(AUTH_CONFIG);
    this.userStorage = new UserStorage();
    this.previousLocation = new LocalStorage('previous-location');
  }

  async redirect(redirectAfterAuth: ILocation) {
    this.previousLocation.set(redirectAfterAuth);
    return this._auth.token.getWithRedirect({
      responseType: ['id_token', 'token'],
      scopes: ['openid', 'email', 'profile', 'groups']
    });
  }

  getUserData(): UserData | undefined {
    return this.userStorage.get() ?? undefined;
  }

  setUserData(data: UserData) {
    this.userStorage.set(data);
  }

  async isAuthenticated(): Promise<boolean> {
    return !!(await this.getAccessToken());
  }

  pullPreviousLocation(): ILocation {
    return this.previousLocation.pull() || { pathname: '/' };
  }

  logout(): Promise<void> {
    this.userStorage.remove();
    this._auth.tokenManager.clear();
    return this._auth.signOut({revokeAccessToken: true})
  }

  async isRedirect(): Promise<boolean> {
      return this._auth.token.isLoginRedirect()
  }

  getAccessToken(): Promise<MaybeToken> {
    return this._auth.tokenManager.get(TOKEN_TYPES.ACCESS_TOKEN);
  }

  async addFromUrl(): Promise<void> {
    const tokens = await this._auth.token
      .parseFromUrl()
      .then(res => res.tokens);

    this._auth.tokenManager.add(
      TOKEN_TYPES.ID_TOKEN,
      tokens[TOKEN_TYPES.ID_TOKEN]!
    );
    this._auth.tokenManager.add(
      TOKEN_TYPES.ACCESS_TOKEN,
      tokens[TOKEN_TYPES.ACCESS_TOKEN]!
    );
  }
}

export default AuthClient;
