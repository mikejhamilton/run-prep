import React from 'react';
import AuthClient from './AuthClient';
import { UserData } from "services/LocalStorage/UserStorage";

export interface IAuthContext {
  readonly authClient: AuthClient;
  readonly authenticated: Maybe<boolean>;
  readonly setAuthenticated: (authenticated: boolean) => void;
  readonly userData: Maybe<UserData>;
  readonly setUserData: (userData: UserData) => void;
}
// https://kentcdodds.com/blog/how-to-use-react-context-effectively
const AuthContext = React.createContext<IAuthContext | undefined>(undefined);

export default AuthContext;
