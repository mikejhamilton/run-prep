import React, { PureComponent } from 'react';
import AuthContext from './AuthContext';
import AuthClient from './AuthClient';
import { AmbryLoadingLogo } from '../components';
import { UserData } from 'services/LocalStorage/UserStorage';
import { isEqual } from 'lodash';

interface IAuthProviderProps {
  children: React.ReactNode;
  auth?: AuthClient;
}

interface IAuthProviderState {
  authenticated?: boolean;
  userData?: UserData;
}

class AuthProvider extends PureComponent<
  IAuthProviderProps,
  IAuthProviderState
> {
  private readonly auth: AuthClient;

  state = {
    authenticated: undefined,
    userData: undefined,
  };

  constructor(props: IAuthProviderProps) {
    super(props);
    this.auth = props.auth || new AuthClient();
  }

  async componentDidMount() {
    try {
      const token = await this.auth.getAccessToken();
      this.setAuthenticated(!!token);

      const userData = await this.auth.getUserData();
      if (userData) {
        this.setUserData(userData);
      }
    } catch (e) {
      // if there is an error we need to authenticate
      this.setAuthenticated(false);
    }
  }

  setAuthenticated = (authenticated: boolean) => {
    if (authenticated !== this.state.authenticated) {
      this.setState({ authenticated });
    }
  };

  setUserData = (userData: UserData) => {
    if (!isEqual(userData, this.state.userData)) {
      this.setState({userData});
  }
}

  get contextValue() {
    const {
      auth,
      setAuthenticated,
      setUserData,
      state: { authenticated, userData }
    } = this;

    return { authClient: auth, setAuthenticated, authenticated, userData, setUserData };
  }

  render() {
    const {
      props: { children },
      state: { authenticated }
    } = this;

    return authenticated === undefined ? (
      <AmbryLoadingLogo>
        <h1 className="text-center">Loading...</h1>
      </AmbryLoadingLogo>
    ) : (
      <AuthContext.Provider value={this.contextValue}>
        {children}
      </AuthContext.Provider>
    );
  }
}

export default AuthProvider;
