import React, { memo, useEffect } from 'react';
import useAuth from './useAuth';
import { LOGIN } from '../constants/routes';
import { RouteDefinition } from '../components/RoutesGenerator';
import {
  Redirect,
  Route,
  RouteChildrenProps,
  RouteProps
} from 'react-router-dom';

// Example from: https://reacttraining.com/react-router/web/example/auth-workflow
const SecureRoute: React.FC<RouteProps & RouteDefinition> = ({
  meta,
  children,
  ...rest
}) => {
  const { authenticated, fetchUserData, getUsername } = useAuth();

  // Fetch user info if we don't have access to the username within AuthContext.
  // This scenario generally means we are in the process of logging in, but could also occur if something
  // goes wrong with our application storage keeping track of username.
  useEffect(() => {
    if (!getUsername()) {
      fetchUserData();
    }
  }, [getUsername()]);

  if (meta) {
    const { title } = meta;
    if (title) {
      window.document.title = `AMP ${title}`;
    }
  }

  // If we are authenticated and children is undefined then this will fallback to the `component` prop that is passed.
  // Just like `<Route />` the below will have `children` take precedent over `component` prop.
  return (
    <Route {...rest}>
      {authenticated
        ? children
        : ({ location }: RouteChildrenProps<any>) => (
            <Redirect to={{ pathname: LOGIN, state: { from: location } }} />
          )}
    </Route>
  );
};

export default memo(SecureRoute);
