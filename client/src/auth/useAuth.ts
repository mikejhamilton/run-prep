import { useContext } from 'react';
import { useLocation, useHistory } from 'react-router';
import AuthContext, { IAuthContext } from './AuthContext';
import api from '../services/api'
import { UserData } from "services/LocalStorage/UserStorage";


interface IUseAuth extends IAuthContext {
    readonly authenticateAndRedirect: () => Promise<void>;
    readonly logoutAndRedirect: () => void;
    readonly fetchUserData: () => void;
    readonly getUserGroups: () => string[];
    readonly getUsername: () => string;
    readonly setUserData: (userData: UserData) => void;
}

type LocationState = {
    from: Location;
};
const useAuth = (): IUseAuth => {
    const context = useContext(AuthContext);
    // Error first so we don't have them later.
    // Typescript likes it this way.
    if (context === undefined) {
        throw new Error('useAuth must be used within AuthProvider');
    }
    const {authClient, authenticated, setAuthenticated, userData} = context;
    const history = useHistory();
    const location = useLocation<LocationState>();

    const fetchUserData = () =>
        api.get("/api/user/info")
            .then(async res => {
                const data = {username: res.data.username, groups: res.data.groups};
                setUserData(data);
            });

    const getUserGroups = () => authClient?.getUserData()?.groups ?? [];
    const getUsername = () => authClient?.getUserData()?.username ?? '';

    const setUserData = (userData: UserData): void => {
        context.setUserData(userData);
        authClient.setUserData(userData);
    }

    const backendLogin = async (): Promise<void> => {
        // Login to set last and second to last login timestamp
        api.post("/api/login").then(() => {})
            .catch(err => {
                console.error('An error occurred logging in.', err);
            });
    };

    const authenticateAndRedirect = async (): Promise<void> => {
        if (authenticated) {
            await backendLogin();
            return history.replace(authClient.pullPreviousLocation());
        }

        const isRedirect = await authClient.isRedirect();
        if (isRedirect) {
            await authClient.getAccessToken()
            // Add tokens to tokenManager.
            await authClient.addFromUrl();
            await authClient
                .isAuthenticated()
                .then(
                    async authenticated => {
                        setAuthenticated(authenticated);
                        if (authenticated) {
                            await backendLogin();
                        }
                    }
                );

            return history.replace(authClient.pullPreviousLocation());
        }

        // If we made it here we ARE NOT authenticated and need to send to SSO for authentication
        return authClient.redirect(location.state?.from);
    };

    // Need to enable CORS on okta for logout to work
    const logoutAndRedirect = (path: string = '/login') =>
        authClient.logout().then(() => {
            setAuthenticated(false);
            history.push(path);
        }).catch((err) => console.log("REJECTED" + err));

    return {
        authClient,
        authenticated,
        setAuthenticated,
        getUserGroups,
        getUsername,
        setUserData,
        authenticateAndRedirect,
        logoutAndRedirect,
        fetchUserData,
        userData,
    };
};

export default useAuth;
