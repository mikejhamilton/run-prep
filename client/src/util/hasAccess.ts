
export default (
    userGroups: string[], accessGroups: string[]
): boolean => {
    return (userGroups && userGroups.some((group) => accessGroups.includes(group)))
};
