import { COORD_REGEX } from "../constants/regexes";
import { IParsedCoord } from "../../types/amp";

export default (
    coord: string
): IParsedCoord => {
    const match = coord.match(COORD_REGEX);
    if (coord == "NA") return { chromosome: "NA", startPosition: -1, endPosition: -1 };
    if (!match) return { chromosome: null, startPosition: null, endPosition: null };
    return {
        chromosome: match!.groups!.chromosome,
        startPosition: Number(match!.groups!.start.replace(/,/g, "")),
        endPosition: Number(match!.groups!.end.replace(/,/g, "")),
    };
};
