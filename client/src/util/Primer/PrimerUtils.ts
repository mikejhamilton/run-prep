import { IParsedCoord } from "../../../types/amp";
import { ISangerPrimerDesign } from "../../../types/amp";

/**
 * Checks to primer coordinates and returns true if there is any overlap between the two coordinates.
 * Returns false if not on the same chromosome or no overlap.
 */
export const hasOverLap = (coordsX: IParsedCoord, coordsY: ISangerPrimerDesign): boolean => {
    if (coordsX.chromosome != coordsY.chromosome) {
        return false
    }
    if (
      coordsX.startPosition === null ||
      coordsY.end_position === null ||
      coordsY.start_position === null ||
      coordsX.endPosition === null
    ) {
      return false;
    }
    if (coordsX.startPosition <= coordsY.end_position && coordsY.start_position <= coordsX.endPosition) {
        return true
    } else {
        return false
    }
}
