import { PrimerDirections, PrimerSteps } from "../constants/primerProperties";
import { ISangerPrimerDesign } from "../../types/amp"

export default (primers: ISangerPrimerDesign[], step: PrimerSteps, direction: PrimerDirections): ISangerPrimerDesign | null => {
    for (const primer of primers) {
        if (primer.direction === direction && primer.step.toUpperCase() === step.toUpperCase()) {
            return primer
        }
    }
    return null
};