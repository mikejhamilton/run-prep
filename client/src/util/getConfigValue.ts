export default (config: string) => {
    const value =
      process.env[`REACT_APP_${config}`] || window.amp?.config[config];
    if (!value) console.error(`Undefined value for config ${config}.`);
    return value;
};
