export default (value: any): string => {
    if (!value) return "Unknown";
    const date = new Date(value + "Z")
    return date.toLocaleString();
}
