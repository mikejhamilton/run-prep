import { forEach, isEmpty } from "lodash";
import { ParsedUrlQuery } from "querystring";
import { ampliconSearchFilters, ampliconSearchConditions } from "../constants/ampliconSearchFilters";
import { ampliconVersionOrderFields } from "../constants/ampliconOrderFields";
import { ISearchFilter } from "../../types/amp";
import { getQueryParamsFromFilters } from "../util";

/**
 * Returns a query string for the low and high date passed in.
 * Returns empty string if both low date and high date are null.
 */
export const getDateRangeQuery = (lowDate: string, highDate: string) => {
    const dates = [];
    if (lowDate) dates.push(`lowDate=${lowDate}`);
    if (highDate) dates.push(`highDate=${highDate}`);
    return isEmpty(dates) ? "" : dates.join("&");
};

/**
 * Converts a list of row ids to a query string.
 * Returns empty string if row id list is empty.
 */
export const getExpandedRowQuery = (rows: number[]) => {
    return !isEmpty(rows) ? `expandedRows=${rows.join(",")}` : "";
};

/**
 * Returns pagination query string from the page and sizePerPage passed in.
 * Returns empty string if page or sizePerPage is null.
 */
export const getPaginationQuery = (page: number, sizePerPage: number) => {
    return sizePerPage && page ? `sizePerPage=${sizePerPage}&page=${page}` : "";
};

/**
 * Gets query string from amplicon search filter critiria.
 * Returns empty string if filter list is empty.
 */
export const getAmpliconSearchFilterQuery = (searchFilters: ISearchFilter[]) => {
    const query = getQueryParamsFromFilters(searchFilters);
    return query != null ? query.toString() : "";
};

/**
 * Gets query string from orderField parameter.
 * @param orderField
 */
export const getOrderFieldQuery = (orderField: string) =>
    !isEmpty(orderField) ? `orderField=${orderField}` : '';

/**
 * Returns parameter setting sort order to descending
 */
export const getIsDescQuery = () => 'isDesc=1';

/**
 * Parse pagination items from a ParsedURLQuery.
 * Returns a list with page and sizePerPage, either being undefined if not a number.
 * @param parsedQuery
 */
export const parsePaginationItems = (parsedQuery: ParsedUrlQuery) => {
    const page = parsedQuery["page"];
    const sizePerPage = parsedQuery["sizePerPage"];
    return [
        Number.isInteger(Number(page)) ? Number(page) : undefined,
        Number.isInteger(Number(sizePerPage)) ? Number(sizePerPage) : undefined,
    ];
};

/**
 * Parses low and high date range from ParsedURLQuery.
 * Returns a list with low and high date, either being undefiend if not in format YYYY/MM/DD.
 */
export const parseDateRange = (parsedQuery: ParsedUrlQuery) => {
    const date_regex = /(1|2)\d{3}-\d\d-\d\d/; // Best before year 3000
    const lowDate = parsedQuery["lowDate"] as string;
    const highDate = parsedQuery["highDate"] as string;
    return [date_regex.test(lowDate) ? lowDate : undefined, date_regex.test(highDate) ? highDate : undefined];
};

/**
 * Parses which bootstrap rows should be expanded from ParsedURLQuery.
 * Returns a list of row numbers from expected format '1,2,3'.
 * Removes all entries that are not an integer. Returns empty list if query param not present.
 */
export const parseExpandedRow = (parsedQuery: ParsedUrlQuery) => {
    const rowString = parsedQuery["expandedRows"] as string;
    if (rowString == null) return [];
    const rowList = rowString.split(",").map((stringId: string) => Number(stringId));
    const filteredRowList = rowList.filter((row) => Number.isInteger(row) && row !== 0);
    return filteredRowList;
};

/**
 * Parses LHS Bracket notation amplicon search filters from ParsedURLQuery.
 * LHS Bracket: https://www.moesif.com/blog/technical/api-design/REST-API-Design-Filtering-Sorting-and-Pagination/
 *
 * Example: Amplicon[Contains]='BLM'
 *
 * Removes all unknown filters from response list.
 */
export const parseAmpliconSearchFilters = (parsedQuery: ParsedUrlQuery): ISearchFilter[] => {
    const filters = [] as any;
    forEach(parsedQuery, (value, filter) => {
        const groups = filter.match(/(?<filter>[a-zA-Z ]+)\[(?<condition>[a-zA-Z ]+)\]/)?.groups;

        if (groups != undefined) {
            if (Array.isArray(value)) {
                forEach(value, (v) => {
                    filters.push({
                        value: v,
                        filter: groups?.filter,
                        condition: groups?.condition,
                    });
                });
            } else {
                filters.push({
                    value: value,
                    filter: groups?.filter,
                    condition: groups?.condition,
                });
            }
        }
    });
    // Only allow known amplicon search filters and conditions
    return filters.filter((filter: ISearchFilter) => {
        return ampliconSearchFilters.includes(filter.filter) && ampliconSearchConditions.includes(filter.condition);
    });
};

export const parseOrderField = (parsedQuery: ParsedUrlQuery): string => {
    const rowString = parsedQuery["orderField"] as string;
    if (rowString === null) return '';
    return ampliconVersionOrderFields.includes(rowString) ? rowString : '';
}

export const parseIsDesc = (parsedQuery: ParsedUrlQuery): boolean => {
    const rowString = parsedQuery["isDesc"] as string;
    if (rowString === undefined) {
        return false;
    }
    return rowString === '1' || rowString.toLowerCase() === 'true';
}

/**
 *
 * Parses bootstrap filters into LHS Bracket query parameters
 * Returns an object of the queries
 */
export const getParamsFromBootstrapFilters = (
    filters: [{ [key: string]: { filterVal: string; filterType: string; comparator: string; caseSensitive: boolean } }]
) => {
    const filter_queries = {} as any;
    forEach(filters, (values: any, dataField: string) => {
        filter_queries[`${dataField}[${values["comparator"]}]`] = values["filterVal"];
    });
    return filter_queries;
};
