import { isEmpty } from "lodash";
import { ISearchFilter } from "../../types/amp";

export default (filters: ISearchFilter[]) => {
    if (isEmpty(filters)) return;
    // Due to the possibility of mulitple duplicate keys, URLSearchParams must be used
    let params = new URLSearchParams()
    filters.map((f: ISearchFilter) => {
        params.append(`${f.filter}[${f.condition}]`, f.value)
    });
    return params
};