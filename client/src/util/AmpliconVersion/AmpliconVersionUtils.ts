import { head, isEmpty, orderBy } from "lodash";
import api from "../../services/api";

/**
 * Queries all amplicon versions for a specific gene and exon, returns the next version
 * number that does not exist yet
 */
export const getAvailableAmpliconVersionNumber = async (gene: string, exon: string) => {
    let versionNumber = 1
    await api.get("/api/v2/amplicon-version/fetch-by-gene-exon", {
        params: {
            gene: gene,
            exon: exon,
        },
    })
    .then((res) => {
        if (!isEmpty(res.data.api_data.results)) {
            const versions = orderBy(
                res.data.api_data.results,
                "version_number",
                "desc"
            );
            versionNumber = head(versions).version_number;
            versionNumber = versionNumber ? versionNumber + 1 : 1;
        }
    });
    return versionNumber
}

/**
 * Converts a version number to a string, if version = 1 return empty string.
 * Version: 2 -> "v2"
 */
export const getVersionString = (versionNumber: number | undefined): string => {
    return (versionNumber && (versionNumber > 1))? "v" + versionNumber : ""
}

/**
 * Subset of Amplicon Version parameters necessary for determining c. range format
 */
interface cRangeParameters {
    c_dot_start: number;
    c_dot_end: number;
    c_start_extension: number;
    c_end_extension: number;
    [x: string]: any;
}

/**
 * Accepts an object that includes at least c. related parameters.
 * Return extended c. range for intronic amplicon versions (those containing 'ivs')
 * and standard c. range for all others.
 */
export const getCRange = (p: cRangeParameters): string => {
    const {
        c_dot_start: cStart,
        c_dot_end: cEnd,
        c_start_extension: cStartExt,
        c_end_extension: cEndExt,
    } = p;
    // Show extended version of c. range if either extension field is not its default value.
    if (String(cStartExt) !== '-100' || String(cEndExt) !== '100') {
        const cStartSign = cStartExt < 0 ? "-" : "+";
        const cEndSign = cEndExt < 0 ? "-" : "+";
        return "c." + cStart + cStartSign + Math.abs(cStartExt) + "_" + cEnd + cEndSign + Math.abs(cEndExt);
    }
    return "c." + String(cStart) + "_c." + String(cEnd);
}