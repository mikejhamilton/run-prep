import './styles/main.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router';
import store from './redux/store';
import history from './redux/history';
import getConfigValue from "util/getConfigValue";

const render = () => {
    const App = require('./App').default;

    ReactDOM.render(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <App/>
            </ConnectedRouter>
        </Provider>,
        document.getElementById('root'),
    );
};

render();

if (getConfigValue('APP_ENV') === 'development' && module.hot) {
    module.hot.accept('./App', render);
}
