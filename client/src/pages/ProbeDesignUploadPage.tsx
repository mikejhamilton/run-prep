import React from "react";
import AccessDenied from "components/error/AccessDenied";
import { Page } from "../components/layout";
import { PROBE_DESIGN_GROUPS } from "config/userPermissions";
import useAuth from "auth/useAuth";
import { useTitle } from "../services/hooks";
import hasAccess from "util/hasAccess";
import ProbeDesignUploadForm from "../components/ProbeDesign/Upload/ProbeDesignUploadForm";

const ProbeDesignUploadPage: React.FC = () => {
    useTitle("Probe Design Upload");

    const initValues = {
        technology: {value: "MLPA", label: "MLPA"},
        library: {value: "177_CancerV5", label: "177_CancerV5"},
    };
    const {getUserGroups} = useAuth();
    const userGroups = getUserGroups();
    const hasProbeDesignAccess = hasAccess(userGroups, PROBE_DESIGN_GROUPS);

    return (
        <Page>
            <Page.Title>Probe Design Upload</Page.Title>
            {hasProbeDesignAccess ? (
                <ProbeDesignUploadForm initialValues={initValues} />
            ) : (
                <AccessDenied />
            )}
        </Page>
    );
};

export default ProbeDesignUploadPage;