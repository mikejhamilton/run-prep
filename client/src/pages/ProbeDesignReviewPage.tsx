import React, { useEffect, useState } from "react";
import { useTitle } from "../services/hooks";
import { Page } from "../components/layout";
import { fetchProbesByJob, resetProbes } from "../redux/slices/probes";
import { useSelector, useDispatch } from "react-redux";
import {
    selectCurrentPage,
    selectCurrentPageItems,
    selectSizePerPage,
    selectTotalSize,
} from "../redux/selectors/probes";
import { useParams } from "react-router";
import { fetchJobById } from "../redux/slices/uploadJobs";
import { selectJobById } from "../redux/selectors/uploadJobs";
import api from "../services/api";
import JobHeader from "../components/ProbeDesign/Review/JobHeader";
import ProbeReviewTable from "../components/ProbeDesign/Review/ProbeTable";
import { Row, Spinner } from "react-bootstrap";
import { isEmpty } from "lodash";
import styles from "./ProbeDesignReviewPage.module.scss";

interface PaginationProps {
    page: number;
    sizePerPage: number;
}

interface ReviewParams {
    id: string;
}

const ProbeDesignReviewPage: React.FC = () => {
    useTitle("Probe Status Page");

    const dispatch = useDispatch();
    const { id } = useParams<ReviewParams>();
    const job = useSelector(selectJobById(id));
    const page = useSelector(selectCurrentPage);
    const sizePerPage = useSelector(selectSizePerPage);
    const probes = useSelector(selectCurrentPageItems);
    const totalSize = useSelector(selectTotalSize);

    const [columns, setColumns] = useState([]);
    const [defaultColumns, setDefaultColumns] = useState([]);

    useEffect(() => {
        dispatch(fetchJobById(Number(id)));
    }, [id, dispatch]);

    useEffect(() => {
        if (job) {
            // Get Column options for a given technology
            api.get(`/api/v2/probe/columns/${job.technology}`).then((res: any) => {
                setColumns(res.data.api_data["columns"]);
                setDefaultColumns(res.data.api_data["defaultColumns"]);
            });
            // Fetch Probes for a given job
            dispatch(fetchProbesByJob(Number(id), page, sizePerPage));
        }
        return () => {
            // Clear probes on component dismount
            dispatch(resetProbes());
        };
    }, [job]);

    const handleTableChange = (type: any, props: PaginationProps) => {
        dispatch(fetchProbesByJob(Number(id), props.page, props.sizePerPage));
    };

    const probeTable =
        isEmpty(columns) || isEmpty(defaultColumns) || !job ? (
            <div>
                <Row className={styles.centered}>
                    <h1>Loading Probes</h1>
                </Row>
                <Row className={styles.centered}>
                    <Spinner animation="border" />
                </Row>
            </div>
        ) : (
            <ProbeReviewTable
                totalColumns={columns}
                defaultColumns={defaultColumns}
                technology={job.technology}
                probes={probes}
                page={page}
                sizePerPage={sizePerPage}
                totalSize={totalSize}
                handler={handleTableChange}
            />
        );
    const jobHeaderInfo = !job ? (
        <div>
            <Row className={styles.centered}>
                <h1>Loading Job</h1>
            </Row>
            <Row className={styles.centered}>
                <Spinner animation="border" />
            </Row>
        </div>
    ) : (
        <JobHeader job={job} />
    );
    return (
        <Page>
            <Page.Title>Probe Design Upload Review</Page.Title>
            {jobHeaderInfo}
            {probeTable}
        </Page>
    );
};

export default ProbeDesignReviewPage;
