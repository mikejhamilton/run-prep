import React from "react";

import { RtpcrAmplicon } from "../../types";
import RtpcrSearchRowDetail from "../components/AmpliconVersion/AmpliconSearchTable/RtpcrSearchRowDetail";
import {
    selectCurrentPage,
    selectCurrentPageItems,
    selectLoadingStatus,
    selectSizePerPage,
    selectTotalSize
} from "../redux/selectors/rtpcr-amplicons";
import { fetchRtpcrAmplicons, resetRtpcrAmplicons } from "../redux/slices/rtpcr-amplicons";
import { useTitle } from "../services/hooks";
import AmpliconPage from "./common/AmpliconPage";

const RtpcrAmpliconPage: React.FC = () => {
    useTitle("RT-PCR Amplicon Database")

    const exportEndpoints = {
        fullExport: "/api/v1/rtpcr-amplicon-version/full-export",
        searchExport: "/api/v1/rtpcr-amplicon-version/search-export"
    };
    const rowDetail = (amplicon: RtpcrAmplicon) => <RtpcrSearchRowDetail amplicon={amplicon}/>;

    return AmpliconPage({
        fetchItems: fetchRtpcrAmplicons,
        resetItems: resetRtpcrAmplicons,
        selectCurrentPage,
        selectCurrentPageItems,
        selectLoadingStatus,
        selectSizePerPage,
        selectTotalSize,
        exportEndpoints,
        rowDetail,
    });
};

export default RtpcrAmpliconPage;
