import styles from "./CreateAmpliconPage.module.scss";
import React, { useState, useEffect } from "react";
import { useTitle } from "../services/hooks";
import { Page } from "../components/layout";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useLocation } from "react-router-dom";
import { selectSangerAmpliconById } from "../redux/selectors/sanger-amplicons";
import { useAuth } from "auth";
import { fetchSangerAmpliconById } from "../redux/slices/sanger-amplicons";
import AmpliconForm from "../components/AmpliconVersion/AmpliconForm/AmpliconForm";
import { CREATE_AMPLICON_GROUPS } from "../config/userPermissions";
import { Button, Spinner } from "react-bootstrap";
import { alertError } from "../services/alert";
import api from "../services/api";
import { filter, isEmpty, map } from "lodash";
import { submit, getFormValues, getFormSyncErrors } from "redux-form";
import { submitCreate, submitModify } from "../components/AmpliconVersion/AmpliconForm/submit";
import SaveModal from "../components/AmpliconVersion/SaveModal";
import parseCoordinates from "../util/parseCoordinates";
import { getPrimerByDirectionAndStep } from "../util";
import hasAccess from "../util/hasAccess";
import Loading from "../components/common/Loading";
import { getAvailableAmpliconVersionNumber, getCRange } from "../util/AmpliconVersion/AmpliconVersionUtils";
import { hasOverLap } from "../util/Primer/PrimerUtils";
import { ISangerPrimerDesign } from "../../types/amp";

const CreateSangerAmpliconPage: React.FC = () => {
    useTitle("Create/Update");

    //@ts-ignore
    const { id } = useParams();
    const location = useLocation();
    const dispatch = useDispatch();
    const [showModal, setShowModal] = useState(false);
    const [saveLoading, setSaveLoading] = useState(false);
    const [ampliconVersionNumber, setAmpliconVersionNumber] = useState(0);
    const [overWrittenPrimers, setoverWrittenPrimers] = useState<object[]>([]);
    const [overWrittenPrimerErrors, setoverWrittenPrimerErrors] = useState<string[]>([]);
    const [overlappedPrimers, setoverlappedPrimers] = useState<string[]>([]);
    const [initialFormValues, setInitialFormValues] = useState<object>({});
    const {getUserGroups} = useAuth();
    const userGroups = getUserGroups();
    const currentAmplicon = useSelector(selectSangerAmpliconById(id));
    const formValues: any = useSelector((state) => getFormValues("amplicon")(state));
    const createAccess = hasAccess(userGroups, CREATE_AMPLICON_GROUPS);
    const newAmplicon = location.pathname.includes("create");
    const duplicateAmplicon = location.pathname.includes("duplicate");
    const formSyncErrors = useSelector((state) => getFormSyncErrors("amplicon")(state));

    const handleClose = (effectLoading: boolean = false) => {
        setShowModal(false);
        if (effectLoading) {
            setSaveLoading(false);
        }
    };

    const getOverWrittenPrimers = async () => {
        const primersToCheck = {
            forwardPCRPrimer: {
                name: formValues.primerDesign.forward_pcr_name,
                sequence: formValues.primerDesign.forward_pcr_seq,
                coords: formValues.primerDesign.forward_pcr_coord,
                alubp: formValues.primerDesign.forward_pcr_alubp,
            },
            reversePCRPrimer: {
                name: formValues.primerDesign.reverse_pcr_name,
                sequence: formValues.primerDesign.reverse_pcr_seq,
                coords: formValues.primerDesign.reverse_pcr_coord,
                alubp: formValues.primerDesign.reverse_pcr_alubp,
            },
            forwardSEQPrimer: {
                name: formValues.primerDesign.forward_seq_name,
                sequence: formValues.primerDesign.forward_seq_seq,
                coords: formValues.primerDesign.forward_seq_coord,
                alubp: false
            },
            reverseSEQPrimer: {
                name: formValues.primerDesign.reverse_seq_name,
                sequence: formValues.primerDesign.reverse_seq_seq,
                coords: formValues.primerDesign.reverse_seq_coord,
                alubp: false
            },
        };
        const errors: string[] = [];
        const primerWarnings: { primers: any; errors: any } = {
            primers: [],
            errors: [],
        };

        for (const [_, primer] of Object.entries(primersToCheck)) {
            if (["s-tag", "as-tag"].includes(primer.name) || !primer.name) {
                continue;
            }
            await isPrimerDifferent(primer.name!, primer.sequence!, primer.alubp, primer.coords!)
                .then(async (diff) => {
                    if (diff) {
                        await getAffectedAmplicons(primer.name).then((res) => {
                            const amps = map(
                                res.data.api_data.results,
                                (amplicon_version) => amplicon_version.amplicon_version_name
                            );
                            primerWarnings.primers.push({
                                name: primer.name,
                                affectedAmplicons: amps,
                            });
                        });
                    }
                })
                .catch(() => {
                    errors.push(primer.name);
                });
        }
        setoverWrittenPrimers(() => [...primerWarnings.primers]);
        setoverWrittenPrimerErrors(() => [...errors]);
        return {
            primers: primerWarnings.primers,
            errors: errors,
        };
    };

    const getAffectedAmplicons = (primer: string) => {
        return api.get("/api/v2/amplicon-version/fetch-by-primer", {
            params: { primer_name: primer },
        });
    };

    const isPrimerDifferent = (
        primerName: string,
        newSeq: string,
        newAluBP: any,
        coord: string
    ): Promise<boolean> => {
        const coords = parseCoordinates(coord);
        return new Promise((resolve, reject) => {
            api.get(
                "/api/v2/primer-design/fetch-by-name?primer_name=" + primerName.replace(" ", "+")
            )
                .then((res) => {
                    const data = res.data.api_data;
                    if (
                        newSeq != data.sequence ||
                        newAluBP != data.is_alubp ||
                        coords.chromosome != data.chromosome ||
                        coords.startPosition != data.start_position ||
                        coords.endPosition != data.end_position
                    ) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                })
                .catch((err) => {
                    // Primer not found
                    if (err.response.data.success) {
                        resolve(false);
                    } else {
                        alertError("Error checking modified primer." + primerName);
                        reject(false);
                    }
                });
        });
    };
    
    /**
     * Given the current amplicon being created, check to see if the forward or reverse
     * PCR primer sequence has been already used in a different amplicon of the same gene
     */
    const getReusedPrimers = async (formValues: any) => {
        let amplicons = await getAmpliconsVersionVersions(formValues.topbar.gene)
        // Remove the current amplicon if updating an already created amplicon
        amplicons = filter(amplicons, amp => amp.pcr_primer_pair_name !== formValues.topbar.pcr_primer_pair_name)
        const overlapList: string[] = [];
        const forwardCoords = parseCoordinates(formValues.primerDesign.forward_pcr_coord)
        const reverseCoords = parseCoordinates(formValues.primerDesign.reverse_pcr_coord)
        // Loop through list of amplicons with same gene and find overlapping primers
        for (const amp of amplicons) {
          const forwardCoordNew = getPrimerByDirectionAndStep(amp.primers, "PCR", "forward");
          const reverseCoordNew = getPrimerByDirectionAndStep(amp.primers, "PCR", "reverse");
          if (forwardCoordNew !== null && hasOverLap(forwardCoords, forwardCoordNew)) {
            overlapList.push(
              "Amplicon has overlap with forward primer: " +
                forwardCoordNew!.sanger_primer_name +
                " used by amplicon: " +
                amp.pcr_primer_pair_name
            );
          }
          if (reverseCoordNew !== null && hasOverLap(reverseCoords, reverseCoordNew)) {
            overlapList.push(
              "Amplicon has overlap with reverse primer: " +
                reverseCoordNew!.sanger_primer_name +
                " used by amplicon: " +
                amp.pcr_primer_pair_name
            );
          }
        }
        setoverlappedPrimers(() => [...overlapList]);
        return overlapList
    }

    /**
     * Get list of amplicon versions with a specific gene
     */
    const getAmpliconsVersionVersions = async (gene: string) => {
        const amps = api.get("/api/v2/amplicon-version/fetch-by-gene", {
            params: { gene: gene },
        }).then((res) => res.data.api_data.results);
        return amps
    }

    const handleSubmit = async () => {
        if (!createAccess) {
            alertError("Insufficient Privileges. Need Group: " + CREATE_AMPLICON_GROUPS);
            return;
        }
        // If there are form errors, the submit function will display all of them, without submitting.
        if (!isEmpty(formSyncErrors)) {
            dispatch(submit("amplicon"));
            return;
        }
        setSaveLoading(true);

        const primers = await getOverWrittenPrimers(); 
        const overLappedPrimers = await getReusedPrimers(formValues);

        if (!isEmpty(primers.primers) || !isEmpty(primers.errors) || !isEmpty(overLappedPrimers)) {
            setShowModal(true);
        } else {
            dispatch(submit("amplicon"));
        }
    };

    const getCoordinateStringFromPrimer = (primer: ISangerPrimerDesign | null) => {
        if (!primer) return null;
        if (primer.chromosome === "NA") {
            return "NA";
        } else {
            return primer.chromosome + ":" + primer.start_position + "-" + primer.end_position;
        }
    };

    // Submit method should be used based on whether we are updating or creating a new amplicon
    const submitMethod = newAmplicon
        ? (values: any) => submitCreate(values, setSaveLoading)
        : (values: any) => submitModify(values, setSaveLoading);

    useEffect(() => {
        // Amplicon not loaded in redux, need to fetch new amplicon
        if (id != null && !newAmplicon && currentAmplicon == undefined) {
            dispatch(fetchSangerAmpliconById(id));
        // Amplicon loaded but version number not defined yet
        } else if (!ampliconVersionNumber && currentAmplicon != undefined) {
                if (duplicateAmplicon) {
                    getAvailableAmpliconVersionNumber(currentAmplicon!.amplicon.gene_symbol, currentAmplicon!.amplicon.exon).then(res => {
                        setAmpliconVersionNumber(res)
                    })
                } else {
                    setAmpliconVersionNumber(currentAmplicon!.version_number)
                }
        // Ampicon Loaded, set initial form values
        } else if (id != null && !newAmplicon && currentAmplicon != undefined && ampliconVersionNumber > 0) {
            const forwardSeqPrimer = getPrimerByDirectionAndStep(currentAmplicon.primers, "BDT", "forward");
            const reverseSeqPrimer = getPrimerByDirectionAndStep(currentAmplicon.primers, "BDT", "reverse");
            const forwardPcrPrimer = getPrimerByDirectionAndStep(currentAmplicon.primers, "PCR", "forward");
            const reversePcrPrimer = getPrimerByDirectionAndStep(currentAmplicon.primers, "PCR", "reverse");
            let version, amplicon_name, pcr_primer_pair_name, priority, status;
            if (duplicateAmplicon) {
                // Duplicating an amplicon requires updating the version and naming
                version = ampliconVersionNumber
                amplicon_name = `${currentAmplicon.amplicon.gene_symbol} ${currentAmplicon.amplicon.exon}v${version}`
                pcr_primer_pair_name = `${amplicon_name} ${forwardPcrPrimer?.sanger_primer_name.split(" ").pop()}/${reversePcrPrimer?.sanger_primer_name.split(" ").pop()}`
                status = "Pending Approval"
                priority = "Backup"
            } else {
                version = currentAmplicon.version_number
                amplicon_name = currentAmplicon.amplicon_version_name
                pcr_primer_pair_name = currentAmplicon.pcr_primer_pair_name
                status = currentAmplicon.validation_status
                priority = currentAmplicon.priority
            }
            setInitialFormValues({
                topbar: {
                    gene: currentAmplicon.amplicon.gene_symbol,
                    exon: currentAmplicon.amplicon.exon,
                    isoform_version: currentAmplicon.isoform_version,
                    amplicon: amplicon_name,
                    pcr_primer_pair_name: pcr_primer_pair_name,
                    c_range: getCRange(currentAmplicon),
                },
                data: {
                    version_number: version,
                    thermo_program: currentAmplicon.thermo_program,
                    pcr_method: currentAmplicon.pcr_method || "Standard",
                    c_start_extension: currentAmplicon.c_start_extension,
                    c_end_extension: currentAmplicon.c_end_extension,
                    c_dot_start: currentAmplicon.c_dot_start,
                    c_dot_end: currentAmplicon.c_dot_end,
                    seq_module: currentAmplicon.seq_module,
                    amp_module: currentAmplicon.amp_module,
                    betaine: currentAmplicon.betaine,
                    // Target can be null for AluBP primers
                    g_start: currentAmplicon.target?.start_position,
                    g_end: currentAmplicon.target?.end_position,
                    dmso: currentAmplicon.dmso,
                    qsol: currentAmplicon.qsol,
                    priority: priority,
                    validation_status: status,
                    fragment_length: currentAmplicon.fragment_length.replace(/\s/g, ''),
                    sheet: currentAmplicon.batching_data.sheet,
                    sequencing_direction: currentAmplicon.sequencing_direction,
                    gc_content: currentAmplicon.gc_content,
                    is_alubp: currentAmplicon.is_alubp,
                    pseudogene: currentAmplicon.pseudogene,
                },

                primerDesign: {
                    forward_seq_name: forwardSeqPrimer?.sanger_primer_name ?? "NA",
                    forward_seq_seq: forwardSeqPrimer?.sequence ?? "NA",
                    forward_seq_coord: getCoordinateStringFromPrimer(forwardSeqPrimer) ?? "NA",
                    reverse_seq_name: reverseSeqPrimer?.sanger_primer_name ?? "NA",
                    reverse_seq_seq: reverseSeqPrimer?.sequence ?? "NA",
                    reverse_seq_coord: getCoordinateStringFromPrimer(reverseSeqPrimer) ?? "NA",
                    forward_pcr_name: forwardPcrPrimer?.sanger_primer_name ?? "NA",
                    forward_pcr_seq: forwardPcrPrimer?.sequence ?? "NA",
                    forward_pcr_coord: getCoordinateStringFromPrimer(forwardPcrPrimer) ?? "NA",
                    forward_pcr_alubp: forwardPcrPrimer?.is_alubp ?? "NA",
                    reverse_pcr_name: reversePcrPrimer?.sanger_primer_name ?? "NA",
                    reverse_pcr_seq: reversePcrPrimer?.sequence ?? "NA",
                    reverse_pcr_coord: getCoordinateStringFromPrimer(reversePcrPrimer) ?? "NA",
                    reverse_pcr_alubp: reversePcrPrimer?.is_alubp ?? "NA",
                },
            });
        } else if (newAmplicon) {
            // Set default values for creating new amplicon
            setInitialFormValues({
                data: {
                    thermo_program: "61",
                    pcr_method: "STANDARD",
                    c_start_extension: "-100",
                    c_end_extension: "100",
                    seq_module: "-7 to 7",
                    amp_module: "-7 to 7",
                    betaine: "0.0",
                    dmso: "0.0",
                    qsol: "0.0",
                    priority: "Primary",
                    validation_status: "Approved CUSTOM",
                    fragment_length: "0",
                    sheet: "SANGER",
                    sequencing_direction: "Both",
                    is_alubp: false,
                    pseudogene: false,
                },
                primerDesign: {
                    forward_seq_name: "s-tag",
                    forward_seq_seq: "TCTGCCTTTTTCTTCCATCGGG",
                    forward_seq_coord: "NA",
                    reverse_seq_name: "as-tag",
                    reverse_seq_seq: "TCCCCAACCCCCTAAAGCGA",
                    reverse_seq_coord: "NA",
                    forward_pcr_alubp: false,
                    reverse_pcr_alubp: false,
                },
            });
        }
    }, [dispatch, currentAmplicon, ampliconVersionNumber]);

    const saveSpinnerShow = saveLoading ? (
        <Spinner animation="border" role="status" aria-hidden="true" />
    ) : (
        "Save"
    );

    return (
        <Page>
            {(isEmpty(initialFormValues)) ? (
                <div style={{ textAlign: "center" }}>
                    Loading Amplicon
                    <Loading />
                </div>
            ) : (
                <React.Fragment>
                    <AmpliconForm initialValues={initialFormValues} onSubmit={submitMethod} />
                    <Button className={styles.save_btn} onClick={handleSubmit}>
                        {saveSpinnerShow}
                    </Button>
                </React.Fragment>
            )}


            <SaveModal
                primers={overWrittenPrimers}
                overlappedPrimers={overlappedPrimers}
                errors={overWrittenPrimerErrors}
                show={showModal}
                handleClose={handleClose}
            />
        </Page>
    );
};

export default CreateSangerAmpliconPage;
