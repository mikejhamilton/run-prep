import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { change } from "redux-form";

import Loading from "../components/common/Loading";
import DateFilter from "../components/form/date-filter";
import EventLogTable from "../components/Event/EventLogTable";
import { Page } from "../components/layout";
import { TWO_WEEKS_MILLISECONDS } from "../constants";
import { selectHighDateFromFilter, selectLowDateFromFilter } from "../redux/selectors/date-filter";
import {
    selectCurrentPage,
    selectCurrentPageItems,
    selectSizePerPage,
    selectTotalSize,
} from "../redux/selectors/events";
import { fetchEventsByDateRange } from "../redux/slices/events";
import { useTitle } from "../services/hooks";
import { useHistory, useLocation } from "react-router";
import { parse } from "querystring";
import { isEmpty } from "lodash";
import { getDateRangeQuery, getPaginationQuery, getExpandedRowQuery, parseExpandedRow, parsePaginationItems, parseDateRange } from "../util/queryStringUtils";

interface EventLogPageProps {
    lowDate: string;
    highDate: string;
    page: number;
    sizePerPage: number;
}

interface PaginationProps {
    page: number;
    sizePerPage: number;
}

const defaultLowDate = new Date(Date.now() - TWO_WEEKS_MILLISECONDS).toISOString().split("T")[0];
const defaultHighDate = new Date().toISOString().split("T")[0];

const EventLogPage: React.FC<EventLogPageProps> = () => {
    useTitle("Event Log");
    const [loading, setLoading] = useState(false);
    const [expandedRows, setExpandedRows] = useState<Array<number>>([]);

    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();

    const lowDate = useSelector(selectLowDateFromFilter) ?? defaultLowDate;
    const highDate = useSelector(selectHighDateFromFilter) ?? defaultHighDate;

    const events = useSelector(selectCurrentPageItems);
    const page = useSelector(selectCurrentPage);
    const sizePerPage = useSelector(selectSizePerPage);
    const totalSize = useSelector(selectTotalSize);

    const handleTableChange = (type: string, newState: PaginationProps) => {
        handleFetch(newState.page, newState.sizePerPage);
    };

    const handleFetch = (newPage?: number | null, newSizePerPage?: number | null, newLowDate?: string, newHighDate?: string) => {
        setLoading(true);
        dispatch(
            fetchEventsByDateRange(
                setLoading,
                newLowDate ?? lowDate,
                newHighDate ?? highDate,
                newPage ?? page,
                newSizePerPage ?? sizePerPage
            )
        );
    };

    useEffect(() => {
        // Push date and pagination items to query params in current url
        let queries = [];
        if (lowDate && highDate) queries.push(getDateRangeQuery(lowDate, highDate));
        if (sizePerPage && page) queries.push(getPaginationQuery(page, sizePerPage));
        if (!isEmpty(expandedRows)) queries.push(getExpandedRowQuery(expandedRows))
        history.replace({
            search: (!isEmpty(queries))? ("?" + queries.join("&")) : "",
        });
    }, [lowDate, highDate, sizePerPage, page, expandedRows]);

    useEffect(() => {
        if (location.search) {
            const filterQueries = parse(location.search.substring(1));
            const currentExpandedRows = parseExpandedRow(filterQueries)
            const [currentPage, currentSizePerPage] = parsePaginationItems(filterQueries);
            const [currentLowDate, currentHighDate] = parseDateRange(filterQueries)
            dispatch(change("date-filter", "lowDate", currentLowDate ?? lowDate));
            dispatch(change("date-filter", "highDate", currentHighDate ?? highDate));
            setExpandedRows(currentExpandedRows)
            handleFetch(currentPage, currentSizePerPage, currentLowDate, currentHighDate);
        } else {
            handleFetch();
        }
    }, [dispatch]);

    return (
        <Page>
            <Row>
                <Col md={9} lg={6}>
                    <DateFilter
                        disabled={loading}
                        initialValues={{ lowDate, highDate }}
                        handleSubmit={() => handleFetch()}
                    />
                </Col>
            </Row>
            {loading ? (
                <Loading />
            ) : (
                <EventLogTable
                    events={events}
                    page={page}
                    sizePerPage={sizePerPage}
                    totalSize={totalSize}
                    handlePagination={handleTableChange}
                    setExpandedRows={setExpandedRows}
                    expandedRows={expandedRows}
                />
            )}
        </Page>
    );
};

export default EventLogPage;
