import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { ActionCreatorWithoutPayload } from "@reduxjs/toolkit";
import { filter, findIndex, isEmpty } from "lodash";
import { parse } from "querystring";
import { RootState } from "@/redux/root";
import { AppThunk } from "@/redux/store";

import { ISearchFilter } from "../../../types";
import AmpliconSearchBar from "../../components/AmpliconVersion/AmpliconSearchBar/AmpliconSearchBar";
import AmpliconSearchTable from "../../components/AmpliconVersion/AmpliconSearchTable/AmpliconSearchTable";
import ExportModal from "../../components/AmpliconVersion/AmpliconSearchTable/ExportModal/ExportModal";
import Loading from "../../components/common/Loading";
import { Page } from "../../components/layout";
import { alertError } from "../../services/alert";
import {
    getAmpliconSearchFilterQuery,
    getExpandedRowQuery,
    getPaginationQuery,
    getOrderFieldQuery,
    parseAmpliconSearchFilters,
    parseExpandedRow,
    parseOrderField,
    parseIsDesc,
    getIsDescQuery
} from "../../util/queryStringUtils";
import styles from "../CreateAmpliconPage.module.scss";

interface PaginationProps {
    page: number;
    sizePerPage: number;
    sortField: string;
    sortOrder: string;
}

interface AmpliconPageProps {
    fetchItems: (page: number, sizePerPage: number, filters: ISearchFilter[], sortField?: string, sortOrder?: string) => AppThunk;
    resetItems: ActionCreatorWithoutPayload;
    selectCurrentPage: (state: RootState) => number;
    selectCurrentPageItems: (state: RootState) => object[];
    selectLoadingStatus: (state: RootState) => boolean;
    selectSizePerPage: (state: RootState) => number;
    selectTotalSize: (state: RootState) => number;
    exportEndpoints: {fullExport: string, searchExport: string};
    rowDetail: (row: any) => React.ReactNode;
}

const AmpliconPage: React.FC<AmpliconPageProps> = (
    props: AmpliconPageProps
) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();
    const page = useSelector(props.selectCurrentPage);
    const sizePerPage = useSelector(props.selectSizePerPage);
    const [showExportModal, setShowExportModal] = useState(false);
    const [searchFilters, setSearchFilters] = useState<ISearchFilter[]>([]);
    const [expandedRows, setExpandedRows] = useState<Array<number>>([]);
    const [orderField, setOrderField] = useState<string>('c_range');
    const [isDesc, setIsDesc] = useState<boolean>(false);
    const loading = useSelector(props.selectLoadingStatus);
    const amplicons = useSelector(props.selectCurrentPageItems);
    const totalSize = useSelector(props.selectTotalSize);

    useEffect(() => {
        // Push new filters and pagination to query params in current url
        const queries = [];
        if (!isEmpty(searchFilters)) queries.push(getAmpliconSearchFilterQuery(searchFilters));
        if (page && sizePerPage) queries.push(getPaginationQuery(page, sizePerPage));
        if (!isEmpty(expandedRows)) queries.push(getExpandedRowQuery(expandedRows));
        if (!isEmpty(orderField)) queries.push(getOrderFieldQuery(orderField));
        if (isDesc) queries.push(getIsDescQuery());
        history.replace({
            search: (!isEmpty(queries)) ? ("?" + queries.join("&")) : ""
        })
    }, [sizePerPage, page, searchFilters, history, expandedRows, orderField, isDesc])

    useEffect(() => {
        // Upon Page load, convert any query params to filters/pagination
        if (location.search) {
            const filterQueries = parse(location.search.substring(1));
            const currentExpandedRows = parseExpandedRow(filterQueries)
            const filters = parseAmpliconSearchFilters(filterQueries);
            setSearchFilters(filters)
            setExpandedRows(currentExpandedRows)
            const orderField = parseOrderField(filterQueries);
            if (!isEmpty(orderField)) setOrderField(parseOrderField(filterQueries));
            setIsDesc(parseIsDesc(filterQueries));
        }
        return () => {
            // Clear Amplicons on component dismount
            dispatch(props.resetItems());
        };
    }, [dispatch]);

    const handleRemoveFilter = (searchFilter: ISearchFilter) => {
        const newSearchFilters = filter(searchFilters, (f: ISearchFilter) => f != searchFilter);
        if (!isEmpty(newSearchFilters)) {
            dispatch(props.fetchItems(1, sizePerPage, newSearchFilters));
        }
        setSearchFilters(newSearchFilters);
    };

    const handleAddFilter = (searchFilter: ISearchFilter) => {
        const {value} = searchFilter
        if (!value) {
            alertError("Cannot add empty filter");
            return;
        }
        // Prevent duplicate filters
        if (findIndex(searchFilters, searchFilter) === -1) {
            const updatedFilters = [...searchFilters, searchFilter];
            setSearchFilters(updatedFilters);
            dispatch(props.fetchItems(1, sizePerPage, updatedFilters));
        } else {
            alertError("Cannot add duplicate filter");
            return;
        }
    };

    const handleTableChange = (type: string, paginationProps: PaginationProps) => {
        dispatch(props.fetchItems(paginationProps.page, paginationProps.sizePerPage, searchFilters, paginationProps.sortField, paginationProps.sortOrder));
    };

    const loadingItems =
        loading ? (
            <div style={{textAlign: "center", paddingTop: "10px"}}>
                Loading Amplicons
                <Loading/>
            </div>
        ) : null;

    const noAmpliconsFound =
        !loading && totalSize === 0 && !isEmpty(searchFilters) ? (
            <div style={{textAlign: "center", paddingTop: "10px"}}>No Amplicons Found</div>
        ) : null;

    const ampTable = isEmpty(searchFilters) ? (
        <div style={{textAlign: "center", paddingTop: "10px"}}>
            Add filter to search for amplicons.
        </div>
    ) : (
        <AmpliconSearchTable
            amplicons={amplicons}
            page={page}
            sizePerPage={sizePerPage}
            totalSize={totalSize}
            handler={handleTableChange}
            setExpandedRows={setExpandedRows}
            expandedRows={expandedRows}
            sorting={{
                orderField,
                setOrderField,
                isDesc,
                setIsDesc,
            }}
            rowDetail={props.rowDetail}
        />
    );

    return (
        <Page>
            <AmpliconSearchBar
                filters={searchFilters}
                setFilters={setSearchFilters}
                handleAddFilter={handleAddFilter}
                handleRemoveFilter={handleRemoveFilter}
                setExpandedRows={setExpandedRows}
            />
            {loadingItems}
            {noAmpliconsFound}
            {ampTable}
            <Button className={styles.export_btn} onClick={() => setShowExportModal(true)}>
                Export Table
            </Button>
            <ExportModal
                searchFilters={searchFilters}
                show={showExportModal}
                handleClose={() => setShowExportModal(false)}
                endpoints={props.exportEndpoints}
            />
        </Page>
    );
};

export default AmpliconPage;
