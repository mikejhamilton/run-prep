import React from "react";

import {
    selectCurrentPage,
    selectCurrentPageItems,
    selectSizePerPage,
    selectTotalSize,
    selectLoadingStatus,
} from "redux/selectors/sanger-amplicons";
import { SangerAmplicon } from "../../types";
import SangerSearchRowDetail from "../components/AmpliconVersion/AmpliconSearchTable/SangerSearchRowDetail";
import { fetchSangerAmplicons, resetSangerAmplicons } from "../redux/slices/sanger-amplicons";
import { useTitle } from "../services/hooks";
import AmpliconPage from "./common/AmpliconPage";

const SangerAmpliconPage: React.FC = () => {
    useTitle("Sanger Amplicon Database");

    const exportEndpoints = {
        fullExport: "/api/v1/amplicon-version/full-export",
        searchExport: "/api/v1/amplicon-version/search-export"
    };
    const rowDetail = (row: SangerAmplicon) => <SangerSearchRowDetail row={row} primers={row.primers}/>;

    return AmpliconPage({
        fetchItems: fetchSangerAmplicons,
        resetItems: resetSangerAmplicons,
        selectCurrentPage,
        selectCurrentPageItems,
        selectLoadingStatus,
        selectSizePerPage,
        selectTotalSize,
        exportEndpoints,
        rowDetail,
    });
};

export default SangerAmpliconPage;
