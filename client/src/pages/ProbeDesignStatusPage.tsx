import React, { useEffect, useState } from "react";
import { useTitle } from "../services/hooks";
import { Page } from "../components/layout";
import { fetchJobs } from "../redux/slices/uploadJobs";
import { useSelector, useDispatch } from "react-redux";
import {
    selectCurrentPage,
    selectCurrentPageItems,
    selectSizePerPage,
    selectTotalSize,
} from "../redux/selectors/uploadJobs";
import ProbeDesignStatusTable from "../components/ProbeDesign/Status/StatusTable";
import Loading from "../components/common/Loading";

interface RemoteProps {
    page: number;
    sizePerPage: number;
    filters: any;
    sortField: string | undefined;
    sortOrder: string | undefined;
}

const ProbeStatusPage: React.FC = () => {
    useTitle("Probe Status Page");

    const dispatch = useDispatch();
    const page = useSelector(selectCurrentPage);
    const sizePerPage = useSelector(selectSizePerPage);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        dispatch(fetchJobs(page, sizePerPage, setLoading));
    }, [dispatch]);

    const jobs = useSelector(selectCurrentPageItems);
    const totalSize = useSelector(selectTotalSize);

    const handleTableChange = (type: any, props: RemoteProps) => {
        setLoading(true);
        dispatch(fetchJobs(props.page, props.sizePerPage, setLoading, props.filters, props.sortField, props.sortOrder));
    };

    return (
        <Page>
            {loading ? (
                <div>
                    <h3 style={{ textAlign: "center" }}>Loading Jobs</h3>
                    <Loading />
                </div>
            ) : null}
            <ProbeDesignStatusTable
                jobs={jobs}
                page={page}
                sizePerPage={sizePerPage}
                totalSize={totalSize}
                handler={handleTableChange}
            />
        </Page>
    );
};

export default ProbeStatusPage;
