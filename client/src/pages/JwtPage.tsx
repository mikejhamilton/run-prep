import React, { CSSProperties, useContext, useState, useEffect } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import jwt_decode from 'jwt-decode';
import { Page } from '../components/layout';
import { useTitle } from '../services/hooks';
import AuthContext from '../auth/AuthContext';

const tokenStyle: CSSProperties = {
    wordWrap: 'break-word',
    whiteSpace: 'pre-wrap',
    wordBreak: 'break-all',
    fontSize: '18px',
};

interface ITokenDict {
    'parts': string[],
    'header': any,
    'payload': any,
    'issued': Date,
    'expires': Date
}
 
const JwtPage: React.FC = () => {
    useTitle('JWT Token');
    const [accessToken, setaccessToken] = useState<ITokenDict>({
        'parts': ['','',''], 'header': {}, 'payload': {}, 'issued': new Date(), 'expires': new Date()
    });
    var context = useContext(AuthContext);
    if (context === undefined) throw new Error('useAuth must be used within AuthProvider');
    const { authClient } = context;
    
    useEffect(() => {
        authClient.getAccessToken().then(obj => {
            if (obj) {
                const token = obj.value
                setaccessToken({
                    'parts': token.split('.'),
                    'header': jwt_decode(token, { header: true }),
                    'payload': jwt_decode(token, { header: false }),
                    'issued': new Date(accessToken.payload.iat * 1000),
                    'expires': new Date(accessToken.payload.exp * 1000)
                })
            }
            }).catch()
    }, [authClient, accessToken.payload.iat, accessToken.payload.exp])
    return (
        <Page>
            <Page.Title>Access Token</Page.Title>
            <Page.Body>
                <Row>
                <Col xs={6}>
                    <Card className="mb-3">
                        <Card.Body>
                            <Card.Title>Encoded Token</Card.Title>
                            <pre style={tokenStyle}>
                                <span style={{ color: '#a00' }}>{accessToken.parts[0]}</span>.
                                <span style={{ color: '#0a0' }}>{accessToken.parts[1]}</span>.
                                <span style={{ color: '#00a' }}>{accessToken.parts[2]}</span>
                            </pre>
                        </Card.Body>
                    </Card>
                    <Card className="mb-3">
                        <Card.Body>
                            Issued: <strong>{accessToken.issued.toLocaleString()}</strong>
                            <br />
                            Expires: <strong>{accessToken.expires.toLocaleString()}</strong>
                        </Card.Body>
                    </Card>
                    </Col>
                    <Col xs={6}>
                        <Card className="mb-3">
                            <Card.Body>
                                <Card.Title>Decoded Header</Card.Title>
                                <pre style={{ color: '#a00' }}>
                                    {JSON.stringify(accessToken.header, null, 2)}
                                </pre>
                            </Card.Body>
                        </Card>
                        <Card>
                            <Card.Body>
                                <Card.Title>Decoded Payload</Card.Title>
                                <pre style={{ color: '#0a0' }}>
                                    {JSON.stringify(accessToken.payload, null, 2)}
                                </pre>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Page.Body>
        </Page>
    );
};

export default JwtPage;
