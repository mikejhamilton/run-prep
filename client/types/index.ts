export type {
    AmpliconVersion,
    Design,
    ISearchFilter,
    RtpcrAmplicon,
    RtpcrPrimer,
    SangerAmplicon,
} from './amp';
export type { Comment } from './comment';
export type { Event } from './event';