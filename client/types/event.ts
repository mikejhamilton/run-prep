export interface Event {
    id: number,
    initial_values: string,
    final_values: string,
    summary: string[],
    username: string,
    created_at: string,
}