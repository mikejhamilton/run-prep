import React from 'react';
import { LinkProps } from 'react-router-dom';
import COLORS from '@/constants/colors';

/**
 * Global Overrides
 */
declare global {
  /**
   * Make properties K required that are optional on T
   */
  type Require<T, K extends keyof T> = Omit<T, K> &
    {
      [P in K]-?: T[P];
    };

  /**
   * Extract the values from an object
   * https://stackoverflow.com/questions/49285864/is-there-a-valueof-similar-to-keyof-in-typescript/49286056
   */
  type ValueOf<T> = T[keyof T];

  /**
   * Override definitions of a parent object
   */
  type Override<T, K extends { [L in keyof T]: any }> = Omit<T, keyof K> & K;

  /**
   * For those optional dependencies
   */
  type Maybe<T> = T | undefined;

  /**
   * Link Props
   */
  interface LinkPropsWithoutTo extends Omit<LinkProps, 'to' | 'id'> {
    btn?: boolean | COLORS;
  }

  interface LinkPropsWithIdentifier extends LinkPropsWithoutTo {
    id: number | string;
  }

  interface LinkPropsWithQueryParams
    extends LinksPropsWithIdentifier,
      Record<string, any> {}

  /**
   * Link Types
   */
  type LinkType = React.ComponentType<LinkPropsWithoutTo>;
  type LinkTypeWithIdentifier = React.ComponentType<LinkPropsWithIdentifier>;
  type LinkTypeWithQueryParams = React.ComponentType<LinkPropsWithQueryParams>;

  interface Window {
      amp: Record<'config', Record<string, string>>;
  }

  export interface Collection<T> {
    [key: string]: T;
    [key: number]: T;
  }

  type SerializablePayloadFields = string | number | Date

}
