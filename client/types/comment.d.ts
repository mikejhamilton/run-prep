export interface Comment {
    id: number,
    message: string,
    pinned: boolean,
    created_at: string,
    username: string,
}