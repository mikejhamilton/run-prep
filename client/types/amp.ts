import { Comment } from "./";

interface BaseAmpliconVersion {
    id: number,
    c_start_extension: number,
    c_end_extension: number,
    c_dot_start: number,
    c_dot_end: number,
    betaine: number,
    priority: string,
    validation_status: string,
    version_number: number,
    fragment_length: string,
    amplicon: Collection<string>,
    pcr_primer_pair_name: string,
    isoform_version: number,
    amplicon_version_name: string
    targets: Target[],
    created_at: string,
    created_by: {username: string},
    updated_at: string,
    updated_by: {username: string},
}

export interface SangerAmplicon extends BaseAmpliconVersion {
    thermo_program: string,
    pcr_method: string,
    seq_module: string,
    amp_module: string,
    dmso: number,
    qsol: number,
    sheet: string,
    sequencing_direction: string,
    g_dot_start: number,
    g_dot_end: number,
    gc_content: number,
    batching_data: Collection<string>,
    primers: Array<ISangerPrimerDesign>,
    target: Collection<string>,
    is_alubp: Boolean,
    pseudogene: Boolean,
    comments?: Comment[],
}

export interface RtpcrAmplicon extends BaseAmpliconVersion {
    rtpcr_primers: RtpcrPrimer[],
    control_pool: Boolean,
    multiplex_rtpcr_amplicon_version?: { id: string, pcr_primer_pair_name: string },
}

export type AmpliconVersion = SangerAmplicon | RtpcrAmplicon;

interface Library {
    id: number,
    name: string,
}

export interface UploadJob {
    id: number,
    technology: string,
    libraries: Library[],
    number_entires_created: number,
    number_errors_encountered: number,
    number_of_files: number,
    created_at: string,
    updated_at: string,
    job_status: string,
}

export type Probe = TargetedArrayProbe | NgsProbe | MlpaProbe;

export interface TargetedArrayProbe {
    id: number,
    name: string,
    design_id: number,
    array_chip_id: number,
    upload_job_id: number,
}

export interface NgsProbe {
    id: number,
    name: string,
    gc_percent: number,
    design_id: number,
    ngs_probeset_id: number,
    ngs_target_group_id: number,
    upload_job_id: number,
}

export interface MlpaProbe {
    id: number,
    name: string,
    primary_ligation_site: string,
    secondary_ligation_site: string,
    exon: string,
    genebank_exon: string,
    gene_symbol: string,
    isoform: string,
    mapview: string,
    chromosome_position: string,
    upload_job_id: number
}

export interface Identifiable {
    id: number | string;
}

export interface ISearchFilter {
    value: string;
    filter: string;
    condition: string;
}

export interface ISangerPrimerDesign extends Object {
    step: string;
    direction: string;
    sanger_primer_name: string;
    start_position: number;
    end_position: number;
    chromosome: string;
    sequence: string;
    is_alubp: boolean;
}

export interface RtpcrPrimer {
    name: string,
    designs: Design[],
}

export interface Design {
    id: number,
    chromosome: string,
    start_position: number,
    end_position: number,
    direction: string,
    sequence: string,
}

export interface Target {
    id: number,
    chromosome: string,
    start_position: number,
    end_position: number,
}

export interface IParsedCoord {
    chromosome: string | null,
    startPosition: number | null,
    endPosition: number | null,
}
