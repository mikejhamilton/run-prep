#!/bin/sh
set -e

# Template files
FILES="/app/templates/*.tmpl"
pwd
ls

for f in $FILES; do
    FILE_NAME=${f/.tmpl/}
    CONF_FILE_NAME=${FILE_NAME/\/app\/templates\/}
    CONF_FILE="/app/config/$CONF_FILE_NAME"
    echo "Creating $CONF_FILE from $f"
    envsubst < $f > $CONF_FILE
done

exec "$@"