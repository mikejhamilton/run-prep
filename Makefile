besh: # Backend Exec SHell
	docker exec -it $$(docker ps -q -f name=amp_api) bash

beshp: # Backend Exec SHell Privilaged
	docker exec -u 0 -it $$(docker ps -q -f name=amp_api) bash

weshp: # Worker Exec SHell Privilaged
	docker exec -u 0 -it $$(docker ps -q -f name=amp_worker) bash

brsh: # Backend Run SHell
	docker-compose run --rm api bash

brshp: # Backend Run SHell Privilaged
	docker-compose run -u 0 --rm api bash

setdb: # Set database migration file based on current db state
	docker-compose run -u 0 --rm api bash -c "poetry run set_db_schema"

npmi: # Run npm install inside lab client node
	docker-compose run --rm lab_client_node npm install

unittest: # Run Unittests
	poetry run python -m pytest -n 3 --junitxml /home/app/tests/reports/junit.xml

coverage: # Run Unittests with coverage report
	poetry run python -m pytest -n 3 --junitxml /home/app/tests/reports/junit.xml --cov-report html:/home/app/tests/reports/coverage --cov=amp

debug: # Start interactive pdb for backend
	telnet 127.0.0.1 4444

pre-commit:
	docker-compose run -u 0 --rm -v $$(pwd):/home/app/_src -w /home/app/_src api bash ./api/scripts/pre-commit.sh

source-mount:
	docker-compose run -u 0 --rm -v $$(pwd):/home/app/_src -w /home/app/_src api bash
